#!/bin/bash

charaGitRootDir=`dirname  $MIRCX4SPICA_SRC`
# rm mircx4spica link if it already exists
if [[ -L "$charaGitRootDir/mircx4spica" && -d "$charaGitRootDir/mircx4spica" ]]
then
    echo "$charaGitRootDir/mircx4spica is an existing symlink, let's delete it"
    rm $charaGitRootDir/mircx4spica
fi

# strip MIRCX4SPICA_SRC path from the last "/" : ${MIRCX4SPICA_SRC%/*} and create the link
ln -sf  $charaGitRootDir/mircx4spicHead   ${MIRCX4SPICA_SRC%/*}

ls -ltra $charaGitRootDir