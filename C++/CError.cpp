/***************************************************************************

                           CError.cpp
                       --------------------------

    begin                : Thursday, 4th of April, 2013 by Sylvain Rousseau
    revision             :
    email                : rousseau AT ipno DOT in2p3 DOT fr
    description          : Error handling

 ***************************************************************************/

#include "CError.h"

using namespace std;

CError::CError( int iErr, std::string& strMessage )
{
  m_stError.m_iErr = iErr;
  m_stError.m_strErrorMessage = strMessage;
}

void CError::ShowErrorMessage() const 
{
  if ( m_stError.m_strErrorMessage.length() )
    cout << m_stError.m_strErrorMessage << endl;
}