#ifndef __CSHMRINGBUF_HH__
#define __CSHMRINGBUF_HH__

#include <assert.h>
#include <cstdio>
#include <cstdlib>
#include <fcntl.h> // For O_CREAT, O_RDWR
#include <pthread.h>
#include <signal.h>
#include <stdint.h>
#include <string>
#include <sys/mman.h> // shared memory and mmap()
#include <sys/stat.h> // S_IRWXU
#include <unistd.h>
#include <err.h>

using std::string;

//
// Shared-memory based Ring buffer.
//
#define EVENT_BUFFER_SHM "/shm_ring"
// T must be POD type

enum enUserType
{
  USER = false,
  ADMIN = true
};
using eUserType = enUserType;

template <typename T>
class CShmRingBuf
{
public:
  CShmRingBuf() : _hdr(NULL), _lock(NULL), _v(NULL), _shm_size(0)
  {
  }

  ~CShmRingBuf()
  {
    if (_hdr)
      munmap((void *)_hdr, _shm_size);
    _hdr = NULL;
    _lock = NULL;
    _v = NULL;
    // Remove a named semaphore from the system if no more process uses it
    if (_master)
      shm_unlink(_shm_path.c_str());
  }

  inline bool Init(size_t cap = 100, eUserType master = USER,
                   const char *path = EVENT_BUFFER_SHM)
  {
    _master = (master == ADMIN);

    assert(path != NULL);
    _shm_path = path;

    // Create the shm_open relevant opening mode according to the eUserType
    int iOFlag = O_CREAT | O_RDWR; // ADMIN mode
    if (master == USER)            // USER mode
    {
      iOFlag = O_RDWR;
    }

    int shm_fd =
        shm_open(path, iOFlag, S_IRWXU | S_IRWXG); // TODO: O_TRUNC?
    if (shm_fd < 0)
    {
      perror("shm_open failed");
      return false;
    }

    _shm_size = sizeof(ShmHeader) + sizeof(ReadWriteLock) + cap * sizeof(T);
    if (_master && (ftruncate(shm_fd, _shm_size) < 0))
    {
      perror("ftruncate failed");
      // Remove a named semaphore from the system if no more process uses it
      shm_unlink(path);
      return false;
    }

    void *pbuf = NULL; /* shared memory adddress */
    pbuf = mmap(NULL, _shm_size, PROT_READ | PROT_WRITE, MAP_SHARED, shm_fd, 0);
    if (pbuf == (void *)-1)
    {
      perror("mmap failed");
      // Remove a named semaphore from the system if no more process uses it
      shm_unlink(path);
      return false;
    }

    _hdr = reinterpret_cast<ShmHeader *>(pbuf);
    assert(_hdr != NULL);
    _lock = reinterpret_cast<ReadWriteLock *>((char *)_hdr + sizeof(ShmHeader));
    assert(_lock != NULL);
    _v = reinterpret_cast<T *>((char *)_lock + sizeof(ReadWriteLock));
    assert(_v != NULL);

    // Check for previous proper master's initialization
    if (!_master && !_hdr->_bInitOK)
    {
      warnx("No previous proper master's initialization");
      return false;
    }

    if (master)
    {
      _hdr->_capacity = cap;
      _hdr->_begin = _hdr->_end = 0;
      _lock->Init(true);
      _hdr->_bInitOK = true;
    }

    return true;
  }

  size_t capacity() const;
  size_t begin() const;
  size_t end() const;
  size_t length() const;

  void clear();              // clear buffer
  void push_back(const T &); // insert new event
  T dump_front(bool bRemove = false);
  string unparse() const; // dump contents in the buffer to a string

private:
  // Mutex, Condition and ReadWriteLock must be POD type to use shared memory
  class Mutex
  {
  public:
    pthread_mutex_t _mutex;
    pthread_mutexattr_t _attr;

    void Init(bool pshared = false)
    {
      pthread_mutexattr_init(&_attr);
      if (pshared)
        pthread_mutexattr_setpshared(&_attr, PTHREAD_PROCESS_SHARED);
      else
        pthread_mutexattr_setpshared(&_attr, PTHREAD_PROCESS_PRIVATE);
      pthread_mutex_init(&_mutex, &_attr);
    }

    int lock() { return pthread_mutex_lock(&_mutex); }
    int trylock() { return pthread_mutex_trylock(&_mutex); }
    int unlock() { return pthread_mutex_unlock(&_mutex); }
  };

  class Condition
  {
  public:
    pthread_cond_t _cond;
    pthread_condattr_t _attr;

    void Init(bool pshared = false)
    {
      pthread_condattr_init(&_attr);
      if (pshared)
        pthread_condattr_setpshared(&_attr, PTHREAD_PROCESS_SHARED);
      else
        pthread_condattr_setpshared(&_attr, PTHREAD_PROCESS_PRIVATE);

      pthread_cond_init(&_cond, &_attr);
    }

    int wait(Mutex &m) { return pthread_cond_wait(&_cond, &m._mutex); }
    int timedwait(const struct timespec &ts, Mutex &m)
    {
      return pthread_cond_timedwait(&_cond, &m._mutex, &ts);
    }
    int signal() { return pthread_cond_signal(&_cond); }
    int broadcast() { return pthread_cond_broadcast(&_cond); }
  };

  // Multiple-writer, multiple-reader lock (write-preferring)
  class ReadWriteLock
  {
  public:
    void Init(bool pshared = false)
    {
      _nread = _nread_waiters = 0;
      _nwrite = _nwrite_waiters = 0;
      _mtx.Init(pshared);
      _rcond.Init(pshared);
      _wcond.Init(pshared);
    }

    void read_lock()
    {
      _mtx.lock();
      if (_nwrite || _nwrite_waiters)
      {
        _nread_waiters++;
        do
          _rcond.wait(_mtx);
        while (_nwrite || _nwrite_waiters);
        _nread_waiters--;
      }
      _nread++;
      _mtx.unlock();
    }

    void read_unlock()
    {
      _mtx.lock();
      _nread--;
      if (_nwrite_waiters)
        _wcond.broadcast();
      _mtx.unlock();
    }

    void write_lock()
    {
      _mtx.lock();
      if (_nread || _nwrite)
      {
        _nwrite_waiters++;
        do
          _wcond.wait(_mtx);
        while (_nread || _nwrite);
        _nwrite_waiters--;
      }
      _nwrite++;
      _mtx.unlock();
    }

    void write_unlock()
    {
      _mtx.lock();
      _nwrite--;
      if (_nwrite_waiters)
        _wcond.broadcast();
      else if (_nread_waiters)
        _rcond.broadcast();
      _mtx.unlock();
    }

  private:
    Mutex _mtx;
    Condition _rcond;
    Condition _wcond;
    uint32_t _nread, _nread_waiters;
    uint32_t _nwrite, _nwrite_waiters;
  };

  typedef struct _ShmHeader
  {
    bool _bInitOK;    // Flag to signal the shared mem seg proper previous initialization
                      // achieved by a master
    size_t _capacity; // max number of logs
    int _begin;       // start index of the circular buffer
    int _end;         // end index of the circular buffer
  } ShmHeader;

  ShmHeader *_hdr;
  ReadWriteLock *_lock;
  T *_v; // pointer to the head of event buffer
  string _shm_path;
  size_t _shm_size; // size(bytes) of shared memory
  bool _master;

  // template <class In> ...
  CShmRingBuf(const CShmRingBuf<T> &);
  CShmRingBuf<T> &operator=(const CShmRingBuf<T> &);
};

template <typename T>
inline size_t CShmRingBuf<T>::capacity() const
{
  assert(_hdr != NULL);

  size_t cap = 0;
  _lock->read_lock();
  cap = _hdr->_capacity;
  _lock->read_unlock();
  return cap;
}

template <typename T>
inline size_t CShmRingBuf<T>::begin() const
{
  assert(_hdr != NULL);

  size_t idx = 0;
  _lock->read_lock();
  idx = _hdr->_begin;
  _lock->read_unlock();
  return idx;
}

template <typename T>
inline size_t CShmRingBuf<T>::end() const
{
  assert(_hdr != NULL);

  size_t idx = 0;
  _lock->read_lock();
  idx = _hdr->_end;
  _lock->read_unlock();
  return idx;
}

template <typename T>
inline size_t CShmRingBuf<T>::length() const
{
  assert(_hdr != NULL);

  size_t length = 0;
  _lock->read_lock();

  if (_hdr->_begin < _hdr->_end)
  {
    length = _hdr->_end - _hdr->_begin;
  }
  else
  {
    length = _hdr->_capacity - (_hdr->_begin - _hdr->_end);
  }

  _lock->read_unlock();
  return length;
}

template <typename T>
inline void CShmRingBuf<T>::clear()
{
  if (!_hdr || !_lock)
    return;

  _lock->write_lock();
  _hdr->_begin = _hdr->_end = 0;
  // TODO: memset the shared memory?
  _lock->write_unlock();
}

template <typename T>
inline void CShmRingBuf<T>::push_back(const T &e)
{
  assert(_hdr != NULL);
  assert(_v != NULL);

  _lock->write_lock();

  //  !!! Works only for POD (Plain Old Data) structure
  //  memcpy(_v + _hdr->_end, &e, sizeof(e));
  //  Replaced with an explicit call to the  T object copy operator
  //  to avoid an intermediate copy overhead
  *(_v + _hdr->_end) = e;
  // !!! T need to provide the proper copy operator !!!

  _hdr->_end = (_hdr->_end + 1) %
               _hdr->_capacity;   // make sure index is in range [0..._capacity)
  if (_hdr->_end == _hdr->_begin) // buffer is full, advance begin index, too
    _hdr->_begin = (_hdr->_begin + 1) % _hdr->_capacity;
  _lock->write_unlock();
}

template <typename T>
inline T CShmRingBuf<T>::dump_front(bool bRemove)
{
  assert(_hdr != NULL);
  assert(_v != NULL);

  T ret;
  _lock->write_lock();
  if (_hdr->_begin != _hdr->_end)
  {
    ret = *(_v + _hdr->_begin);
    if (bRemove)
    {
      _hdr->_begin = (_hdr->_begin + 1) % _hdr->_capacity;
    }
  }
  _lock->write_unlock();
  return ret;
}

template <typename T>
inline string CShmRingBuf<T>::unparse() const
{
  assert(_hdr != NULL);
  assert(_v != NULL);

  string ret;
  _lock->read_lock();
  if (_hdr->_begin == _hdr->_end)
  {
    _lock->read_unlock();
    return string();
  }

  for (int i = _hdr->_begin; i != _hdr->_end; i = (i + 1) % _hdr->_capacity)
  {
    ret += string((_v + i)->unparse()) +
           "\n"; // Suppose T has a unparse() member function
  }
  _lock->read_unlock();
  return ret;
}

#endif // __CSHMRINGBUF_HH__
