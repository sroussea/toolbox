#include "CShmRingBuf.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <unistd.h>
#include <string>
#include <pthread.h>
#include <stdio.h>

using namespace std;

#include <sstream>
template <typename T>
std::string ToString(T val)
{
    std::stringstream stream;
    stream << val;
    return stream.str();
}

struct LogNode
{
    int ts;  // timestamp
    int len; // length
#define MAX_LOG_LEN 256

    char log[MAX_LOG_LEN];
    char *plog_;

    // Default constructor
    LogNode()
    {
        plog_ = NULL;
    }

    // Constructor from a char pointer
    LogNode(char *plog)
    {
        if (plog)
            plog_ = plog;
    }

    // Copy operator
    LogNode &operator=(LogNode const &src)
    {
        if (src.plog_ != NULL)
        {
            memcpy(log, (const void *)src.plog_, MAX_LOG_LEN);
        }
        else
        {
            memcpy(log, (const void *)src.log, MAX_LOG_LEN);
        }

        plog_ = src.plog_;
        ts = src.ts;
        len = src.len;

        return *this;
    }

    const std::string unparse()
    {
        return "[" + ToString(ts) + "] " + std::string(&log[0]);
    }
};

const int CAPACITY = 1;

void *ringFeeder(void *arg)
{

    // Connect to the ring buffer
    CShmRingBuf<LogNode> buffer;
    buffer.Init(CAPACITY, ADMIN, "/shm_ring");
    int i = 0;
    while (1)
    {
        usleep(rand()%900+500);
        LogNode log;
        snprintf(log.log, MAX_LOG_LEN, "%zu: %d", buffer.end(), i++);
        buffer.push_back(log);
        std::cout << "Ring Buffer content :" << std::endl;
        std::cout << buffer.unparse() << std::endl;
    }

    return arg;
}

int main(int argc, char *argv[])
{
    /* initialize random seed: */
    srand(time(NULL));

    LogNode log1, log2;
    snprintf(log1.log, MAX_LOG_LEN, "%du: %d", 0, 3);
    log2 = log1;

    char mylog[MAX_LOG_LEN];
    snprintf(mylog, MAX_LOG_LEN, "%du: %d", 1, 7);
    log2 = LogNode(mylog);

    // Launch producer
    pthread_t thProd;
    if (pthread_create(&thProd, NULL, ringFeeder, NULL) != 0)
    {
        cout << "could not start the ring feeder thread" << endl;
        return -1;
    }

    // Create the ring buffer
    CShmRingBuf<LogNode> buffer;
    buffer.Init(CAPACITY, USER);

    while (1)
    {
        LogNode log = buffer.dump_front();
        cout << "Just removed node : " << endl;
        cout << log.unparse() << endl;
        usleep(rand()%1000+500);
    }

    return 0;
}
