/***************************************************************************

                           CError.h
                       --------------------------

    begin                : Thursday, 4th of April, 2013 by Sylvain Rousseau
    revision             :
    email                : rousseau AT ipno DOT in2p3 DOT fr
    description          : Error handling

 ***************************************************************************/
#ifndef __CError_h__
#define __CError_h__

#include <iostream>
#include <string>  

typedef struct
{
  int    m_iErr;
  std::string m_strErrorMessage;
} structError;

class CError
{
public:  
  CError( int iErr, std::string& strMessage );
  ~CError() {;}

  // Accessors
  const std::string& ErrorMessage () const { return m_stError.m_strErrorMessage; }
  const int& ErrorCode ()            const { return m_stError.m_iErr; }
  void  ShowErrorMessage()           const;

private:
  structError m_stError;


};

#endif // __CError_h__
