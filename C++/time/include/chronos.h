#ifndef CHRONOS_H__
#define CHRONOS_H__
/*******************************************************************************
 * Lagrange Lab - SPICA project
 *
 * "@(#) $Id: CSpicaService.h 307417 2018-03-08 11:37:58Z sroussea $"
 *
 * who       when        what
 * --------  ---------- --------------------------------------------------------
 * sroussea  2019-01-28  Creation
 *
 *******************************************************************************/

// Nb of nanoseconds per second
#define NSEC_PER_SEC 1000000000

//-----------------------------------------------------------------------------
// System Headers
//-----------------------------------------------------------------------------
#include <time.h> // Time stamp management
#include <stdio.h>
#include <signal.h>
#include <sys/time.h> // struct timespec
#include <string.h>
// Eigen lib
#include <Eigen/Core>
using Eigen::Matrix;
using Eigen::seq;

//-----------------------------------------------------------------------------
// local Headers
//-----------------------------------------------------------------------------

// Alias for syntaxic sugar
using stTime = struct timespec;
using vecTime = Matrix<__syscall_slong_t, -1, 1>;

//-----------------------------------------------------------------------------
// usleep (almost deprecated) and nanosleep() execution is interrupted THEN
// cancelled by any signal caught by the running thread.
// The execution flow resumes at the instruction following the sleep(),
// even if time isn't up.
//
// The work around consists in using nanosleep() instead of usleep() and add
// the logic implemented here.
//
// For more info : https://man7.org/linux/man-pages/man2/nanosleep.2.html
//
//-----------------------------------------------------------------------------
void nanoSleepSigProof(const struct timespec &pTimeReq);

//-----------------------------------------------------------------------------
// The struct timespec consists of nanoseconds
// and seconds. if the nanoseconds are getting
// bigger than 1000000000 (= 1 second) the
// variable containing seconds has to be
// incremented and the nanoseconds decremented
// by 1000000000.
//-----------------------------------------------------------------------------
static inline void tsnorm(stTime *ts)
{
  while (ts->tv_nsec >= NSEC_PER_SEC)
  {
    ts->tv_nsec -= NSEC_PER_SEC;
    ts->tv_sec++;
  }
}

//-----------------------------------------------------------------------------
// Mesure a gap between a time and now in nano seconds
//-----------------------------------------------------------------------------
static inline __syscall_slong_t timeIntervalFromNowNs(stTime &tLastCall)
{
  // Current call time
  stTime tNow;
  clock_gettime(CLOCK_MONOTONIC_RAW, &tNow);

  // 2022, 18th of Jan
  //  tsnorm(&tNow); // increment seconds if needed

  // Compute time increment
  __syscall_slong_t dT_ns = tNow.tv_nsec - tLastCall.tv_nsec;
  __time_t dT_s = tLastCall.tv_sec - tNow.tv_sec;

  return dT_ns + dT_s * NSEC_PER_SEC;
}

//-----------------------------------------------------------------------------
// Mesure a gap between 2 calls in nano seconds
//-----------------------------------------------------------------------------
static inline __syscall_slong_t timeIntervalNs(stTime &tStart, stTime &tStop)
{
  // Compute time increment
  __syscall_slong_t dT_ns = tStop.tv_nsec - tStart.tv_nsec;
  __time_t dT_s = tStop.tv_sec - tStart.tv_sec;

  return dT_ns + dT_s * NSEC_PER_SEC;
}

//-----------------------------------------------------------------------------
// Timer structure
//
// Purpose : Encapsulate data to set up a oneshot
//
//  Workflow :
//
//   // Set up timer structure
//   m_stProxyCmdTimer.m_stTimer.it_interval.tv_sec = 0;
//   m_stProxyCmdTimer.m_stTimer.it_interval.tv_usec = 0;
//   m_stProxyCmdTimer.m_stTimer.it_value.tv_sec = 0;
//   m_stProxyCmdTimer.m_stTimer.it_value.tv_usec = uiTimeout * 1000; //*1000 to move into ms unit
//   m_stProxyCmdTimer.m_stAlarmAction.sa_handler = sigAlarmHandler; // signal handler ptr
//
//   void sigAlarmHandler(int iSig)
//   {
//     switch (iSig)
//     {
//     case SIGALRM:
//     {
//       m_bTimeOut = true;
//     }
//     }
//   }

//   // Unblock SIGALARM and associate it to an action
//   sigaction(SIGALRM, &m_stProxyCmdTimer.m_stAlarmAction, NULL);
//   // Start timer
//   setitimer(ITIMER_REAL, &m_stProxyCmdTimer.m_stTimer, NULL);
//
//   Implement a SIGALRM handler to catch the signal on timer
//
// Remarks
//
// https://man7.org/linux/man-pages/man2/sigaction.2.html
// struct sigaction {
//   void     (*sa_handler)(int); // Handler pointer
//   sigset_t sa_mask;            // Specifies a mask of signals which should be blocked
//                                // During execution of the signal handler.
//   int      sa_flags;           // Specifies a set of flags which modify the behavior of
//                                // the signal
//  };
//
//
// https://man7.org/linux/man-pages/man2/setitimer.2.html
//  struct itimerval {
//    struct timeval it_interval; // Interval for periodic timer
//    struct timeval it_value;    // Time until next expiration
//  };
//
//  struct timeval {
//    time_t      tv_sec;         // seconds
//    suseconds_t tv_usec;        // microseconds
//  };
//-----------------------------------------------------------------------------
struct stTimerCtrl
{
  // Timer structure to parametrize timer duration/period => feeds setitimer()
  struct itimerval stTimer_;
  // Structure which defined action triggered on signal => feeds sigaction()
  struct sigaction stAlarmAction_;
  // Timer flag
  bool bTimerExhausted_;

  stTimerCtrl() : bTimerExhausted_(false)
  {
    memset((void *)&stTimer_, 0, sizeof(stTimer_));
    memset((void *)&stAlarmAction_, 0, sizeof(stAlarmAction_));
  }
};
using stTimerCtrl = struct stTimerCtrl;

//-----------------------------------------------------------------------------
// Compute an average time interval
//
// bInit : if true, reset internal the static internal variables. Should be
//         called once before starting any progiling
//-----------------------------------------------------------------------------
__syscall_slong_t avTimeInNano(stTime &tStart, stTime &tStop, vecTime &vDuration, int &iCurSampId, bool bInit = false);

#endif // CHRONOS_H__
