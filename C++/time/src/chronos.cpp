/*************************time related facilitieschronos***********************

 * who       when        what
 * --------  ---------- --------------------------------------------------------
 * sroussea  2019-01-28  Creation
 *
 * NAME
 *   chronos.cpp -
 *
 * SYNOPSIS
 *   some useful time related facilities
 *
 * DESCRIPTION
 *
 *
 *
 *******************************************************************************/

//-----------------------------------------------------------------------------
// local Headers
//-----------------------------------------------------------------------------
#include "chronos.h"
#include <errno.h> // errno

//-----------------------------------------------------------------------------
// usleep (almost deprecated) and nanosleep() execution is interrupted THEN
// cancelled by any signal caught by the running thread.
// The execution flow resumes at the instruction following the sleep(),
// even if time isn't up.
//
// The work around consists in using nanosleep() instead of usleep() and add
// the logic implemented here.
//
// For more info : https://man7.org/linux/man-pages/man2/nanosleep.2.html
//
//-----------------------------------------------------------------------------
void nanoSleepSigProof(const struct timespec &pTimeReq)
{
  struct timespec timeRem = {0, 0};

  int sleepRet = nanosleep(&pTimeReq, &timeRem);
  while (sleepRet < 0 && errno == EINTR)
  {
    sleepRet = nanosleep(&timeRem, &timeRem);
  }
}

//-----------------------------------------------------------------------------
// Compute an average time interval
//
// bInit : if true, reset internal the static internal variables. Should be
//         called once before starting any progiling
//-----------------------------------------------------------------------------

//#include <iostream>
__syscall_slong_t avTimeInNano(stTime &tStart, stTime &tStop, vecTime &vDuration, int &iCurSampId, bool bInit)
{
  iCurSampId = 0;
  if (bInit)
  {
    iCurSampId = 0;
    vDuration = vecTime::Zero(vDuration.rows());
    // Put a marker to detect that vector 's been written once yet
    vDuration(vDuration.rows() - 1) = 0xDEADBEEF;
    return 0;
  }

  // Stack the current duration
  vDuration(iCurSampId++) = timeIntervalNs(tStart, tStop);

  iCurSampId = iCurSampId % vDuration.rows();

  int iLastId = (vDuration(vDuration.rows() - 1) == 0xDEADBEEF ? iCurSampId - 1 : vDuration.rows() - 1);
  long lAvTimeInNano = vDuration(seq(0, iLastId)).mean();

  // std::cout << "vDuration [ " << iSampId << "] = " << vDuration(iSampId) << std::endl;

  return lAvTimeInNano;
}
