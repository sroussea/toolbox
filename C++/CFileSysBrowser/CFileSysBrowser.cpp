#include "CFileSysBrowser.h"

using namespace std;

//-----------------------------------------------------------------------------------------
// Const/Dest - ructor
//-----------------------------------------------------------------------------------------
CFileSysBrowser::CFileSysBrowser(){;}
CFileSysBrowser::~CFileSysBrowser(){;}

//-----------------------------------------------------------------------------------------
// Provide recursively if needed the content of a directory
//-----------------------------------------------------------------------------------------
unsigned CFileSysBrowser::listDirContent( vector<string>& vStrContent, const string & strPath, eType leafType,
                                          const string & strSuffix, bool bRecursive ) const
{
  unsigned  uiErr = 0;

  DIR *dir;
  char buffer[PATH_MAX + 2];
  char *p = buffer;
  const char *src;
  char *end = &buffer[PATH_MAX];

  /* Copy directory name to buffer */
  src = strPath.c_str();
  while (p < end  &&  *src != '\0') {
      *p++ = *src++;
  }
  *p = '\0';

  /* Open directory stream */
  dir = opendir ( strPath.c_str() );
  if (dir != NULL)
  {
    struct dirent *ent;

    /* Print all files and directories within the directory */
    while ((ent = readdir (dir)) != NULL)
    {
      char *q = p;
      char c;

      /* Get final character of directory name */
      if (buffer < q) {
          c = q[-1];
      } else {
          c = ':';
      }

      /* Append directory separator if not already there */
      if (c != ':'  &&  c != '/'  &&  c != '\\') {
          *q++ = '/';
      }

      /* Append file name */
      src = ent->d_name;
      while (q < end  &&  *src != '\0') {
          *q++ = *src++;
      }
      *q = '\0';

      // Check if we prune files through the suffixes
      bool bGoodSuffix = true;
      if ( !strSuffix.empty() && ent->d_type != DT_DIR )
      {
        string strToFind = ".";
        string::size_type pos = string::npos;
        pos = string(ent->d_name).rfind( strToFind );
        if ( pos == string::npos )
          bGoodSuffix = false;
        else
        {
          bGoodSuffix = strSuffix.compare( string(ent->d_name).substr( pos+1, strSuffix.length() ) ) == 0;
        }
      }

      /* Decide what to do with the directory entry */
      switch (ent->d_type) {
      case DT_LNK:
      {
        if ( leafType == eLink && bGoodSuffix )
          vStrContent.push_back( string (ent->d_name) );
        break;
      }
      case DT_REG:
      {
        if ( leafType == eRegularFiles && bGoodSuffix )
          vStrContent.push_back( string (ent->d_name) );
        break;
      }

      case DT_DIR:
      {
        /* Scan sub-directory recursively */
        if ( strcmp (ent->d_name, ".") != 0
          &&  strcmp (ent->d_name, "..") != 0
          && bRecursive )
        {
          if ( leafType == eDir )
            vStrContent.push_back( string (ent->d_name) );
          listDirContent ( vStrContent, string( buffer), leafType, strSuffix, bRecursive );
        }
        break;
      }

      default:
          /* Ignore device entries */
          /*NOP*/;
      }
    }

    closedir (dir);

  }
  else
  {
      /* Could not open directory */
      cout  << strCANNOT_FIND_DIR << strPath << endl;
      uiErr = CANNOT_FIND_DIR_1;
  }


  return uiErr;
}
