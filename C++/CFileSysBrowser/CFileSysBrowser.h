
#ifndef __CFILE_SYS_BROWSER__
#define __CFILE_SYS_BROWSER__
/***************************************************************************
                             CFileSysBrowser.h
                             -------------------
    begin        : Thursday, 20th of March 2015
    revision     : S. Rousseau
    email        : rousseau AT ipno DOT in2p3 DOT fr
    description  : This class provides the facilities to browse through the
                   system file system.
                   LINUX - WINDOWS compliant

 ***************************************************************************/
#ifdef _WIN32
#include "winDirent/dirent.h"
#else
#include "dirent.h"
#endif

#include <string>
#include <cstring>
#include <vector>
#include <iostream>

using std::string;
using std::vector;

// Errors -o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-
const unsigned CANNOT_FIND_DIR    =   0x0001;
const unsigned CANNOT_FIND_DIR_1  =   (CANNOT_FIND_DIR << 16) + 1;
const string   strCANNOT_FIND_DIR =   string(__FILE__) + string("Cannot open directory named : ");


typedef enum { eNoType = -1, eRegularFiles = DT_REG, eDir = DT_DIR, eLink = DT_LNK } eType;

class CFileSysBrowser {

private:

  // Attributes -o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-

  // Private methods -o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-

public:

  // Public methods -o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-

  //-----------------------------------------------------------------------------------------
  // Const/Dest - ructor
  //-----------------------------------------------------------------------------------------
  CFileSysBrowser();
  ~CFileSysBrowser();

  //-----------------------------------------------------------------------------------------
  // Provide recursively if needed the content of a directory
  //-----------------------------------------------------------------------------------------
  unsigned listDirContent( vector<string>& vStrContent, const string & strPath, eType leafType = eNoType,
                           const string & strSuffix = "", bool bRecursive = false ) const;


};

#endif // __CFILE_SYS_BROWSER__
