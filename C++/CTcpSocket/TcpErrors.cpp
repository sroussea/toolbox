#include "TcpErrors.h"

namespace Sockets
{
	int GetError()
	{
#ifdef _WIN32
		return WSAGetLastError();
#else
		return errno;
#endif
	}
}