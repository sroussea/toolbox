#include "TCPSocket.h"

using namespace std;

//-----------------------------------------------------------------------------
// Static members instanciation
//-----------------------------------------------------------------------------

// Hold windows socket env data
#ifdef _WIN32
WSAData TCPSocket::wsaData_;
#endif
bool TCPSocket::bStartOK_ = false;
SOCKET TCPSocket::maxSockID_ = 0;
int TCPSocket::iNbOfSock_ = 0; // To handle OS ressources

//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Constructor
//
// Trigger std::runtime_error() exception on failure
//
//-----------------------------------------------------------------------------
TCPSocket::TCPSocket(eSockUsage sockUsage, SOCKET scktID, sockaddr_in *pScktAddr_)
    : sockUsage_(sockUsage), scktID_(scktID)
{
  // Flush members
  memset((void *)&scktAddr_, 0, sizeof(scktAddr_));
  if (pScktAddr_)
  {
    memcpy((void *)&scktAddr_, (const void *)pScktAddr_, sizeof(scktAddr_));
  }

  // Environment intialization when "this" is the first instance
  if (!bStartOK_)
  {
    bStartOK_ = Start();

    // On system failure, warn user !!!
    if (!bStartOK_)
    {
      std::ostringstream error;
      error << "Erreur initialisation socket env [" << GetError() << "]";
      throw std::runtime_error(error.str());
    }
  }

  // In case of a server socket, let's ask a socket handler to the system (socket() call)
  // In case of a new incoming client (just accepted by a  TCPSocket of type eSockUsage::eSERVER), the socket ID
  // has been delivered by the accept() call on the server socket side
  if (scktID == INVALID_SOCKET)
  {
    scktID_ = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    cout << "socket  acquired scktID_ = " << scktID_ << endl;

    // On socket instanciation failure, warn user !!!
    if (scktID_ == INVALID_SOCKET)
    {
      std::ostringstream error;
      error << "Erreur instanciation socket [" << GetError() << "]";
      throw std::runtime_error(error.str());
    }
  }

  // Instance counter gets increased
  iNbOfSock_++;

  // Increment instance counter
  if (scktID_ > maxSockID_)
    maxSockID_ = scktID_;
}

//-----------------------------------------------------------------------------
// Destructor
//-----------------------------------------------------------------------------
TCPSocket::~TCPSocket()
{
  // Gently close the socket
  CloseSocket();

  // Decrement instance counter
  iNbOfSock_--;

  // If no more instance, release OS resources
  if (iNbOfSock_ == 0)
  {
    Release();
  }
}

//-----------------------------------------------------------------------------
// Copy operator
//-----------------------------------------------------------------------------
TCPSocket &TCPSocket::operator=(const TCPSocket &src)
{
  sockUsage_ = src.sockUsage_;
  scktID_ = src.scktID_;
  memcpy((void *)&scktAddr_, &src.scktAddr_, sizeof(scktAddr_));

  return *this;
}

//-----------------------------------------------------------------------------
// comparison operator for ordered containers (here set of TCPSocket)
//-----------------------------------------------------------------------------
bool TCPSocket::operator<(const TCPSocket &rightOp) const
{
  return scktID_ < rightOp.scktID_;
}

//-----------------------------------------------------------------------------
// Look for new client connexion
//
// RETURN VALUE
// New TCPSocket object pointer on new connexion, NULL otherwise
//
// May throw a runtime_error exception on failure
//-----------------------------------------------------------------------------
TCPSocket *TCPSocket::acceptNewClient()
{
  SOCKET clientSock = 0;
  struct sockaddr IncomingAddr;
  socklen_t Addrlen = sizeof(IncomingAddr);
  TCPSocket *pNewClient = NULL;

  fd_set setReads;
  FD_ZERO(&setReads);
  timeval timeout = {0, 0}; // Exit immediately

  // Load FD_SET with the server socket handler
  FD_SET(static_cast<int>(this->scktID_), &setReads);

  // Test if a new client connexion request is pending
  int selectResult = Select(&setReads, NULL, NULL, &timeout);
  if (selectResult == -1)
  {
    std::ostringstream ostrError;
    ostrError << "Server select failed : " << GetError() << std::endl;
    throw std::runtime_error(ostrError.str());
  }
  else if (selectResult > 0)
  {
    // Wait for new clients
    clientSock = Accept(&IncomingAddr, &Addrlen);
    if (clientSock == INVALID_SOCKET)
    {
      return NULL; // On failure return
    }
    // Return a new TCPSocket of type eSockUsage::eCLIENT which should be handled in a list by the deriving class
    // which is a  TCPSocket of type eSockUsage::eSERVER
    pNewClient = new TCPSocket(eSockUsage::eCLIENT, clientSock, (sockaddr_in *)&IncomingAddr);
  }

  return pNewClient;
}

//-----------------------------------------------------------------------------
// Update socket request status in the socket set member setSock_
//
// Parameters
//
// sockSet : a ref to TCPSocket set which will hold
//
// RETURN VALUE
// true if a new client request
//-----------------------------------------------------------------------------
bool TCPSocket::getPendingClients(set<spTCPSocket> &setClients)
{
  // Usage check
  if (sockUsage_ == eSockUsage::eCLIENT)
  {
    string strErr("TCPSocket::RequestingClients cannot be called over a client socket, aborting...");
    throw std::runtime_error(strErr);
  }

  fd_set setReads;
  fd_set setWrite;
  fd_set setErrors;
  FD_ZERO(&setReads);
  FD_ZERO(&setWrite);
  FD_ZERO(&setErrors);
  timeval timeout = {0, 0};

  // Load FD_SET with clients socket handler
  bool bClientOnline = false;
  for (auto &tcpSocket : setClients)
  {
    if (tcpSocket->sockUsage_ == eSockUsage::eCLIENT)
    {
      FD_SET(static_cast<int>(tcpSocket->scktID_), &setReads);
      FD_SET(static_cast<int>(tcpSocket->scktID_), &setWrite);
      FD_SET(static_cast<int>(tcpSocket->scktID_), &setErrors);

      bClientOnline = true;
    }
  }

  // If no client online, let's return
  if (!bClientOnline)
    return bClientOnline;

  // Collect only client with pending request
  int selectResult = Select(&setReads, &setWrite, &setErrors, &timeout);
  if (selectResult == -1)
  {
    string strErr("TCPSocket::getPendingClients : Select() call failed, aborting...");
    throw std::runtime_error(strErr);
  }
  else if (selectResult > 0)
  {
    auto itClient = setClients.begin();
    while (itClient != setClients.end())
    {
      if (FD_ISSET((*itClient)->scktID_, &setErrors))
      {
        const_cast<spTCPSocket &>(*itClient)->sockReq_ = eSockReq::EXCEPT;
      }
      else if (FD_ISSET((*itClient)->scktID_, &setReads))
      {
        const_cast<spTCPSocket &>(*itClient)->sockReq_ = eSockReq::READ;
      }
      else if (FD_ISSET((*itClient)->scktID_, &setWrite))
      {
        const_cast<spTCPSocket &>(*itClient)->sockReq_ = eSockReq::WRITE;
      }

      itClient++;
    }
  }

  return bClientOnline;
}

//-----------------------------------------------------------------------------
// Bind the server socket to the hardware
//
// Return value
// If no error occurs, bind returns zero. Otherwise, it returns SOCKET_ERROR
//-----------------------------------------------------------------------------
int TCPSocket::Bind(u_short usPort)
{
  int iErr = 0;
  socklen_t optLen = 0;

  // Try to retrieve address family (AF)
#ifdef _WIN32
  WSAPROTOCOL_INFOW protocolInfo{0};
#else
  int protocolInfo;
#endif

  optLen = sizeof(protocolInfo);
  iErr = getsockopt(scktID_, SOL_SOCKET, SO_DOMAIN, reinterpret_cast<char *>(&protocolInfo), &optLen);
  if (iErr != 0)
  {
    return iErr;
  }

#ifdef _WIN32
  scktAddr_.sin_family = protocolInfo.iAddressFamily;
#else
  scktAddr_.sin_family = protocolInfo;
#endif

  // Let's bind the socket to the hardware
  scktAddr_.sin_addr.s_addr = INADDR_ANY;
  scktAddr_.sin_port = htons(usPort);
  iErr = bind(scktID_, (sockaddr *)&scktAddr_, sizeof(scktAddr_));



  return iErr;
}

//-----------------------------------------------------------------------------
// Marks the socket referred to by sockfd as a passive socket,
// that is, as a socket that will be used to accept incoming connection
// requests using accept().
// Set server maximum allowed connexions as well.
//
// On success, zero is returned.On error, -1 is returned, and errno is
// set appropriately.
//-----------------------------------------------------------------------------
int TCPSocket::Listen(int iMaxCnxAccepted)
{
  // Usage check
  if (sockUsage_ == eSockUsage::eCLIENT)
  {
    cout << "TCPSocket::RequestingClients cannot be called over a client socket, aborting..." << endl;
    return -1;
  }

  return listen(scktID_, iMaxCnxAccepted);
}

//-----------------------------------------------------------------------------
// Accept incoming client connexion to a server socket
//
// On success, these system calls return a file descriptor for the
// accepted socket(a nonnegative integer).
// On error addrlen is left unchanged and :
// Linux : -1 is returned, errno is set appropriately.
// Win : INVALID_SOCKET is returned,
//
// The integer referred to by addrlen initially contains the amount of space
// pointed to by addr. On return it will contain the actual length in bytes of
// the address returned.
//-----------------------------------------------------------------------------
SOCKET TCPSocket::Accept(sockaddr *pIncomingAddr, socklen_t *pAddrlen)
{
  // Usage check
  if (sockUsage_ == eSockUsage::eCLIENT)
  {
    cout << "TCPSocket::RequestingClients cannot be called over a client socket, aborting..." << endl;
    return INVALID_SOCKET;
  }

  return accept(scktID_, pIncomingAddr, pAddrlen);
}

//-----------------------------------------------------------------------------
//  Wait until some sockets have thrown some R/W/ERR requests
//
// select() allows a program to monitor multiple file descriptors,
// waiting until one or more of the file descriptors become "ready" for
// some class of I / O operation
//
// CAREFULL must be taken concerning the fd_set passed in args : they are
// modified : only bits of the requesting sockets are left active
//
// RETURN VALUE
// On success, select() and pselect() return the number of file
// descriptors contained in the three returned descriptor sets(that is,
// the total number of bits that are set in readfds, writefds,
// exceptfds).The return value may be zero if the timeout expired
// before any file descriptors became ready.
// On error, SOCKET_ERROR is returned, and errno is set to indicate the error; the
// file descriptor sets are unmodified, and timeout becomes undefined.
//-----------------------------------------------------------------------------
int TCPSocket::Select(fd_set *readfds, fd_set *writefds, fd_set *exceptfds, timeval *timeout)
{
  // Usage check
  if (sockUsage_ == eSockUsage::eCLIENT)
  {
    cout << "TCPSocket::RequestingClients cannot be called over a client socket, aborting..." << endl;
    return -1;
  }

  return select((int)maxSocketID() + 1, readfds, writefds, exceptfds, timeout);
}

//-----------------------------------------------------------------------------
// Connect as a client socket to a server socket
//
// RETURN VALUE : errno
// 0 : If the connection or binding succeeds,
// EISCONN : Already connected => bAlreadyConnected set to true
// See https://www.thegeekstuff.com/2010/10/linux-error-codes/ for more
//-----------------------------------------------------------------------------
int TCPSocket::Connect(const std::string &strIP, unsigned short usPort, bool &bAlreadyConnected)
{
  // Usage check
  if (sockUsage_ == eSockUsage::eSERVER)
  {
    cout << "TCPSocket::RequestingClients cannot be called over a server socket, aborting..." << endl;
    return -1;
  }

  sockaddr_in serverAddr;
  serverAddr.sin_family = AF_INET;
  serverAddr.sin_port = htons(usPort);
  // Cpy ASCII IP address into sockaddr struct in the appropriate network format
  inet_pton(AF_INET, strIP.c_str(), &serverAddr.sin_addr.s_addr);

  int iCnxErr = connect(scktID_, (const sockaddr *)&serverAddr, sizeof(serverAddr));
  if (iCnxErr != 0)
  {
    if (errno == EISCONN)
    {
      cout << "Client TCP socket already connected ! " << endl;
      bAlreadyConnected = true;
      return iCnxErr;
    }
  }

  return iCnxErr;
}

//-----------------------------------------------------------------------------
// Send a packet through any socket (as client or server)
//
// RETURN value
// These calls return the number of bytes received, or -1 if an error
// occurred.In the event of an error, errno is set to indicate the
// error.
//
// When a stream socket peer has performed an orderly shutdown, the
// return value will be 0 (the traditional "end-of-file" return).
// The value 0 may also be returned if the requested number of bytes to
// receive from a stream socket was 0.
//-----------------------------------------------------------------------------
int TCPSocket::Send(const char *data, unsigned int len, bool bNoneBlocking)
{
  if (bNoneBlocking)
  {
    return sendto(scktID_, data, len, 0, NULL, 0);
  }
  return send(scktID_, data, len, 0);
}

//-----------------------------------------------------------------------------
// Receive a packet (as client or server)
//
// RETURN value
// On success, these calls return the number of bytes sent.  On error,
// -1 is returned, and errno is set appropriately.
//-----------------------------------------------------------------------------
int TCPSocket::Receive(char *buffer, int len, int iFlag)
{
  //  return recv(scktID_, buffer, len, 0);
  return recv(scktID_, buffer, len, iFlag);
}

//-----------------------------------------------------------------------------
// Close socket
//-----------------------------------------------------------------------------
void TCPSocket::CloseSocket()
{
#ifdef _WIN32
  closesocket(scktID_);
#else
  close(scktID_);
#endif
}

//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
bool TCPSocket::SetNonBlocking(SOCKET socket)
{
#ifdef _WIN32
  u_long mode = 1;
  return ioctlsocket(socket, FIONBIO, &mode) == 0;
#else
  return fcntl(socket, F_SETFL, O_NONBLOCK) != -1;
#endif
}

//-----------------------------------------------------------------------------
// Retrieve human readable IP from a sochaddr_in structure
//-----------------------------------------------------------------------------
std::string TCPSocket::GetAddress()
{
  char cIP[INET6_ADDRSTRLEN];
  return inet_ntop(scktAddr_.sin_family, (void *)&(scktAddr_.sin_addr), cIP, INET6_ADDRSTRLEN);
}

//-----------------------------------------------------------------------------
// Retrieve human readable socket port
//-----------------------------------------------------------------------------
unsigned short TCPSocket::GetPort()
{
  return ntohs(scktAddr_.sin_port);
}

//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
int TCPSocket::GetError()
{
#ifdef _WIN32
  return WSAGetLastError();
#else
  return errno;
#endif
}

//-----------------------------------------------------------------------------
// Initialize socket environment (specific to windaube)
//
// Return value  ----------
// true on success
//
//-----------------------------------------------------------------------------
bool TCPSocket::Start()
{
#ifdef _WIN32
  return WSAStartup(MAKEWORD(2, 2), &wsaData_) == 0;
#else
  return true;
#endif
}

//-----------------------------------------------------------------------------
// Release socket environment (specific to windaube)
//-----------------------------------------------------------------------------
void TCPSocket::Release()
{
#ifdef _WIN32
  WSACleanup();
#endif
}

//-----------------------------------------------------------------------------
// Switch to Blocking/non blocking socket
//
// Returns true on success, or false if there was an error
//-----------------------------------------------------------------------------
bool TCPSocket::SetSocketBlockingEnabled(int fd, bool blocking)
{
  if (fd < 0)
    return false;

#ifdef _WIN32
  unsigned long mode = blocking ? 0 : 1;
  return (ioctlsocket(fd, FIONBIO, &mode) == 0) ? true : false;
#else
  int flags = fcntl(fd, F_GETFL, 0);
  if (flags == -1)
    return false;
  flags = blocking ? (flags & ~O_NONBLOCK) : (flags | O_NONBLOCK);
  return (fcntl(fd, F_SETFL, flags) == 0) ? true : false;
#endif
}