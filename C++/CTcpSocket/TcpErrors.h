#ifndef BOUSK_DVP_COURS_ERRORS_HPP
#define BOUSK_DVP_COURS_ERRORS_HPP

#pragma once

#ifdef _WIN32
	#include <WinSock2.h>
#else
	#include <cerrno>
  // Windows world INVALID_SOCKET == 0 , linux world INVALID_SOCKET == -1
  #define INVALID_SOCKET ((int)-1)  
  #ifndef SOCKET_ERROR
    #define SOCKET_ERROR (int(-1))
  #endif
#endif

namespace Sockets
{
	int GetError();
	enum class Errors {
#ifdef _WIN32
		WOULDBLOCK = WSAEWOULDBLOCK
#else
		WOULDBLOCK = EWOULDBLOCK
#endif
	};
}

#endif // BOUSK_DVP_COURS_ERRORS_HPP