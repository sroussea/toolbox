#ifndef TCP_SOCKETS_H__
#define TCP_SOCKETS_H__
/*******************************************************************************
 * Lagrange Lab - SPICA project
 *
 *
 * who       when        what
 * --------  ---------- --------------------------------------------------------
 * sroussea       2020-10-20  Creation
 *
 * This class is intended to offer TCP socket interface for both Windows 64 and
 * Linux
 *
 *
 * Remarks
 *
 * The port is not a socket member because :
 * - If the socket is a client, its port is generated randomly by the OS
 * - if the socket is a server, it's port is provided by the application
 *   and used as an input parameter for the bind() function
 *
 * Difference between sockaddr_in & sockaddr
 * https://stackoverflow.com/questions/21099041/why-do-we-cast-sockaddr-in-to-sockaddr-when-calling-bind/21099196
 *
 *
 *******************************************************************************/

//-----------------------------------------------------------------------------
// System Headers
//-----------------------------------------------------------------------------
#include <set>
#include <vector>
#include <iostream>
#include <string>   //std::string
#include <string.h> // memset
#include <stdexcept>
#include <sstream>
#include <memory> //std::shared_ptr
#include <fcntl.h>

using std::set;
using std::vector;
class TCPSocket;
using spTCPSocket = std::shared_ptr<TCPSocket>;

#ifdef _WIN32
#include <WinSock2.h>
#include <WS2tcpip.h>
#pragma comment(lib, "Ws2_32.lib")
#else
#include <sys/socket.h>
#include <netinet/in.h> // sockaddr_in, IPPROTO_TCP
#include <arpa/inet.h>  // hton*, ntoh*, inet_addr
#include <unistd.h>     // close
#include <cerrno>       // errno

#define SOCKET int

#endif

//-----------------------------------------------------------------------------
// Local Headers
//-----------------------------------------------------------------------------
#include "TcpErrors.h"

//-----------------------------------------------------------------------------
// Useful definitions
//-----------------------------------------------------------------------------
#ifdef _WIN32
// SO_DOMAIN is the standard option of the SOL_SOCKET level to retrieve the IP address
// of the socket. Windows redefined it as SO_PROTOCOL_INFO, hence this overriding.
#define SO_DOMAIN SO_PROTOCOL_INFO

#else
#endif

// Hold sock usage
enum class eSockUsage
{
  eCLIENT = 0,
  eSERVER = 1
};
using eSockUsage = enum class eSockUsage;

// Hold sock pending request
enum class eSockReq
{
  NONE = 0,
  READ = 1,
  WRITE = 2,
  EXCEPT = 3
};
using eSockReq = enum class eSockReq;

class TCPSocket
{

public:
  // (Con/De)structor
  TCPSocket(eSockUsage sockUsage, SOCKET scktID = INVALID_SOCKET, sockaddr_in *pScktAddr_ = NULL);
  ~TCPSocket();

  // Copy operator
  TCPSocket &operator=(const TCPSocket &src);
  // comparison operator for ordered containers (here set of TCPSocket)
  bool operator<(const TCPSocket &rightOp) const;

  // Accessors  ----------------------------------------------------------
  eSockReq sockRequest() const
  {
    return sockReq_;
  }

  // Public methods ----------------------------------------------------------

  // Update socket request status in the socket set member setSock_
  bool getPendingClients(set<spTCPSocket> &vClients);

  // Bind the server socket to the hardware
  int Bind(u_short usPort);
  // Set server maximum connexions
  int Listen(int iMaxCnxAccepted);
  // Look for new client connexion
  TCPSocket *acceptNewClient();
  // Wait until some sockets have thrown some R/W/ERR requests
  int Select(fd_set *readfds, fd_set *writefds, fd_set *exceptfds, struct timeval *timeout);
  // Switch to Blocking/non blocking socket
  bool SetSocketBlockingEnabled(int fd, bool blocking);

  // Connect as a client socket to a server socket
  int Connect(const std::string &strIP, unsigned short usPort, bool &bAlreadyConnected);
  // Send a packet through any socket (as client or server)
  int Send(const char *data, unsigned int len, bool bNoneBlocking = false);
  // Receive a packet (as client or server)
  int Receive(char *buffer, int len, int iFlag = 0);

  // Close socket
  void CloseSocket();
  // Set socket as non blocking
  bool SetNonBlocking(SOCKET socket);
  // Retrieve human readable IP from a sochaddr_in structure
  std::string GetAddress();
  // Retrieve human readable socket port
  unsigned short GetPort();
  // Retrieve last error number
  int GetError();

private:
  // Hidden methods ----------------------------------------------------------
  // Initialize socket environment (specific to windaube)
  bool Start();
  // Release socket environment (specific to windaube)
  void Release();
  // Accept incoming client connexion to a server socket
  SOCKET Accept(struct sockaddr *pIncomingAddr, socklen_t *pAddrlen);

  // Get max socket ID in the current process
  SOCKET maxSocketID() const
  {
    return maxSockID_;
  };

  // Attributes ----------------------------------------------------------

  eSockUsage sockUsage_;              // CLIENT | SERVER
  SOCKET scktID_ = INVALID_SOCKET;    // Socket handler
  sockaddr_in scktAddr_;              // Socket address structure to hold the incoming connexion data
  eSockReq sockReq_ = eSockReq::NONE; // Socket current reauest status, relevant only for client socket

  // Flag for the OS socket background initialization status
  static bool bStartOK_;
  // Maximum socket ID ( for select() 1st parameter )
  static SOCKET maxSockID_;
  // Instance counter
  static int iNbOfSock_;
  // Hold windows socket env data
#ifdef _WIN32
  static WSAData wsaData_;
#endif
};

#endif //  TCP_SOCKETS_H__