/*******************************************************************************
 * Lagrange Lab - SPICA project
 *
 * "@(#) $Id: CShm.cpp 307417 2018-03-08 11:37:58Z sroussea $"
 *
 * who       when        what
 * --------  ---------- --------------------------------------------------------
 * sroussea  2019-01-22  Creation
 *
 * NAME
 *   CShm  -
 *
 * SYNOPSIS
 *   Class which allows :
 *     -
 *
 * DESCRIPTION
 *
 *
 *
 *******************************************************************************/

//-----------------------------------------------------------------------------
// system  Headers
//-----------------------------------------------------------------------------
#include <stdexcept> // std::invalid_argument

//-----------------------------------------------------------------------------
// local Headers
//-----------------------------------------------------------------------------
#include "CShm.h"
#include <cstdio>
#include <cstdlib>
#include <errno.h>
#include <sys/mman.h>
#include <iostream>

#define SM_SEMA "/semaphoreInit"

//-----------------------------------------------------------------------------
// Destructor
//-----------------------------------------------------------------------------
CShm::~CShm()
{
  // unlink the SM
  //  Clear();
}

//-----------------------------------------------------------------------------
// Constructor
//-----------------------------------------------------------------------------
CShm::CShm(const string &sName) : m_sName(sName), m_iD(-1), m_SemID(NULL), m_nSize(0), m_Ptr(NULL)
{

  // Instanciate a semaphore
  string strSemaName = m_sName + string("_sem");

  m_SemID = sem_open(strSemaName.c_str(), O_CREAT, S_IRUSR | S_IWUSR, 1);
  //  m_SemID = sem_open(SM_SEMA, O_CREAT, S_IRUSR | S_IWUSR, 1);
  //  m_SemID = sem_open("/buffer_seg15_sema", O_CREAT, S_IRUSR | S_IWUSR, 1);

  if (m_SemID == NULL)
  {
    string strErr = string("Couldn't open the semaphore ") + strSemaName;
    cout << strErr << endl;
    cout << "Shm " << sName << " constructor failed !!!" << endl;
    throw std::invalid_argument(strErr.c_str());
  }

  // Heuristic semaphore unlocking : in case some previous process crashed before unlocking it
  // !!!CAVEAT :  Border effect : May release a semaphore locked by some running healthy process !!!!
  int iSemVal = 0;
  sem_getvalue(m_SemID, &iSemVal);
  while (iSemVal == 0)
  {
    sleep(1);
    sem_post(m_SemID);
    cout << "Semaphore locking detected ! Sem value = " << iSemVal << endl;
    cout << "Trying to release it..." << endl;
    sem_getvalue(m_SemID, &iSemVal);
  }
}

//-----------------------------------------------------------------------------
// Open the SM, create it if it doesn't exist
//-----------------------------------------------------------------------------
bool CShm::Create(size_t nSize, int mode /*= READ_WRITE*/)
{

  m_nSize = nSize;
  m_iD = shm_open(m_sName.c_str(), O_CREAT | mode, S_IRWXU | S_IRWXG);
  // cout << "In CShm::Create, shm_open retCode = " << m_iD << endl;

  if (m_iD < 0)
  {
    switch (errno)
    {
      case EACCES:
        throw CShmException("Permission Exception ");
        break;
      case EEXIST:
        throw CShmException("Shared memory object specified by name already exists.");
        break;
      case EINVAL:
        throw CShmException("Invalid shared memory name passed.");
        break;
      case EMFILE:
        throw CShmException("The process already has the maximum number of files open.");
        break;
      case ENAMETOOLONG:
        throw CShmException("The length of name exceeds PATH_MAX.");
        break;
      case ENFILE:
        throw CShmException("The limit on the total number of files open on the "
                            "system has been reached");
        break;
      default:
        throw CShmException("Invalid exception occurred in shared memory creation");
        break;
    }
  }

  // adjusting mapped file size (make room for the whole segment to map
  int iErr = 0;
  if (mode != C_READ_ONLY)
  {
    iErr = ftruncate(m_iD, m_nSize);
    cout << "In CShm::Create, ftruncate retCode = " << iErr << endl;
  }

  return true;
}

//-----------------------------------------------------------------------------
// Map shared memory to a local pointer
//-----------------------------------------------------------------------------
bool CShm::Attach(int mode /*= A_READ | A_WRITE*/)
{
  // requesting the shared segment
  m_Ptr = mmap(NULL, m_nSize, mode, MAP_SHARED, m_iD, 0);
  if (m_Ptr == NULL)
  {
    throw CShmException("Exception in attaching the shared memory region");
  }
  return true;
}

//-----------------------------------------------------------------------------
// Unmap the shared memory from the pointer
//-----------------------------------------------------------------------------
bool CShm::Detach()
{
  return munmap(m_Ptr, m_nSize);
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
bool CShm::Lock() const
{
  return sem_wait(m_SemID);
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
bool CShm::UnLock() const
{
  return sem_post(m_SemID);
}

//-----------------------------------------------------------------------------
// Semaphore close: Remove a named semaphore  from the system.
//-----------------------------------------------------------------------------
void CShm::semRelease()
{
  if (m_SemID != NULL)
  {
    // Semaphore Close: Close a named semaphore
    if (sem_close(m_SemID) < 0)
    {
      perror("sem_close");
    }
  }
}

//-----------------------------------------------------------------------------
// SM unlink: Remove a named semaphore from the system if no more process uses it
//-----------------------------------------------------------------------------
void CShm::Clear()
{
  if (m_iD != -1)
  {
    if (shm_unlink(m_sName.c_str()) < 0)
    {
      perror("shm_unlink : sempahore seems to have been removed yet from the system...");
    }
  }

  semRelease();
}

CShmException::CShmException(const string & /*message*/, bool /*bSysMsg = false*/) throw()
{
}

CShmException::~CShmException() throw()
{
}
