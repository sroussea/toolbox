#ifndef CShm_H__
#define CShm_H__
/*******************************************************************************
 * Lagrange Lab - SPICA project
 *
 * "@(#) $Id: CShm.h 307417 2018-03-08 11:37:58Z sroussea $"
 *
 * who       when        what
 * --------  ---------- --------------------------------------------------------
 * sroussea  2019-01-22  Creation
 *
 *******************************************************************************/

//-----------------------------------------------------------------------------
// System Headers
//-----------------------------------------------------------------------------
#include <string>
#include <sys/mman.h>
#include <sys/stat.h> /* For mode constants */
#include <sys/mman.h>
#include <fcntl.h> /* For O_* constants */
#include <semaphore.h>
#include <unistd.h>

//-----------------------------------------------------------------------------
// local Headers
//-----------------------------------------------------------------------------

using namespace std;

/**
 *   Signals a problem with the execution of a SharedMemory call.
 */

class CShmException : public std::exception
{
public:
  /**
   *   Construct a SharedMemoryException with a explanatory message.
   *   @param message explanatory message
   *   @param bSysMsg true if system message (from strerror(errno))
   *   should be postfixed to the user provided message
   */
  CShmException(const std::string &message, bool bSysMsg = false) throw();

  /** Destructor.
   * Virtual to allow for subclassing.
   */
  virtual ~CShmException() throw();

  /** Returns a pointer to the (constant) error description.
   *  @return A pointer to a \c const \c char*. The underlying memory
   *          is in posession of the \c Exception object. Callers \a must
   *          not attempt to free the memory.
   */
  virtual const char *what() const throw()
  {
    return m_sMsg.c_str();
  }

protected:
  /** Error message.
   */
  std::string m_sMsg;
};

class CShm
{
public:
  enum
  {
    C_READ_ONLY = O_RDONLY,
    C_READ_WRITE = O_RDWR,
  } CREATE_MODE;

  enum
  {
    A_READ = PROT_READ,
    A_WRITE = PROT_WRITE,
  } ATTACH_MODE;

  static const std::string sLockSemaphoreName;

public:
  CShm(const std::string &sName);
  ~CShm();

  // Open the SM, create it if it doesn't exist
  bool Create(size_t nSize, int mode = C_READ_WRITE);
  // Map the shared memory to a local pointer
  bool Attach(int mode = A_READ | A_WRITE);
  // Unmap the shared memory from the pointer
  bool Detach();
  bool Lock() const;
  bool UnLock() const;
  int GetID()
  {
    return m_iD;
  }
  void *GetData()
  {
    return m_Ptr;
  };
  //    const void *GetData() const { return m_Ptr; }
  void Clear();

  void semRelease();
  int semGetVal()
  {
    int iSemVal = 0;
    sem_getvalue(m_SemID, &iSemVal);
    return iSemVal;
  }

private:
  std::string m_sName;
  int m_iD;
  sem_t *m_SemID;
  size_t m_nSize;
  void *m_Ptr;
};

#endif // CShm_H__
