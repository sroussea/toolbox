#include "CShm.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <string>
#include <unistd.h>
#include <signal.h>

using namespace std;

CShm* pCShm = NULL;

// Signal handler
void sig_handler(int signo)
{
  if (signo == SIGINT)
    cout << "received SIGINT" << endl;
  else if (signo == SIGSEGV)
    cout << "received SIGSEGV" << endl; 
  else if (signo == SIGHUP)
    cout << "received SIGHUP" << endl; 
  else if (signo == SIGILL)
    cout << "received SIGILL" << endl; 
  else if (signo == SIGTERM)
    cout << "received SIGTERM" << endl; 
  else
  {;} 

    if (pCShm) {

        cout << "SIG HANDLER Sem value = " << pCShm->semGetVal() << endl;
    }
    
  // Transmit the signal to the kernel for default action
  signal(signo, SIG_DFL);
  kill(getpid(), signo);

  exit(0);
}

int main(int argc, char *argv[]) {
  try {
    CShm shmMemory("/testSharedmemory1");
    shmMemory.Create(100);
    shmMemory.Attach();
    char *str = (char *)shmMemory.GetData();

    pCShm = &shmMemory;

 
    // Catch usufull sigs
    signal(SIGINT, sig_handler);
    signal(SIGSEGV, sig_handler);
    signal(SIGHUP, sig_handler);
    signal(SIGILL, sig_handler);
    signal(SIGTERM, sig_handler);

 

    if (std::string(argv[1]) == "1") {
      for (int i = 0; i < 10; i++) {
        char sTemp[10];

        cout << "BEFORE LOCK  ! Sem value = " << pCShm->semGetVal() << endl;

        shmMemory.Lock();

        cout << "LOCKED PASSED ! Sem value = " << pCShm->semGetVal() << endl;
          while(1);

        sprintf(sTemp, "Data:%d", rand() % 100);
        strcpy(str, sTemp);
        printf("\nWriting:%s", str);
        shmMemory.UnLock();
        sleep(1);
      }
    } else {
      for (int i = 0; i < 10; i++) {
        char sTemp[10];
        printf("\nReading:%d", i + 1);
        shmMemory.Lock();
        printf("--->%s", str);
        shmMemory.UnLock();
        sleep(1);
      }
    }
  } catch (std::exception &ex) {
    cout << "Exception:" << ex.what();
  }
}