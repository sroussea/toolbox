#ifndef CSimpleFITS_H__
#define CSimpleFITS_H__
//---------------------------------------------------------------------------------------------------------------
/// \file CSimpleFITS.h
/// \brief CSimpleFITS class declaration
/// \section Milestones
/// who      |  when        what
/// ---------|------------|------------------------------------------------------
/// sroussea | 2020-11-06 | Creation
/// boebion  | 2021-09-07 | Learning FITS
/// boebion  | 2021-10-26 | First step to read and write data in a binary extension
/// boebion  | 2021-11-05 | Switch to Doxygen comments and documentations
/// boebion  | 2021-11-10 | Add readBinaryArrays() and writeBinaryArrays() functions
/// boebion  | 2023-07-12 | Add addPrimaryKeyword()
/// \author Olivier Boebion
/// \version 0.8
/// \date 2022/07/12
/// \copyright copyright Lagrange Lab - SPICA project
//---------------------------------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// System Headers
//-----------------------------------------------------------------------------
// #include <sys/socket.h>
// #include <netinet/in.h>
// #include <arpa/inet.h>
// #include <memory>

//-----------------------------------------------------------------------------
// local Headers
//-----------------------------------------------------------------------------
#include "localdefs.h"

/// \fn int numberOfSimpleFITS()
/// \brief Function wich return the number of CSimpleFITS object.
/// \return The number of CSimpleFITS objects.
///
int numberOfSimpleFITS();

/// \struct struct hduKeyword
/// \brief struct hduKeyword define a type for HDU key data
template <typename T> struct hduKeywordT
{
  string keywordName;
  T keywordValue;
  string keywordComment;
};

using strKeyword = struct hduKeywordT<string>;
using floatKeyword = struct hduKeywordT<float>;

//---------------------------------------------------------------------------------------------------------------
/// \class CSimpleFITS
/// \brief CSimpleFITS class provides a simple FITS file access for some basic operations.
/// \details CSimpleFITS Class allows to :
/// - open, read and write a FITS file
/// - give informations of FITS file content : number of HDUs, type of data extensions
/// - access of a given HDU : list of keynames, data
/// \section Related_links Related links
///  See https://fits.gsfc.nasa.gov/fits_home.html to understand standard data format used in astronomy.
///
///  See https://heasarc.gsfc.nasa.gov/docs/software/fitsio/fitsio.html to download and install fitsio C library
///
///  See https://heasarc.gsfc.nasa.gov/fitsio/CCfits/ to download and install C++ fitsio wrapper of fitsio C library
///
///  See Allowed column formats in a binary file (https://docs.astropy.org/en/stable/io/fits/usage/table.html)
///
/// \section Milestones
///  who      |  when      | what
/// ----------|------------|------------------------------------------------------
/// sroussea  | 2020-11-06 | Creation
/// boebion   | 2021-09-07 | Learning FITS
/// boebion   | 2021-10-26 | First step to read and write data in a binary extension
/// boebion   | 2021-11-05 | Switch to Doxygen comments and documentations
/// boebion   | 2021-11-10 | Add readBinaryArrays() and writeBinaryArrays() functions
/// boebion   | 2022-02-25 | Add description of exception in Constructor
/// boebion   | 2023-07-12 | Add addPrimaryKeyword()
/// \todo A lot of things
/// \version 0.2
/// \date 2023/07/12
/// \author Olivier Boebion
/// \copyright copyright Lagrange Lab - SPICA project
//---------------------------------------------------------------------------------------------------------------
class CSimpleFITS
{
public:
  // ( Cons / Des) tructor  -------------------------------------------------------------------

  //---------------------------------------------------------------------------------------------------------------
  /// \brief Constructor of CSimpleFITS objects
  /// \details Creates a CSimpleFITS object while open/create a FITS file. An instance of CCfits::FITS
  /// is initially created with same parameters. The number of HDU in FITS file is stored.
  /// In case of construction failure, an exception is raised and propagated to calling function.
  /// In case of success, the number of CSimpleFITS object is increased.
  /// \param str_FITSFilename Pathname of FITS file to open (string)
  /// \param rwmode FITS file open mode provided by CCFITS library (RWmode) (Read=READONLY,Write=READWRITE)
  /// \throw Throws a (char *) exception if unable to create CSimpleFITS object from FITS file
  /// \return None.
  //---------------------------------------------------------------------------------------------------------------
  CSimpleFITS(const string &str_FITSFilename, RWmode rwmode);

  //---------------------------------------------------------------------------------------------------------------
  /// \brief Default destructor
  /// \details Default destructor. The number of CSimpleFITS object (simpleFITSNumber) is decreased.
  /// \return None.
  //---------------------------------------------------------------------------------------------------------------
  ~CSimpleFITS();

  // Public facilities  -------------------------------------------------------------------

  //---------------------------------------------------------------------------------------------------------------
  /// \fn int numberOfHDU()
  /// \brief Give the number of HDU (primary and extensions) in a FITS file
  /// \details Give the number of HDU (primary and extensions) in a FITS file
  /// \return 0 if no HDU, or a positive number.
  //---------------------------------------------------------------------------------------------------------------
  int numberOfHDU();

  //---------------------------------------------------------------------------------------------------------------
  /// \fn int displayAnalyzeFITSFile()
  /// \brief Display primary and extensions type of a FITS file
  /// \details Display primary and extensions type (Image, Ascii table, binary table) of opened FITS file
  /// \return 0 if successful, or a negative/positive error code if not. This code is defined by CFITSIO library@brief
  //---------------------------------------------------------------------------------------------------------------
  int displayAnalyzeFITSFile();

  //---------------------------------------------------------------------------------------------------------------
  /// \fn int displayPHDU()
  /// \brief Display informations (keywords, comments and history) of primary HDU
  /// \details Display informations (keywords, comments and history) of primary HDU
  /// \return 0 if successful, or -1 error code if not.
  ///
  int displayPHDU();

  //---------------------------------------------------------------------------------------------------------------
  /// \fn int displayExtHDU(const int &i_hdunumber)
  /// \brief Display informations of a extension HDU by number.
  /// \details Display informations (keywords, comments and history) of a extension HDU by number.
  /// \param i_hdunumber the extension (HDU) number to display. This number begin to 1.
  /// \return 0 if successful, or -1 error code if not.
  //---------------------------------------------------------------------------------------------------------------
  int displayExtHDU(const int &i_hdunumber);

  //---------------------------------------------------------------------------------------------------------------
  /// \fn int displayExtHDU(const string &str_hduname)
  /// \brief Display informations (keywords, comments and history) of a extension HDU by name.
  /// \details Display informations (keywords, comments and history) of a extension HDU by name.
  /// \param str_hduname Name of extension to read.
  /// \return 0 if successful, or -1 error code if not.
  //---------------------------------------------------------------------------------------------------------------
  int displayExtHDU(const string &str_hduname);

  //---------------------------------------------------------------------------------------------------------------
  /// \fn int createBinaryTable(const string &str_hduname, const unsigned long &l_rows,vector<string> &str_colName,
  /// vector<string> &v_colForm, vector<string> &v_colUnit)
  /// \brief Create a new binary table extension.
  /// \details Add a new binary table extension to a FITS file with columns name, type and unit
  /// \param str_hduname : name of the extension to create.
  /// \param l_rows : number of empty rows to write.
  /// \param v_colName : vector containing name of columns.
  /// \param v_colForm : vector containing number and type of datas (respecting the FITS format).
  /// \param v_colUnit : vector containing units of datas.
  /// \return 0 if successful, or -1 error code if not.
  //---------------------------------------------------------------------------------------------------------------
  int createBinaryTable(const string &str_hduname, const unsigned long &l_rows, vector<string> &v_colName,
                        vector<string> &v_colForm, vector<string> &v_colUnit);

  //---------------------------------------------------------------------------------------------------------------
  /// \fn int createBinaryImage(const string &str_hduname,const int &i_bitpix, const vector<long> &v_extAxes, const long
  /// &l_firstPixel, const long &l_numberOfPixels, std::valarray<long> &v_PixelValue) \brief Create a new binary image
  /// extension and write image. \details Add a new binary image extension and image to a FITS file \param str_hduname :
  /// name of the extension to create. \param i_bitpix : CFITSIO type constants: BYTE_IMG, SHORT_IMG, LONG_IMG,
  /// FLOAT_IMG, DOUBLE_IMG, USHORT_IMG, ULONG_IMG, LONGLONG_IMG \param v_extAx : vector containing axes. \param
  /// l_firstPixel : first pixel of data image \param l_numberOfPixels : number of pixel \param v_PixelValue : array of
  /// data corresponding at each pixel \return 0 if successful, or -1 error code if not.
  //---------------------------------------------------------------------------------------------------------------
  int createBinaryImage(const string &str_hduname, const int &i_bitpix, vector<long> &v_extAxes, long &l_firstPixel,
                        long &l_numberOfPixels, std::valarray<float> &v_PixelValue);

  //---------------------------------------------------------------------------------------------------------------
  /// \fn template <class T> int writeBinary(const vector<T> &v_value, const string &hduname, const string &str_column)
  /// \brief Write values to a column in a binary extension (Template).
  /// \details Write values stored in a vector to a column in a binary extension (Template).
  /// \param v_value : vector name containing values to write.
  /// \param str_hduname : extension name to select.
  /// \param str_column : name of columns to write in extension.
  /// \param l_firstRow : the first row to be written.
  /// \return 0 if successful, or -1 error code if not.
  //---------------------------------------------------------------------------------------------------------------
  template <class T>
  int writeBinary(const vector<T> &v_value, const string &str_hduname, const string &str_column,
                  const long &l_firstRow);

  //---------------------------------------------------------------------------------------------------------------
  /// \fn template <class T>int writeBinaryArrays(const std::vector<std::valarray<T>>, const string &str_hduname, const
  /// string &str_column, const long &long_firstRow) \brief Write values stored in a vector of valarray to a column in a
  /// binary extension. \details Write values stored in a vector of valarray to a column in a binary extension
  /// (Template). \param v_value : vector name containing valarray values to write. \param str_hduname : extension name
  /// to select. \param str_column : name of columns to write in extension. \param l_firstRow : the first row to be
  /// written. \return 0 if successful, or -1 error code if not.
  //---------------------------------------------------------------------------------------------------------------
  template <class T>
  int writeBinaryArrays(const vector<std::valarray<T>> &v_value, const string &str_hduname, const string &str_column,
                        const long &l_firstRow);

  //---------------------------------------------------------------------------------------------------------------
  /// \fn template <class T> int readBinary(vector<T> &v_value, const string &str_hduname, const string &str_column, *
  /// const long &l_firstRow, const long &l_numberRow) \brief Read a vector of values from a binary extension. \details
  /// Read a vector of values from a column in a binary extension (Template). \param v_value : vector name where read
  /// values will be stored. \param str_hduname : name of extension to access. \param str_column : name of columns to
  /// select. \param l_firstRow : the first row to read. \param l_numberRow : the number of rows to read \return 0 if
  /// successful, or -1 error code if not.
  //---------------------------------------------------------------------------------------------------------------
  template <class T>
  int readBinary(vector<T> &v_value, const string &str_hduname, const string &str_column, const long &l_firstRow,
                 const long &l_numberRow);

  //---------------------------------------------------------------------------------------------------------------
  /// \fn   template <class T> int CSimpleFITS::readBinaryArrays(vector <std::valarray<T>> &v_value,const string
  /// &str_hduname, const string &str_column, const long &l_numberRow) \brief Read a vector of valarray from a binary
  /// extension. \details Read a vector of valarray from a binary extension (Template). \param v_value : vector name
  /// where read values will be stored. \param str_hduname : name of extension to access. \param str_column : name of
  /// columns to select. \param l_firstRow : the first row to read. \param l_numberRow : the number of rows to read
  /// \return 0 if successful, or -1 error code if not.
  //---------------------------------------------------------------------------------------------------------------
  template <class T>
  int readBinaryArrays(vector<std::valarray<T>> &v_value, const string &str_hduname, const string &str_column,
                       const long &l_firstRow, const long &l_numberRow);

  //---------------------------------------------------------------------------------------------------------------
  /// \fn int addPrimaryKeyword()
  /// \brief Add keyword data in the Primary HDU header
  /// \details Give the number of HDU (primary and extensions) in a FITS file
  /// \param hduKeyword is a struct containing 3 string : the name, the value of the keyword and a comment
  /// \return 0 if successful, or -1 error code if not.
  //---------------------------------------------------------------------------------------------------------------
  template <typename T> int addPrimaryKeyword(struct hduKeywordT<T> &newKeyword)
  {
    ccfits_.pHDU().addKey(newKeyword.keywordName, newKeyword.keywordValue, newKeyword.keywordComment);
    return (0);
  }

  // not yet implemented
  int searchKeyword(const int &i_index, const string &str_keyword);

protected:
  string strFITSFileName_; /*! File name of FITS file */
  FITS ccfits_;            /*! CCFITS FITS object */
  int iTotalHDUnb_;        /*! Total number of HDU (primary HDU + extensions) set by CFITSIO call in constructor() */
  // Hidden facilities ------------------------------------------------------------

  // void updateRows();
};

//---------------------------------------------------------------------------------------------------------------
// Template member function definitions
//---------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------

template <class T>
int CSimpleFITS::writeBinary(const vector<T> &v_value, const string &str_hduname, const string &str_column,
                             const long &l_firstRow)
{
  try
  {
    // Move to the primary header
    ccfits_.resetPosition();

    ExtHDU &table = ccfits_.extension(str_hduname);

    // Called to force the Table to reset its internal "rows" attribute.
    //   table.updateRows();

    // Write the column in file
    table.column(str_column).write(v_value, l_firstRow);

    // Test second writing to verify the gap in rows
    // table.column(str_column).write(v_value,l_firstRow);

    // Add history informations
    string hist("Data writing - Testing CCFITS write functionnality");
    table.writeHistory(hist);

    // add a comment string.
    string comment(" Learning FITS is a long way ");
    table.writeComment(comment);

    // Write buffer to file
    ccfits_.flush();
  }
  catch (const CCfits::Table::NoSuchColumn &e)
  {
    std::cerr << e.message() << '\n';
    return (-1);
  }
  return (0);
}

//---------------------------------------------------------------------------------------------------------------

template <class T>
int CSimpleFITS::writeBinaryArrays(const std::vector<valarray<T>> &v_value, const string &str_hduname,
                                   const string &str_column, const long &l_firstRow)
{
  try
  {

    ExtHDU &table = ccfits_.extension(str_hduname);

    // Called to force the Table to reset its internal "rows" attribute.
    // table::Table.updateRows();

    // table.setColumn(str_column,)

    // Write the column in file
    table.column(str_column).writeArrays(v_value, l_firstRow);

    // Add history informations
    string hist("Vector of valarray writing - Testing CCFITS write functionnality");

    table.writeHistory(hist);

    // add a comment string
    string comment(" Learning FITS is a long way ");

    table.writeComment(comment);

    // Write buffer to file
    ccfits_.flush();
  }
  catch (const CCfits::Table::NoSuchColumn &e)
  {
    std::cerr << e.message() << '\n';
    return (-1);
  }
  return (0);
}

//---------------------------------------------------------------------------------------------------------------

template <class T>
int CSimpleFITS::readBinary(vector<T> &v_value, const string &str_hduname, const string &str_column,
                            const long &l_firstRow, const long &l_numberRow)
{
  try
  {
    ccfits_.resetPosition();

    ExtHDU &table = ccfits_.extension(str_hduname);

    table.column(str_column).read(v_value, l_firstRow, l_numberRow);
  }

  catch (CCfits::Column::RangeError)
  {
    return (-1);
  }

  return (0);
}

//---------------------------------------------------------------------------------------------------------------

template <class T>
int CSimpleFITS::readBinaryArrays(vector<std::valarray<T>> &v_value, const string &str_hduname,
                                  const string &str_column, const long &l_firstRow, const long &l_numberRow)
{
  try
  {
    ccfits_.resetPosition();

    ExtHDU &table = ccfits_.extension(str_hduname);

    table.column(str_column).read(v_value, l_firstRow, l_numberRow);
  }
  catch (CCfits::Column::RangeError)
  {
    return (-1);
  }

  return (0);
}

#endif // CSimpleFITS_H__