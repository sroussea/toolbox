//---------------------------------------------------------------------------------------------------------------
///
/// \file CSimpleFITS.cpp
/// \brief CSimpleFITS class definition
/// \section Milestones
///  who      |  when      | what
/// ----------|------------|------------------------------------------------------
/// sroussea  | 2020-11-06 | Creation
/// boebion   | 2021-09-07 | Learning FITS
/// boebion   | 2021-10-26 | First step to read and write data in a binary extension
/// boebion   | 2021-11-05 | Switch to Doxygen comments and documentations
/// boebion   | 2021-11-10 | Add readBinaryArrays() and writeBinaryArrays() functions
/// boebion   | 2022-02-22 | Add exception in constructor
/// boebion  | 1022-05-16 | Add binary table and binary image extension
///
/// \author Olivier Boebion
/// \version 0.8
/// \date 2022/05/16
/// \copyright copyright Lagrange Lab - SPICA project
//---------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------
// System Headers
//---------------------------------------------------------------------------------------------------------------

//#include <sys/socket.h>
//#include <netinet/in.h>
//#include <arpa/inet.h>
//#include <memory>
#include <cassert>

//---------------------------------------------------------------------------------------------------------------
// local Headers
//---------------------------------------------------------------------------------------------------------------

#include "localdefs.h"
#include "CSimpleFITS.h"

//---------------------------------------------------------------------------------------------------------------
// Static variables init
//---------------------------------------------------------------------------------------------------------------

unsigned int simpleFITSNumber = 0; /*! Number of CSimpleFITS object, init value equal to 0 */

//---------------------------------------------------------------------------------------------------------------
/// \fn numberOfSimpleFITS()
/// \brief Return the number of CSimpleFITS object.
///
/// \param None.
/// \return The number of CSimpleFITS objects as a integer (int)
//---------------------------------------------------------------------------------------------------------------
int numberOfSimpleFITS()
{
  return (simpleFITSNumber);
}

//---------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------
/// CSimpleFITS implementation
//---------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------
/// ( Cons / Des) trutor
//---------------------------------------------------------------------------------------------------------------

CSimpleFITS::CSimpleFITS(const string &strFITSFileName, RWmode rwmode) : ccfits_(strFITSFileName, rwmode)
{
  // Keep the name of file name in private member variable
  strFITSFileName_ = strFITSFileName;

  int iStatus = 0;            // Status used by CFITSIO call MUST be initialized to zero!
  fitsfile *pFitsFile = NULL; // FITS file pointer, defined in CFITSIO fitsio.h

  // Set CFITSIO fitsfile pointer for this FITS object
  pFitsFile = ccfits_.fitsPointer();
  if (pFitsFile != NULL)
  {
    // Use CFITSIO call to get the number of HDU
    fits_get_num_hdus(pFitsFile, &iTotalHDUnb_, &iStatus);

    // If fits_get_num_hdus return an error in iStatus OR ccfits_.fitsPointer() didnt return a non-NULL address
    // Throw an exception from CSimpleFITS constructor
    if ((iStatus != 0))
    {
      string strErrorMessage = string("Failed to get the number of HDU from ") + strFITSFileName +
                               string("fits file, CSimpleFITS construstion aborted !");
      throw(FITS::OperationNotSupported(strErrorMessage));
    }
  }
  else
  {
    throw(FITS::OperationNotSupported(string("Failed to get the internal ccfits pointer from ") + strFITSFileName +
                                      string("fits file, CSimpleFITS construstion aborted !")));
  }

  // One more CsimpleFITS object => +1 in static variable counter
  simpleFITSNumber++;
}

//---------------------------------------------------------------------------------------------------------------

CSimpleFITS::~CSimpleFITS()
{
  simpleFITSNumber--;
}

//---------------------------------------------------------------------------------------------------------------
/// Public facilities
//---------------------------------------------------------------------------------------------------------------

int CSimpleFITS::displayAnalyzeFITSFile()
{
  int iStatus = 0;            // CFITSIO status value MUST be initialized to zero!
  fitsfile *pFitsFile = NULL; // FITS file pointer, defined in CFITSIO fitsio.h
  int i_hdupos;               // HDU position in FITS file used by CFITSIO call
  int i_hdutype;              // HDU type (IMAGE_HDU, ASCII_TBL, BINARY_TBL) used by CFITSIO call

  cout << endl << "*****    Display FITS file informations    *****" << endl;
  cout << "File : " << strFITSFileName_ << endl;

  // Reset HDU position to primaryHDU
  ccfits_.resetPosition();
  // Set CFITSIO fitsfile pointer for this FITS object
  pFitsFile = ccfits_.fitsPointer();

  // Use CFITSIO call to get the HDU index number
  fits_get_hdu_num(pFitsFile, &i_hdupos);

  for (; i_hdupos <= iTotalHDUnb_; i_hdupos++) // Loop for each HDU
  {
    fits_get_hdu_type(pFitsFile, &i_hdutype, &iStatus); // Get the HDU type
    cout << endl << "HDU number : " << i_hdupos << endl;
    cout << "------------------" << endl;
    switch (i_hdutype)
    {
      case IMAGE_HDU:
        if (i_hdupos == 1)
          cout << "HDU Type : Primary HDU" << endl;
        else
          cout << "HDU Type : IMAGE" << endl;
        break;
      case ASCII_TBL:
        cout << "HDU Type : ASCII Table" << endl;
        break;
      case BINARY_TBL:
        cout << "HDU Type : BINARY Table" << endl;
        break;
      default:
        cout << "HDU Type : Unknown" << endl;
        break;
    }
    cout << "------------------" << endl;
    if (i_hdupos < iTotalHDUnb_)
      fits_movrel_hdu(pFitsFile, 1, NULL, &iStatus); /* try move to next ext */
  }

  if (iStatus) /* print any error messages */
  {
    fits_report_error(stderr, iStatus);
    return (iStatus);
  }
  return (0);
}

//---------------------------------------------------------------------------------------------------------------

int CSimpleFITS::numberOfHDU()
{
  return (iTotalHDUnb_);
}

//---------------------------------------------------------------------------------------------------------------

int CSimpleFITS::displayPHDU()
{
  cout << endl << "*****    Display PHDU keywords and comments    *****" << endl;

  try
  {

    PHDU &image = ccfits_.pHDU();

    // read all user-specifed, coordinate, and checksum keys in the image
    image.readAllKeys();
    // image.read(contents);
    // this doesn't print the data, just header info.
    std::cout << image << std::endl;
    return (0);
  }

  catch (const FITS::CantOpen e)
  {
    std::cerr << e.message() << '\n';
    return (-1);
  }
}

//---------------------------------------------------------------------------------------------------------------

int CSimpleFITS::displayExtHDU(const int &i_hdunumber)
{
  try
  {
    // If there is only a primary HDU => no extension HDU
    if (iTotalHDUnb_ == 1)
      return (-1);

    cout << endl << "** Display FITS ExtHDU informations (ExtHDU number " << i_hdunumber << ") **" << endl << endl;

    ccfits_.resetPosition();

    ExtHDU &table = ccfits_.extension(i_hdunumber);

    // read all the keywords, excluding those associated with columns.
    table.readAllKeys();

    // print the result.
    std::cout << table << std::endl;
  }

  catch (const FITS::NoSuchHDU e)
  {
    std::cerr << e.message() << '\n';
    return (-1);
  }
  return (0);
}

//---------------------------------------------------------------------------------------------------------------

int CSimpleFITS::displayExtHDU(const string &str_hduname)
{
  try
  {
    // If there is only a primary HDU => no extension HDU
    if (iTotalHDUnb_ == 1)
      return (-1);

    ccfits_.resetPosition();

    ExtHDU &table = ccfits_.extension(str_hduname);

    // read all the keywords, excluding those associated with columns.
    table.readAllKeys();

    // print the result.
    std::cout << table << std::endl;
  }
  catch (const FITS::NoSuchHDU &e)
  {
    std::cerr << e.message() << '\n';
    return (-1);
  }
  return (0);
}

//---------------------------------------------------------------------------------------------------------------

int CSimpleFITS::createBinaryTable(const string &str_hduname, const unsigned long &l_rows, vector<string> &v_colName,
                                   vector<string> &v_colForm, vector<string> &v_colUnit)
{
  try
  {
    // Create an extension "BinaryTbl"
    ccfits_.addTable(str_hduname, l_rows, v_colName, v_colForm, v_colUnit, BinaryTbl);

    // Write the new extension to file
    ccfits_.flush();

    // Insert 3 empty rows
    // newTable->insertRows(1,3);

    //##SR : commented out but still there for example purpose  ---------------------------------
    // Add history informations
    string hist("Learning CCfits - ");
    hist += " The binary table extension is created now.";
    // newTable->writeHistory(hist);

    // add a comment string. Use std::string method to change the text in the message
    // and write the previous junk as a comment.
    string comment(" It is the first comment ");
    // newTable->writeComment(comment);
    //##SR : commented out but still there for example purpose  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    // Write buffer to file
    ccfits_.flush();
  }
  catch (const CCfits::Table::NoSuchColumn &e)
  {
    std::cerr << e.message() << '\n';
    return (-1);
  }
  return (0);
}

//---------------------------------------------------------------------------------------------------------------

int CSimpleFITS::createBinaryImage(const string &str_hduname, const int &i_bitpix, vector<long> &v_extAxes,
                                   long &l_firstPixel, long &l_numberOfPixels, std::valarray<float> &v_PixelValue)
{
  try
  {
    // Create an extension "Binary Image"
    ExtHDU *newBinaryImageExt = ccfits_.addImage(str_hduname, i_bitpix, v_extAxes);

    // Write pixel values
    newBinaryImageExt->write(l_firstPixel, l_numberOfPixels, v_PixelValue);

    // Write the new extension to file
    ccfits_.flush();

    // Add history informations
    string hist("Learning CCfits - ");
    hist += " The binary image extension is created now.";
    newBinaryImageExt->writeHistory(hist);

    // add a comment string. Use std::string method to change the text in the message
    // and write the previous junk as a comment.
    string comment(" It is the first comment ");
    newBinaryImageExt->writeComment(comment);

    // Write buffer to file
    ccfits_.flush();
  }
  catch (const CCfits::Table::NoSuchColumn &e)
  {
    std::cerr << e.message() << '\n';
    return (-1);
  }
  catch (const CCfits::ExtHDU::WrongExtensionType &e)
  {
    std::cerr << e.message() << '\n';
    return (-1);
  }
  return (0);
}

