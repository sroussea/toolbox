/**
* \brief Test program for CSimpleFITS class
* \section Introduction
* tstCcfitsio.cpp is a test program for CSimpleFITS Class. 
* 
* Some FITS files are available in FileExamples directory.
*   
* \section how_to_run How to run the test program
* 
* - download and install fitsio C library
* - download and install C++ fitsio wrapper of fitsio C library
* - set str_workingDirectory and str_fitsFileDirectory in tstCcfitio.cpp with the path and directory name containing the FITS files.
* - set the include path in GNUmakefile and run make
* - execute tstCcfitsio binary
* 
* \section Related_links Related links
*  See https://fits.gsfc.nasa.gov/fits_home.html to understand standard data format used in astronomy.
*  
*  See https://heasarc.gsfc.nasa.gov/docs/software/fitsio/fitsio.html to download and install fitsio C library
* 
*  See https://heasarc.gsfc.nasa.gov/fitsio/CCfits/ to download and install C++ fitsio wrapper of fitsio C library
* 
* \section milestones Milestones 
* who       | when       | what
* ----------|------------|------------------------------------------------------
* sroussea  | 2020-11-06 | Creation
* boebion   | 2021-09-07 | Learning FITS
* boebion   | 2021-10-26 | First step to read and write data in a binary extension
* boebion   | 2021-11-05 | Switch to Doxygen comments and documentations
*
* \author Olivier Boebion
* \version 0.2
* \date 2021/11/05 
* \copyright copyright Lagrange Lab - SPICA project
*/

//-----------------------------------------------------------------------------
// System Headers
//-----------------------------------------------------------------------------
#include <iostream>
#include <fstream>
#include <cmath>
#include <dirent.h>

/*! \namespace std
* 
* Standard namespace
*/
using namespace std;

//Our CSimpleFITS Class
#include "CSimpleFITS.h"

// where to find FITS files
string str_workingDirectory("/home/boebion/Code/CSimpleFITS/csimplefits");
string str_fitsFileDirectory = str_workingDirectory + "/FileExamples";

// Select a FITS file in the main() function
string str_chooseFITSFile(const string &str_fitsFileDirectory);

int main()
{

    //-----------------------------------------------------------------------------
    // Display a menu to choose a FITS file to read 
    //-----------------------------------------------------------------------------        

    //Name of FITS file to open and read 
    std::string str_chosenFITSFile = "None";

    //menu entry
    char char_menuChoice;
    
    do
    {
        cout << endl << endl << "** Open,read and write FITS files **" << endl;
        cout << endl << "\tChosen FITS file: " << str_chosenFITSFile << endl;
        cout << endl << "\tNumber of actual CSimpleFITS objects: " << numberOfSimpleFITS() << endl << endl;
        cout << "\t1 - Choose a FITS file" << endl;
        cout << "\tG - Go" << endl << endl;
        cout << "Your choice : ";
    
        cin >> char_menuChoice; 

        switch (char_menuChoice)
        {
            case '1':
            {
            //Choice of FITS file in fitFileDirectory
            str_chosenFITSFile = str_chooseFITSFile(str_fitsFileDirectory);
            }
            break;
        }
    }
    while(char_menuChoice!='G');

//-----------------------------------------------------------------------------------------------------------------------   
    
    // Verify the number of CSimpleFITS instance (normally, it's one) 
    // cout << endl << "\tNumber of CSimpleFITS objects: " << numberOfSimpleFITS() << endl << endl;

    //-----------------------------------------------------------------------------
    //  HDU file selection and display FITS informations 
    //-----------------------------------------------------------------------------
    
    cout << endl << "\tLet's go" << endl;
    cout << endl << "******************* Read informations of primary HDU and extensions *****************************" << endl;


    // Open FITS file in read mode
    CSimpleFITS myFITSFileIn(str_chosenFITSFile,Read);

    // Display number of CSimpleFITS object
    cout << endl << "\tNumber of CSimpleFITS objects: " << numberOfSimpleFITS() << endl;

    // Display number of HDU (aka extensions)
    cout << "\tNumber of HDUs (set by class constructor) : " << myFITSFileIn.numberOfHDU() << endl << endl;

    // Display number of CSimpleFITS object
    // cout << endl << "\tNumber of CSimpleFITS objects: " << numberOfSimpleFITS() << endl << endl;
    
    // Display FITS file informations
    myFITSFileIn.displayAnalyzeFITSFile();
    
    // Display PHDU keywords and comments
    myFITSFileIn.displayPHDU();
    
//-----------------------------------------------------------------------------------------------------------------------   
  
    // Next step : read an extension (HDU) and binary table column
    cout << endl << "**************************** Read an extension *****************************************" << endl;

    //-----------------------------------------------------------------------------
    // Binary HDU data reading
    //-----------------------------------------------------------------------------

    // Open FITS file in read mode
    // string testReadBinaryFile = "/home/boebion/Code/CSimpleFITS/csimplefits/FileExamples/binarytableMELO.fits";
    string testReadBinaryFile = "/home/boebion/Code/CSimpleFITS/csimplefits/FileExamples/EUVEngc4151imgx.fits";
    CSimpleFITS myFITSBinaryFile(testReadBinaryFile,Read);

    // Display number of CSimpleFITS object
    // cout << endl << "\tNumber of CSimpleFITS objects: " << numberOfSimpleFITS() << endl << endl;

    // Display FITS file informations
    // myFITSBinaryFile.displayAnalyzeFITSFile();

    // Display PHDU keywords history and comments
    // myFITSBinaryFile.displayPHDU();

    cout << endl << "**************************** Read extension number \"1\" ***************"  << endl;
    // Display HDU by number (7 for example)
    myFITSBinaryFile.displayExtHDU(7);
    
    cout << endl << "**************** Read extension \"ds_limits\" ***************" << endl;
    // Display HDU by name (ds_limit, for example)
    myFITSBinaryFile.displayExtHDU("ds_limits");

    cout << endl << "**************************** Read data from a binary table *****************************************" << endl;

    // Read a binary table
    vector <float> dataReadFromHDU;

    myFITSBinaryFile.readBinary(dataReadFromHDU,"ds_limits", "LOW",1,3);

    cout << "Data read from FITS file \"" << testReadBinaryFile << "\" in HDU \"ds_limits\" and column \"LOW\" are : ";

    for(int i; i<(int)dataReadFromHDU.size(); i++)
    {
        cout << dataReadFromHDU[i] << " " << flush;
    }
    cout << "\n" << endl;

    
    // Display number of CSimpleFITS object
    // cout << endl << "\tNumber of CSimpleFITS objects: " << numberOfSimpleFITS() << endl << endl;  
    
//-----------------------------------------------------------------------------------------------------------------------   

    // Next step : writing data in a binary table
    cout << endl << "**************************** write data in a binary table *********************************" << endl;

    //-----------------------------------------------------------------------------
    // Binary HDU data writing
    //-----------------------------------------------------------------------------
    
    // Open FITS file in write mode with reset (!)
    string testFile = "/home/boebion/Code/CSimpleFITS/csimplefits/FileExamples/test.fits";
    CSimpleFITS myFITSFileOut(("!" + testFile),Write);

    // cout << endl << "\tNumber of CSimpleFITS objects: " << numberOfSimpleFITS() << endl << endl;  

    // Display FITS file informations
    // myFITSFileOut.displayAnalyzeFITSFile()(testFile);

    // Display PHDU keywords history and comments
    // myFITSFileOut.displayPHDU();

    // Name of extension to create 
    string hduName("Learning");

    // Extension will contain 2 columns with a name, a form (number of data and type) and a unity
    vector<string> colName(2, "");
    vector<string> colForm(2, "");
    vector<string> colUnit(2, "");

    colName[0] = "numbers";
    colName[1] = "cosinus";
    colForm[0] = "1I";  // One int value
    colForm[1] = "100D"; // One hundred of double values
    colUnit[0] = "none"; // Unity 
    colUnit[1] = "none"; // Unity

    // No line when the extension will be created
    unsigned long rows(1);

    // Creation of binary table extension
    myFITSFileOut.createBinaryTable(hduName,rows,colName,colForm,colUnit);

    // Create 3 random integer between 0 to 32767
    vector <int> randomInt(3);
    srand (time(NULL)); // Seed initialization for rand() function
    for (int i = 0;i<3;++i) 
    {
        randomInt[i] = rand() % 32768 ;
    }

    // add the value of randomInt in extension "Learning" column "numbers" beginning at first row 
    myFITSFileOut.writeBinary(randomInt,"Learning","numbers",1);

    cout << endl << "Writing \"1 integer in 3 rows\" done in : " << testFile << endl;

    // add the value of randomInt in extension "Learning" column "numbers" beginning at 4th row
    myFITSFileOut.writeBinary(randomInt,"Learning","numbers",4);

    cout << endl << "Writing \"1 integer in 3 rows\" done in : " << testFile << endl;

    // Create 100 random double values in a valarray
    valarray <double> randomCosinus(100);
    for (int i = 0; i<100; ++i) randomCosinus[i] = cos(i/100.0) + 0.2*rand();

    // Create a vector of 3 void valarray of double
    vector <std::valarray<double>> vect_cosinus(3);

    // Add the same valarray values in 3vector
    for (int i = 0;i<3;++i) 
    {
        vect_cosinus.push_back(randomCosinus);
    }

    // Add the value of vector of valarray "vecto_cosinus" in extension "Learning" column "cosinus" at first row 1
    myFITSFileOut.writeBinaryArrays(vect_cosinus,"Learning","cosinus",1);

    // Add the value of vector of valarray "vecto_cosinus" in extension "Learning" column "cosinus" at row 4
    myFITSFileOut.writeBinaryArrays(vect_cosinus,"Learning","cosinus",4);

    // Display FITS file informations
    // myFITSFileOut.displayAnalyzeFITSFile();

    cout << endl << "Writing \"1 vector of 100 double in 3 rows\" done in : " << testFile << endl;
    
    exit(EXIT_SUCCESS); // EXIT_SUCCESS or EXIT_FAILURE for OS compatibility
}
//-----------------------------------------------------------------------------------------------------------------------
// Local functions 

/**
* \fn string str_chooseFITSFile(const string & str_fitsFileDirectory)
* \brief Give a full path file name
*
* \param str_fitsFileDirectory the full path of a directory
* \return A string containing the full path name of chosen file
*/   
string str_chooseFITSFile(const string & str_fitsFileDirectory)
{    
    //String to const char*; necessary to use opendir()
    const char *fitsdir = str_fitsFileDirectory.c_str();

    // 
    DIR *directory = NULL;
    struct dirent* dirEntry = NULL;

    // file name to open
    string filename;

    cout << "** Choose a FITS file **" <<  endl;

    // Try to open the directory 
    if ((directory = opendir(fitsdir))== NULL)
    {
        // Unable to open directory (bad path, directory permissions, ... ?)
        cout << endl << "Error : unable to open dir " << fitsdir << endl;
        return("None");
    }  

    cout << endl << "Available files in \"" << fitsdir << "\" :" << endl << endl;

    // Display all files and directories in directory  
    while ((dirEntry = readdir(directory)) != NULL)
    {
        // Display Regular files only (not . and .. directories)
        if (dirEntry->d_type == DT_REG) cout << dirEntry->d_name << " ";
    }
    cout << endl;

    // Ask for a file name
    cout << endl << "Which file name to open ? : ";
    cin >> filename ;
    
    // Concatenate FITS directory and file name and obtain a full path file name to open
    filename = str_fitsFileDirectory + "/" + filename;
    
    return(filename);
}
