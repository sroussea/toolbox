#!/usr/bin/make
# $Id: GNUmakefile 439 2014-12-17 11:22:14Z Sylvain Rousseau $
# PREREQUISITES :
# -o- 

DEBUG:=yes


# Source, Executable, Includes, Library Defines
INCPATH   :=  ../include/ \
			/usr/local/include/ \
			/usr/local/include/CCfits/ \
			/usr/include/boost/ \
			$(TOOL_SRC)/C++/CmdLine/ \
			$(MIRCX4SPICA_SRC)/ \
			$(MIRCX4SPICA_SRC)/control/ \
			$(SPICA_SRC)/Toolbox/time/include/ \
			$(SPICA_SRC)/Toolbox/CShmRingBuf/ \
			$(SPICA_SRC)/btNice/btCommon/ 

INCL  := $(patsubst %,-I%, $(INCPATH))		  

SRC    := $(wildcard ../src/*.cpp)
				  

target1 := tstCcfitsIo
target2 := tstCcfitsIo-menu
target3 := createFitsHeader
 
targPrefix := ./bin/

OBJ1    := $(SRC:.cpp=.o) $(target1).o
OBJ2    := $(SRC:.cpp=.o) $(target2).o
OBJ3	:= $(SRC:.cpp=.o) $(target3).o
LIBS   := -lpthread -lrt  -lm  -lCCfits -lcfitsio
EXEC1   :=  $(targPrefix)$(target1) 
EXEC2   :=  $(targPrefix)$(target2)
EXEC3   :=  $(targPrefix)$(target3)

# Generate a list of .d files corresponding to each .cpp files contained in the project
HDR_DEP := $(patsubst %.cpp,%.d,$(SRC)) $(target1).d 

# Compiler, Linker Defines
CFLAGS   :=  
ifeq ($(DEBUG), yes)
CXXFLAGS := $(CFLAGS) -g -std=c++17  -W -Wall $(INCL) -Woverloaded-virtual  -pipe -DGNU_GCC  
else
CXXFLAGS := $(CFLAGS) -std=c++17 -O2 -W -Wall $(INCL) -Woverloaded-virtual -pipe  
endif
CXX            := g++
LIBPATH        := -L/usr/local/lib  
LDFLAGS        :=  $(LIBPATH) $(LIBS) 
RM             := /bin/rm -f

# all : this target defines the set of executable to produce
all:$(EXEC1) $(EXEC2) $(EXEC3)

# include the %.d files ( "-" means don't complain if .d files does not exist yet
# which is the case at the first processing of this makefile). All the .d files
# will be generated @ the compilation step (-MMD flag)
-include $(HDR_DEP)

# Inference rules to compile and Assemble Cpp Source Files into Object Files
# $@ : target name ; $^ : dependancies list ; $< : 1st dependancy name
# -MMD : automatically create the $@.d dependancy file based upon the currently 
# compiled $@.cpp (based on its includes) 
# https://gcc.gnu.org/onlinedocs/gcc/Preprocessor-Options.html
# -MP : generate a forcing target, see
# https://www.gnu.org/software/make/manual/html_node/Force-Targets.html
%.o: %.cpp GNUmakefile
	@echo  "compiling $@  -------------------------------------------"
	$(CXX) $(CXXFLAGS) -MMD -MP -c $< -o $@ 

# Link all Object Files with external Libraries into Binaries
# Be careful to the .o and .a order : first the .o which call functions,
#  then .o or .a which contains these functions code
$(EXEC1): $(OBJ1)  
	@echo "Create the bin directory if needed --------------------"
	$(shell  [ ! -d ../bin ] && mkdir ../bin)	
	@echo "linking $@  -------------------------------------------"
	$(CXX) -o $@ $(OBJ1) $(LDFLAGS)  
 
$(EXEC2): $(OBJ2)  
	@echo "Create the bin directory if needed --------------------"
	$(shell  [ ! -d ../bin ] && mkdir ../bin)	
	@echo "linking $@  -------------------------------------------"
	$(CXX) -o $@ $(OBJ2) $(LDFLAGS) 

$(EXEC3): $(OBJ3)  
	@echo "Create the bin directory if needed --------------------"
	$(shell  [ ! -d ../bin ] && mkdir ../bin)	
	@echo "linking $@  -------------------------------------------"
	$(CXX) -o $@ $(OBJ3) $(LDFLAGS) 

# .PHONY : special target systematically rebuilt, even if dependencies are older
# clean has no dependancy, if a file named clean exists, it will be necessarily
# considered up to date. Make clean won't be rebuilt ever.
# We should set it as .PHONY
.PHONY :  clean 

# Deploy the binary
install : 
	# Check whether the deployment directory env var exists
	$(if $(SPICA_BT_BINS),$(shell echo SPICA_BT_BINS=$(SPICA_BT_BINS)), $(error ERROR : SPICA_BT_BINS env var not set, installation aborted...!) )
	# Create the deployment directory if needed
	$(shell  [ ! -d $(SPICA_BT_BINS) ] && mkdir $(SPICA_BT_BINS))
	# Copy the binary on target
	cp $(EXEC1)  $(SPICA_BT_BINS)

# Clean Up Objects, Executables, Dumps out of source directory
clean:
	$(RM) $(OBJ1) $(OBJ2) $(OBJ3) $(EXEC1) $(EXEC2) $(EXEC3) core* a.out  $(HDR_DEP)