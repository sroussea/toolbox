//---------------------------------------------------------------------------------------------------------------
/// \mainpage createFitsHeader - main program
/// \brief Test program with CSimpleFITS class - add keyword in pHDU
/// \section Introduction
/// createFitsHeader.cpp is a test program creating FITS header information with CSimpleFITS Class.
/// 
/// Some FITS files are available in FileExamples directory.
///
/// \section how_to_run How to run the test program
///
/// - download and install fitsio C library
/// - download and install C++ fitsio wrapper of fitsio C library
/// - set str_workingDirectory and str_fitsFileDirectory in tstCcfitio.cpp with the path and directory name containing
/// the FITS files.
/// - set the include path in GNUmakefile and run make
/// - execute createFitsHeader binary
///
/// \section Related_links Related links
///  See https://fits.gsfc.nasa.gov/fits_home.html to understand standard data format used in astronomy.
///
///  See https://heasarc.gsfc.nasa.gov/docs/software/fitsio/fitsio.html to download and install fitsio C library
///
///  See https://heasarc.gsfc.nasa.gov/fitsio/CCfits/ to download and install C++ fitsio wrapper of fitsio C library
///
/// \section milestones Milestones
/// who      | when       | what
/// ---------|------------|------------------------------------------------------
/// boebion  | 2023-07-11 | Creation
///
/// \author Olivier Boebion
/// \version 1
/// \date 2023/07/12
/// \copyright copyright Lagrange Lab - SPICA project
//---------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------
// System Headers
//---------------------------------------------------------------------------------------------------------------
#include <cmath>
#include <dirent.h>
#include <fstream>
#include <iostream>

/// \namespace std
///
/// Standard namespace
///
using namespace std;

// Our CSimpleFITS Class
#include "CSimpleFITS.h"

// Pi
double dPI = 2 * asin(1);

// where to find FITS files
string str_workingDirectory("/home/boebion/Code/CSimpleFITS/csimplefits");
string str_fitsFileDirectory = str_workingDirectory + "/FileExamples";
// Name of FITS file to create
std::string str_chosenFITSFile = "dummyFitsHeaderFile.fits";

// Some function forward declarations
int createFitsFileHeader(string &strFileName,hduKeyword &newKeyword);
int writeFitsBinaryTable(string &strFileName);
int displayFitsFileHeader(string &strFileName);

int main()
{
  // return value
  int iErr = EXIT_SUCCESS;
  
  // The keyword to add
  struct hduKeyword newKeyword;

  cout << endl << endl << "** Create a FITS file, write header data and a binary table**" << endl;

  // Merge FITS directory and file name and obtain a full path file name to open
  string strFileName = str_fitsFileDirectory + "/" + str_chosenFITSFile;

  // Define the keyword data
  newKeyword.keywordName = "TELESCOPE";
  newKeyword.keywordValue = "UT1";
  newKeyword.keywordComment = "Origin of data";

  iErr = createFitsFileHeader(strFileName, newKeyword);
  if (iErr != 0)
  {
    cout << "writeFitsHeader() execution upon " << strFileName << " failed !" << endl;
    exit(iErr);
  }

  iErr = writeFitsBinaryTable(strFileName);
  if (iErr != 0)
  {
    cout << "writeFitsBinaryTable() execution upon " << strFileName << " failed !" << endl;
    exit(iErr);
  }

  iErr = displayFitsFileHeader(strFileName);
  if (iErr != 0)
  {
    cout << "displayFitsFileHeader() execution upon " << strFileName << " failed !" << endl;
    exit(iErr);
  }

  exit(iErr);
}

//---------------------------------------------------------------------------------------------------------------
// Local functions
//---------------------------------------------------------------------------------------------------------------

/// \fn int createFitsFileHeader(string &strFileName)
/// \brief Create a FITS file with primary HDU header Data
///
/// \param strFileName the file name to be created
/// \return Error as an int => 0 == success
//-----------------------------------------------------------------------------------------------------------------------
int createFitsFileHeader(string &strFileName,hduKeyword &newKeyword)
{
  int iErr = EXIT_SUCCESS;

  try
  {
    // Binary HDU data writing
    // Open FITS file in write mode with reset (!)
    CSimpleFITS myFITSFileOutBinaryTable(("!" + strFileName), Write);

    myFITSFileOutBinaryTable.addPrimaryKeyword(newKeyword);

    return iErr;
  }
  catch (const char *e)
  {
    std::cerr << e << "Unable to create CSimpleFITS object from file\n";
    iErr = EXIT_FAILURE;
  }
  return iErr;
}


/// \fn int writeFitsBinaryTable(string &strFileName)
/// \brief Write a binary table in a existing FITS file
///
/// \param strFileName the file name to be created
/// \return Error as an int => 0 == success
//-----------------------------------------------------------------------------------------------------------------------
int writeFitsBinaryTable(string &strFileName)
{
  int iErr = EXIT_SUCCESS;

  try
  {
    // Binary HDU data writing
    CSimpleFITS myFITSFileOutBinaryTable(strFileName, Write);

    // Name of extension to create
    string hduName("Learning binary table extension");

    // Extension will contain 2 columns with a name, a form (number of data and type) and a unity
    vector<string> colName(2, "");
    vector<string> colForm(2, "");
    vector<string> colUnit(2, "");

    colName[0] = "numbers";
    colName[1] = "cosinus";
    colForm[0] = "1I";   // One int value
    colForm[1] = "100D"; // One hundred of double values
    colUnit[0] = "none"; // Unity
    colUnit[1] = "none"; // Unity

    // Example with no data line when the extension will be created
    unsigned long rows(1);

    cout << endl << "**************************** Create a binary table *********************************" << endl;

    // Creation of binary table extension
    iErr = myFITSFileOutBinaryTable.createBinaryTable(hduName, rows, colName, colForm, colUnit);
    if (iErr != 0)
    {
      cout << "createBinaryTable(): myFITSFileOutBinaryTable.createBinaryTable() failed, aborting... " << endl;
      return iErr;
    }
    //----------------------------------------------------------------------------------------------------
    // CREATING DUMMY DATA -------------------------------------------------------------------------------
    // Create 3 random integer between 0 to 32767
    vector<int> randomInt(3);
    srand(time(NULL)); // Seed initialization for rand() function
    for (int i = 0; i < 3; ++i)
    {
      //  randomInt[i] = rand() % 32768;
      randomInt[i] = 2 * i + 1;
    }

    // Create 100 random double values in a valarray, add the valarray into a vector<valarray>
    int iValNb = 100;
    valarray<double> randomCosinus(iValNb);
    vector<std::valarray<double>> vect_cosinus;
    for (int iCurv = 0; iCurv < 3; iCurv++)
    {
      // Create a val array
      for (int i = 0; i < iValNb; ++i)
      {
        randomCosinus[i] = iCurv + cos(4 * dPI * i / iValNb) + 0.2 * rand() / RAND_MAX;
      }
      // Add the same valarray values in 3vector
      vect_cosinus.push_back(randomCosinus);
    }

    //----------------------------------------------------------------------------------------------------
    // FILING DUMMY DATA ---------------------------------------------------------------------------------
    cout << endl
         << "**************************** write data in a binary table *********************************" << endl;

    // add the value of randomInt in extension "Learning" column "numbers" beginning at first row
    iErr = myFITSFileOutBinaryTable.writeBinary(randomInt, hduName, "numbers", 1);
    if (iErr != 0)
    {
      cout << "writeFitsBinary : myFITSFileOutBinaryTable.writeBinary() failed, aborting... " << endl;
      return iErr;
    }

    // Add the value of vector of valarray "vecto_cosinus" in extension "Learning" column "cosinus" beginning at first
    // row
    // 1
    iErr = myFITSFileOutBinaryTable.writeBinaryArrays(vect_cosinus, hduName, "cosinus", 1);
    if (iErr != 0)
    {
      cout << "writeBinaryArrays() : myFITSFileOutBinaryTable.writeBinaryArrays() failed, aborting... " << endl;
      return iErr;
    }

    cout << endl << "Writing \"1 int and 1 vector of 100 double in 3 rows\" done in : " << strFileName << endl;

    cout << endl
         << "**************************** write data in a binary table *********************************" << endl;

    // add the value of randomInt in extension "Learning" column "numbers" beginning at first row
    iErr = myFITSFileOutBinaryTable.writeBinary(randomInt, hduName, "numbers", 4);
    if (iErr != 0)
    {
      cout << "writeBinary() : myFITSFileOutBinaryTable.writeBinary() failed, aborting... " << endl;
      return iErr;
    }

    // Add the value of vector of valarray "vecto_cosinus" in extension "Learning" column "cosinus" at first row 1
    iErr = myFITSFileOutBinaryTable.writeBinaryArrays(vect_cosinus, hduName, "cosinus", 4);
    if (iErr != 0)
    {
      cout << "writeBinaryArrays : myFITSFileOutBinaryTable.writeBinaryArrays() failed, aborting... " << endl;
      return iErr;
    }

    cout << endl << "Writing \"1 int and 1 vector of 100 double in 3 new rows\" done in : " << strFileName << endl;
    return iErr;
  }
  catch (const char *e)
  {
    std::cerr << e << "Unable to create CSimpleFITS object from file\n";
    iErr = EXIT_FAILURE;
  }
  return iErr;
}


//---------------------------------------------------------------------------------------------------------------
/// \fn int displayFitsFileHeader(string &strFileName)
/// \brief Display a FITS file primary header
///
/// \param strFileName the file name to be readout
/// \return Error as an int => 0 == success
//---------------------------------------------------------------------------------------------------------------
int displayFitsFileHeader(string &strFileName)
{
  int iErr = EXIT_SUCCESS;
  //-----------------------------------------------------------------------------------------------------------------------

  cout << endl
       << "******************* Read informations of primary HDU and extensions *****************************" << endl;

  // Open FITS file in read mode
  CSimpleFITS myFITSFileIn(strFileName, Read);

  // Display number of CSimpleFITS object
  cout << endl << "\tNumber of CSimpleFITS objects: " << numberOfSimpleFITS() << endl;

  // Display number of HDU (aka extensions)
  cout << "\tNumber of HDUs (set by class constructor) : " << myFITSFileIn.numberOfHDU() << endl << endl;

  // Display number of CSimpleFITS object
  // cout << endl << "\tNumber of CSimpleFITS objects: " << numberOfSimpleFITS() << endl << endl;

  // Display FITS file informations
  myFITSFileIn.displayAnalyzeFITSFile();

  // Display PHDU keywords and comments
  myFITSFileIn.displayPHDU();

  return(iErr);
}