//---------------------------------------------------------------------------------------------------------------
/// \mainpage tstCcfitsio - main program
/// \brief Test program for CSimpleFITS class
/// \section Introduction
/// tstCcfitsio.cpp is a test program for CSimpleFITS Class.
///
/// Some FITS files are available in FileExamples directory.
///
/// \section how_to_run How to run the test program
///
/// - download and install fitsio C library
/// - download and install C++ fitsio wrapper of fitsio C library
/// - set str_workingDirectory and str_fitsFileDirectory in tstCcfitio.cpp with the path and directory name containing
/// the FITS files.
/// - set the include path in GNUmakefile and run make
/// - execute tstCcfitsio binary
///
/// \section Related_links Related links
///  See https://fits.gsfc.nasa.gov/fits_home.html to understand standard data format used in astronomy.
///
///  See https://heasarc.gsfc.nasa.gov/docs/software/fitsio/fitsio.html to download and install fitsio C library
///
///  See https://heasarc.gsfc.nasa.gov/fitsio/CCfits/ to download and install C++ fitsio wrapper of fitsio C library
///
/// \section milestones Milestones
/// who      | when       | what
/// ---------|------------|------------------------------------------------------
/// sroussea | 2020-11-06 | Creation
/// boebion  | 2021-09-07 | Learning FITS
/// boebion  | 2021-10-26 | First step to read and write data in a binary extension
/// boebion  | 2021-11-05 | Switch to Doxygen comments and documentations
/// sroussea | 2022-01-03 | Corrections and remark
/// boebion  | 2022-02-25 | Add exception in CSimpleFITS constructor
/// boebion  | 1022-05-16 | Add binary table and binary image extension
///
/// \author Olivier Boebion
/// \version 0.8
/// \date 2022/05/16
/// \copyright copyright Lagrange Lab - SPICA project
//---------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------
// System Headers
//---------------------------------------------------------------------------------------------------------------
#include <cmath>
#include <dirent.h>
#include <fstream>
#include <functional> // std::hash<>
#include <iostream>

/// \namespace std
///
/// Standard namespace
///
using namespace std;

// Our CSimpleFITS Class
#include "CSimpleFITS.h"

// Pi
// double dPI = 2 * asin(1);

// where to find FITS files
string str_workingDirectory("/home/boebion/Code/CSimpleFITS/csimplefits");
string str_fitsFileDirectory = str_workingDirectory + "/FileExamples";

// Some function forward declarations
// Select a FITS file in the main() function
string str_chooseFITSFile(const string &str_fitsFileDirectory);
int writeFitsBinaryTable(string &strFileName);
int writeFitsBinaryImage(string &strFileName);
int readFitsFile(string &strFileName);

#include "btComDefs.h"
using pixDynMat = Matrix<unsigned short, Dynamic, Dynamic, RowMajor>;

int main()
{
  int iErr = EXIT_SUCCESS;

  cout << "sizeof(unsigned short) = " << sizeof(unsigned short) << endl;
  unsigned short usBiasOut[9] = {0, 1, 2, 3, 4, 5, 6, 7, 8};
  fstream biasFileOut("dum.bin", ios::binary | ios::out);
  biasFileOut.write((const char *)usBiasOut, sizeof(usBiasOut));
  biasFileOut.close();

  pixDynMat matBiasFrameIn = pixDynMat::Zero(3, 3);
  fstream biasFileIn("dum.bin", ios::binary | ios::in);
  biasFileIn.read((char *)matBiasFrameIn.data(), sizeof(usBiasOut));
  cout << "matBiasFrameIn = " << matBiasFrameIn << endl;
  //---------------------------------------------------------------------------------------------------------------

  // Name of FITS file to open and read
  std::string str_chosenFITSFile = "None";

  // menu entry
  string strMenuChoice;
  hash<string> hasher;

  do
  {
    //-----------------------------------------------------------------------------
    // Display a menu to choose a FITS file to read
    //-----------------------------------------------------------------------------
    cout << endl << endl << "** Open,read and write FITS files **" << endl;
    cout << endl << "Chosen FITS file: " << str_chosenFITSFile << endl;
    cout << endl << "Number of actual CSimpleFITS objects: " << numberOfSimpleFITS() << endl << endl;
    cout << "1 - Create a FITS file from scratch with a binary table extension -> dummy1.fits" << endl;
    cout << "2 - Create a FITS file from scratch with a binary image extension -> dummy2.fits" << endl;
    cout << "3 - Read and analyze dummy1.fits" << endl;
    cout << "Q - Quit" << endl << endl;
    cout << "Your choice : ";

    cin >> strMenuChoice;

    if (hasher(strMenuChoice) == hasher(string("1")))
    {
      string strFileName("dummy1.fits");
      iErr = writeFitsBinaryTable(strFileName);
      if (iErr != 0)
      {
        cout << "writeFitsBinaryTable() execution upon " << strFileName << " failed !" << endl;
      }
    }
    else if (hasher(strMenuChoice) == hasher(string("2")))
    {
      string strFileName("dummy2.fits");
      iErr = writeFitsBinaryImage(strFileName);
      if (iErr != 0)
      {
        cout << "writeFitsBinaryImage() execution upon " << strFileName << " failed !" << endl;
      }
    }
    else if (hasher(strMenuChoice) == hasher(string("3")))
    {
      // Choice of FITS file in fitFileDirectory
      // str_chosenFITSFile = str_chooseFITSFile(str_fitsFileDirectory);
      str_chosenFITSFile = "dummy1.fits";
      iErr = readFitsFile(str_chosenFITSFile);
      if (iErr != 0)
      {
        cout << "readFitsFile() execution upon " << str_chosenFITSFile << " failed !" << endl;
      }
    }
  } while (hasher(strMenuChoice) != hasher(string("Q")));

  exit(iErr);
}

//---------------------------------------------------------------------------------------------------------------
// Local functions
//---------------------------------------------------------------------------------------------------------------

/// \fn int writeFitsBinaryTable(string &strFileName)
/// \brief Create a test FITS file with a binary table
///
/// \param strFileName the file name to be created
/// \return Error as an int => 0 == success
//-----------------------------------------------------------------------------------------------------------------------
int writeFitsBinaryTable(string &strFileName)
{
  int iErr = EXIT_SUCCESS;

  try
  {
    // Binary HDU data writing
    // Open FITS file in write mode with reset (!)
    CSimpleFITS myFITSFileOutBinaryTable(("!" + strFileName), Write);

    // Name of extension to create
    string hduName("Learning binary table extension");

    // Extension will contain 2 columns with a name, a form (number of data and type) and a unity
    vector<string> colName(2, "");
    vector<string> colForm(2, "");
    vector<string> colUnit(2, "");

    colName[0] = "numbers";
    colName[1] = "cosinus";
    colForm[0] = "1I";   // One int value
    colForm[1] = "100D"; // One hundred of double values
    colUnit[0] = "none"; // Unity
    colUnit[1] = "none"; // Unity

    // Example with no data line when the extension will be created
    unsigned long rows(1);

    cout << endl << "**************************** Create a binary table *********************************" << endl;

    // Creation of binary table extension
    iErr = myFITSFileOutBinaryTable.createBinaryTable(hduName, rows, colName, colForm, colUnit);
    if (iErr != 0)
    {
      cout << "createBinaryTable(): myFITSFileOutBinaryTable.createBinaryTable() failed, aborting... " << endl;
      return iErr;
    }
    //----------------------------------------------------------------------------------------------------
    // CREATING DUMMY DATA -------------------------------------------------------------------------------
    // Create 3 random integer between 0 to 32767
    vector<int> randomInt(3);
    srand(time(NULL)); // Seed initialization for rand() function
    for (int i = 0; i < 3; ++i)
    {
      //  randomInt[i] = rand() % 32768;
      randomInt[i] = 2 * i + 1;
    }

    // Create 100 random double values in a valarray, add the valarray into a vector<valarray>
    int iValNb = 100;
    valarray<double> randomCosinus(iValNb);
    vector<std::valarray<double>> vect_cosinus;
    for (int iCurv = 0; iCurv < 3; iCurv++)
    {
      // Create a val array
      for (int i = 0; i < iValNb; ++i)
      {
        randomCosinus[i] = iCurv + cos(4 * dPI * i / iValNb) + 0.2 * rand() / RAND_MAX;
      }
      // Add the same valarray values in 3vector
      vect_cosinus.push_back(randomCosinus);
    }

    //----------------------------------------------------------------------------------------------------
    // FILING DUMMY DATA ---------------------------------------------------------------------------------
    cout << endl
         << "**************************** write data in a binary table *********************************" << endl;

    // add the value of randomInt in extension "Learning" column "numbers" beginning at first row
    iErr = myFITSFileOutBinaryTable.writeBinary(randomInt, hduName, "numbers", 1);
    if (iErr != 0)
    {
      cout << "writeFitsBinary : myFITSFileOutBinaryTable.writeBinary() failed, aborting... " << endl;
      return iErr;
    }

    // Add the value of vector of valarray "vecto_cosinus" in extension "Learning" column "cosinus" beginning at first
    // row
    // 1
    iErr = myFITSFileOutBinaryTable.writeBinaryArrays(vect_cosinus, hduName, "cosinus", 1);
    if (iErr != 0)
    {
      cout << "writeBinaryArrays() : myFITSFileOutBinaryTable.writeBinaryArrays() failed, aborting... " << endl;
      return iErr;
    }

    cout << endl << "Writing \"1 int and 1 vector of 100 double in 3 rows\" done in : " << strFileName << endl;

    cout << endl
         << "**************************** write data in a binary table *********************************" << endl;

    // add the value of randomInt in extension "Learning" column "numbers" beginning at 4th row
    iErr = myFITSFileOutBinaryTable.writeBinary(randomInt, hduName, "numbers", 4);
    if (iErr != 0)
    {
      cout << "writeBinary() : myFITSFileOutBinaryTable.writeBinary() failed, aborting... " << endl;
      return iErr;
    }

    // Add the value of vector of valarray "vecto_cosinus" in extension "Learning" column "cosinus" at 4th row
    iErr = myFITSFileOutBinaryTable.writeBinaryArrays(vect_cosinus, hduName, "cosinus", 4);
    if (iErr != 0)
    {
      cout << "writeBinaryArrays : myFITSFileOutBinaryTable.writeBinaryArrays() failed, aborting... " << endl;
      return iErr;
    }

    cout << endl << "Writing \"1 int and 1 vector of 100 double in 3 new rows\" done in : " << strFileName << endl;
    return iErr;
  }
  catch (const char *e)
  {
    std::cerr << e << "Unable to create CSimpleFITS object from file\n";
    iErr = EXIT_FAILURE;
  }
  return iErr;
}

//---------------------------------------------------------------------------------------------------------------
/// \fn int writeFitsBinaryImage(string &strFileName)
/// \brief Create a test FITS file with a binary image
///
/// \param strFileName the file name to be created
/// \return Error as an int => 0 == success
//---------------------------------------------------------------------------------------------------------------
int writeFitsBinaryImage(string &strFileName)
{
  int iErr = EXIT_SUCCESS;

  try
  {
    // Binary HDU data writing
    // Open FITS file in write mode with reset (!)
    CSimpleFITS myFITSFileOutBinaryImage(("!" + strFileName), Write);

    // Name of extension to create
    string hduName("Learning binary image extension");

    // CFITSIO constants corresponding to the type of images: BYTE_IMG, SHORT_IMG, LONG_IMG, FLOAT_IMG, DOUBLE_IMG,
    // USHORT_IMG, ULONG_IMG, LONGLONG_IMG
    int i_bitPixel = FLOAT_IMG;

    // allocate a vector for an image of 300x300 pixels
    std::vector<long> v_extAxes(2, 300);

    // calculate the number of pixels (300x300)
    long l_numberOfPixels = std::accumulate(v_extAxes.begin(), v_extAxes.end(), 1, std::multiplies<long>());

    // prepare an array with a size equal to the number of pixels
    std::valarray<float> v_PixelValue(l_numberOfPixels);

    // create some dummy data
    const float PIBY(M_PI / 150.);
    for (int jj = 0; jj < l_numberOfPixels; ++jj)
    {
      float arg = PIBY * jj;
      v_PixelValue[jj] = std::cos(arg);
      // cout << v_PixelValue[jj] << " " ;
    }

    long l_firstPixel(1);

    cout << endl
         << "**************************** Create a binary image extension *********************************" << endl;

    // Creation of binary image extension and write an image with float data
    iErr = myFITSFileOutBinaryImage.createBinaryImage(hduName, i_bitPixel, v_extAxes, l_firstPixel, l_numberOfPixels,
                                                      v_PixelValue);
    if (iErr != 0)
    {
      cout << "writeFitsBinaryImage() : myFITSFileOutBinaryImage.createBinaryImage() failed, aborting... " << endl;
      return iErr;
    }
  }
  catch (const char *e)
  {
    std::cerr << e << "Unable to create CSimpleFITS object\n";
    iErr = EXIT_FAILURE;
  }
  return iErr;
}

//---------------------------------------------------------------------------------------------------------------
/// \fn int readFitsFile(string &strFileName)
/// \brief Browse a test FITS file
///
/// \param strFileName the file name to be readout
/// \return Error as an int => 0 == success
//---------------------------------------------------------------------------------------------------------------
int readFitsFile(string &strFileName)
{
  int iErr = EXIT_SUCCESS;
  //-----------------------------------------------------------------------------------------------------------------------

  // Verify the number of CSimpleFITS instance (normally, it's one)
  // cout << endl << "\tNumber of CSimpleFITS objects: " << numberOfSimpleFITS() << endl << endl;

  //-----------------------------------------------------------------------------
  //  HDU file selection and display FITS informations
  //-----------------------------------------------------------------------------

  cout << endl
       << "******************* Read informations of primary HDU and extensions *****************************" << endl;

  // Open FITS file in read mode
  CSimpleFITS myFITSFileIn(strFileName, Read);

  // Display number of CSimpleFITS object
  cout << endl << "\tNumber of CSimpleFITS objects: " << numberOfSimpleFITS() << endl;

  // Display number of HDU (aka extensions)
  cout << "\tNumber of HDUs (set by class constructor) : " << myFITSFileIn.numberOfHDU() << endl << endl;

  // Display number of CSimpleFITS object
  // cout << endl << "\tNumber of CSimpleFITS objects: " << numberOfSimpleFITS() << endl << endl;

  // Display FITS file informations
  myFITSFileIn.displayAnalyzeFITSFile();

  // Display PHDU keywords and comments
  myFITSFileIn.displayPHDU();

  //-----------------------------------------------------------------------------------------------------------------------

  // Next step : read an extension (HDU) and binary table column
  cout << endl << "**************************** Read an extension *****************************************" << endl;

  //-----------------------------------------------------------------------------
  // Binary HDU data reading
  //-----------------------------------------------------------------------------
  /*
    // Open FITS file in read mode
    // string testReadBinaryFile = "/home/boebion/Code/CSimpleFITS/csimplefits/FileExamples/binarytableMELO.fits";
    string testReadBinaryFile = "/home/boebion/Code/CSimpleFITS/csimplefits/FileExamples/EUVEngc4151imgx.fits"; */

  CSimpleFITS myFITSBinaryFile(strFileName, Read);

  // Display number of CSimpleFITS object
  // cout << endl << "\tNumber of CSimpleFITS objects: " << numberOfSimpleFITS() << endl << endl;

  // Display FITS file informations
  // myFITSBinaryFile.displayAnalyzeFITSFile();

  // Display PHDU keywords history and comments
  // myFITSBinaryFile.displayPHDU();

  cout << endl << "**************************** Read extension number \"1\" ***************" << endl;
  // Display HDU by number (1 for example)
  myFITSBinaryFile.displayExtHDU(1);

  cout << endl << "**************** Read extension \"Learning binary table extension\" ***************" << endl;
  // Display HDU by name (ds_limit, for example)
  myFITSBinaryFile.displayExtHDU("Learning binary table extension");

  cout << endl
       << "**************************** Read data from a binary table *****************************************"
       << endl;

  // Read a binary table
  vector<int> dataReadFromHDU;

  myFITSBinaryFile.readBinary(dataReadFromHDU, "Learning binary table extension", "numbers", 1, 3);

  cout << "Read 3 rows of column \"numbers\" in HDU \"Learning binary table extension\" from FITS file \""
       << strFileName << "\" :" << endl;

  // cout << "Number of results :" << (int)dataReadFromHDU.size() << endl;

  for (int i = 0; i < (int)dataReadFromHDU.size(); i++)
  {
    cout << dataReadFromHDU[i] << " " << flush;
  }
  cout << "\n" << endl;

  // Display number of CSimpleFITS object
  // cout << endl << "\tNumber of CSimpleFITS objects: " << numberOfSimpleFITS() << endl << endl;

  return (iErr); // EXIT_SUCCESS or EXIT_FAILURE for OS compatibility
}

//---------------------------------------------------------------------------------------------------------------
/// \fn string str_chooseFITSFile(const string & str_fitsFileDirectory)
/// \brief Give a full path file name
///
/// \param str_fitsFileDirectory the full path of a directory
/// \return A string containing the full path name of chosen file
///
//---------------------------------------------------------------------------------------------------------------
string str_chooseFITSFile(const string &str_fitsFileDirectory)
{
  // String to const char*; necessary to use opendir()
  const char *fitsdir = str_fitsFileDirectory.c_str();

  //
  DIR *directory = NULL;
  struct dirent *dirEntry = NULL;

  // file name to open
  string filename;

  cout << "** Choose a FITS file **" << endl;

  // Try to open the directory
  if ((directory = opendir(fitsdir)) == NULL)
  {
    // Unable to open directory (bad path, directory permissions, ... ?)
    cout << endl << "Error : unable to open dir " << fitsdir << endl;
    return ("None");
  }

  cout << endl << "Available files in \"" << fitsdir << "\" :" << endl << endl;

  // Display all files and directories in directory
  while ((dirEntry = readdir(directory)) != NULL)
  {
    // Display Regular files only (not . and .. directories)
    if (dirEntry->d_type == DT_REG)
      cout << dirEntry->d_name << " ";
  }
  cout << endl;

  // Ask for a file name
  cout << endl << "Which file name to open ? : ";
  cin >> filename;

  // Concatenate FITS directory and file name and obtain a full path file name to open
  filename = str_fitsFileDirectory + "/" + filename;

  return (filename);
}