%% ------------------------------------------------------
% Extract images from an avi

% movFullPath : avi file path with name
% savPath     : path to store single images
% results   : results returned by analyzeProfiles.m
% energList : list of doses/energies studied
% ------------------------------------------------------

function [] =  extractImFromAvi( movFullPath, savPath )

mov = aviread(movFullPath );
% Determine how many frames there are.
numberOfFrames = size(mov, 2);
for frame = 1 : numberOfFrames
    % Extract the frame from the movie structure.
    thisFrame = mov(frame).cdata;
    % Create a filename.
    outputBaseFileName = sprintf('Frame %4.4d.tif', frame);
    outputFullFileName = fullfile(savPath, outputBaseFileName);

    % Write it out to disk.
    imwrite(thisFrame, outputFullFileName, 'tif');
end
