% function [entropy] = RelativeEntropyInShannon( probaDistri1, probaDistri2 )
%Function which computes and returns the relative entropy (in Shannon) of 2 discrete probability
%distributions given as input parameters
function [entropy] = RelativeEntropyInShannon( probaDistri1, probaDistri2 )
%make 0*log2( 0 ) null

ratio = probaDistri1./probaDistri2;
finiteIds = isfinite(ratio);
ratio = ratio ( finiteIds );
probaDistri1 = probaDistri1( finiteIds ) ;
zerosIds = find(ratio==0);
ratio ( zerosIds )= 1;
probaDistri1( zerosIds ) = 1;

entropy = sum( probaDistri1.*log2( ratio ) );
