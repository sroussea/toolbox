clc;close all;clear all;

addpath E:\tmp\GaspardData\matlabRdy
fid = fopen('run_0150.dat.10Oct12_11h05m10s_2000Tks_2560Pts_00_01_.bin');

NChannels = fread(fid, [1], '*uint');
nTracks = fread(fid, [1], '*uint');
N = fread(fid, [1], '*ushort');
N = uint32(N);

% Read tracks : sort by column : track_i = tracks(:,i)
% Signal amplitude is given in multiple of ADC LSB (125 �V)
tracksInARow = double( fread(fid, [N*NChannels, nTracks], '*ushort') );
fclose(fid);

% Reshape data : put a track matrix per column for each channel
% tracks = [ [ uint32(N), nTracks_Of_Channel1 ], NChannels ]
cellTracks = cell(1,NChannels);
for i = 1 : NChannels
  cellTracks{1,i} = tracksInARow( 1+N*(i-1):i*N, 1:nTracks);
end
% free unuseful ressources
clear tracksInARow;


% Base line suppression --------------------------------------------
% Q stands for the ADC LSB magnitude, BL stands for Base Line 
for i = 1 : NChannels
  cellTracksBL{1,i} = cellTracks{1,i} - repmat( mean( cellTracks{1,i}(1:300,:)), size(cellTracks{1,i},1), 1);
end

%good track
i1=cellTracksBL{1,1}(:,8);

%bipolar Track
bipolarTk = cellTracksBL{1,1}(:,3);

%free mem
% clear cellTracksBL;

fo_ = fitoptions('method','NonlinearLeastSquares','Lower',[-Inf -Inf    0]);
st_ = [415.60333333333324 375 161.60322331603655 ];
% st_ = [];
set(fo_,'Startpoint',st_);
ft_ = fittype('gauss1');
% Fit this model using new data
x_1=(1:size(i1))';

goodCurvId=[];
figure(1);
for i = 1 : size(cellTracksBL{1,1}, 2)
  sig=cellTracksBL{1,1}(:,i);
  [fitobject,gof] = fit(x_1 ,sig ,ft_,fo_);
  if gof(1).rsquare > .7
    goodCurvId = [ goodCurvId i ];
  end
end

plot(cellTracksBL{1,1}(:,goodCurvId));


