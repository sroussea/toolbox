function [zeroOrderMoment] = myBestBiPolarFilter( x  )

global xRange;
tmin = xRange(1); tmax = xRange(end); 
mu = tmin + (tmax - tmin)/2;
sig1 = x(1);
sig2 = x(2);

% gauss * gauss
% myFilter = gaussGaussFunc(xRange, mu, sig1, sig2);

% cos * gauss
myFilter = cosGaussFunc(xRange, mu, sig1, x(2));


zeroOrderMoment = sum( myFilter );
% zeroOrderMoment = abs(sum( myFilter ));
% zeroOrderMoment = abs(1-sum( filterShape (xRange, mu, sig, omeg) ));
% zeroOrderMoment = abs(integral( @(t) filterShape (t, mu, sig, omeg) ,tmin,tmax));
