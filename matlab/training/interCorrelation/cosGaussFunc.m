function [myFilter] = cosGaussFunc( xRange, mu, sig1, alpha )

global gaussIn gaussNorm;
gIn = gaussIn(xRange, mu, sig1);
gIn = gIn - alpha;
myFilter = abs(sum(.5*(cos(2*pi*(xRange - mu)/(length(xRange)-1)) + 1).*gIn)); % take abs() because we don't want any negative value