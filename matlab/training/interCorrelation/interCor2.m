close all;clear all;clc;
addpath S:\svn\DSP\common\matlab
imPath = 'd:\tmp\AndromedData\';
outputFolder = [imPath, 'images\'];

% x1 = [ 1 2];
% x2 = [ 1 -4];
% disp('cov(x1,x2)')
% cov(x1,x2)
% disp('xcov(x1,x2)')
% xcov(x1,x2)
% disp('corr([x1;x2]'')')
% corr([x1;x2]')
% disp('xcorr(x1,x2)')
% xcorr(x1,x2)
% return

% Fetch the list of files
listOfIms = ls ([outputFolder '/*.tif']);
% store number of files
nbOfFiles = length(listOfIms);

[imOrg,mu]=imread([outputFolder,listOfIms(1,:)]);
imOrgDbl = im2double(imOrg) - mean2(im2double(imOrg ));
figure; imagesc(imOrgDbl) ; colormap(gray);


% imSlice= imcrop(imOrgDbl,[0 0 1  size(imOrgDbl,1)] );
imSlice= imcrop(imOrgDbl,[605 0 0  size(imOrgDbl,1)] );
figure('name','imSlice');
plot( imSlice);

sigGaussFilter = 1.7;
filtRange = [1 20.];
% gaussFilter = fspecial('gaussian', filtRange, sigGaussFilter);
% biPolGaussFilter = gaussFilter - mean(gaussFilter);
avFilt = fspecial('average', filtRange);


% SR : 5th of December 2014 : real bipolar wavelet is not good.
% It amplifies similar oscillations (which are generally noise) in the signal 
% as well as the blobs which is not what we wish
global gaussIn gaussNorm;
gaussNorm = @( x, mu, sig)(1/sig/sqrt(2*pi))*exp((-(x-mu).^2)/(2*sig^2));
gaussIn = @( x, mu, sig)exp((-(x-mu).^2)/(2*sig^2));
% gaussIn = @( x, mu, sig)(1/sig/sqrt(2*pi)).*exp((-(x-mu).^2)/(2*sig^2));
% gaussGaussFunc = @( x, mu, sig1, sig2 )gaussIn( x, mu, sig1).*gaussIn( x, mu, sig2);
% global gaussCosFunc;
% gaussCosFunc = @( x, mu, sig, omeg )exp((-(x-mu).^2)/(2*sig^2)).*cos((omeg*(x-mu))) ;


% xmin = -10; xmax = -xmin;
% mu = (xmax-xmin)/2; sig = 1; omeg = sig/3*2*pi;
% filter2_Int = integral( @(x) filter2 (x, mu, sig, omeg) ,xmin,xmax)

global xRange;
xRange = filtRange(1):1:filtRange(end);
tmin = xRange(1); tmax = xRange(end); 
mu = tmin + (tmax - tmin)/2;

gaussNormSlowFilter = gaussNorm ( xRange, mu, 4);
gaussNormSlowFilter = gaussNormSlowFilter - mean(gaussNormSlowFilter );
gaussFastFilter = gaussNorm ( xRange, mu, sigGaussFilter);
gaussFastFilter = gaussFastFilter - mean(gaussFastFilter );

% gs = GlobalSearch;
% sig1Start = sigGaussFilter;
% sig2Start = sig1Start;
% upperB = [ 2.5*sigGaussFilter, 1*sig2Start];
% lowerB = [ .99*sigGaussFilter, 1*sig2Start];
% 
% myFunc = @(x)(myBestBiPolarFilter( x));
% 
% problem = createOptimProblem('fmincon','x0',[sig1Start, sig2Start],...
%   'objective',myFunc,'lb',lowerB,'ub',upperB);
% [xmin, fmin, flag, outpt, allmins] = run( gs, problem );
% 
% biPolGaussFilter = gaussGaussFunc ( xRange, mu, xmin(1), xmin(2));
% % biPolGaussFilter = biPolGaussFilter - mean(biPolGaussFilter);
% 
% figure('name','High Resolution Gauss.Cos filter in direct space');
% hp = plot(xRange, biPolGaussFilter ,'b');

corrRange   = length(imSlice) - floor(length( xRange )/2) : 2*length(imSlice) - round(length( xRange )/2) - 1 ;
xCorNormSlowGauss = xcorr(imSlice, gaussNormSlowFilter ); 
xCorNormSlowGauss = xCorNormSlowGauss( corrRange );

xCorFastGauss = xcorr(imSlice, gaussFastFilter ); 
xCorFastGauss = xCorFastGauss( corrRange );

xCorFastGaussOvSlowGauss = xcorr(xCorNormSlowGauss, gaussFastFilter ); 
xCorFastGaussOvSlowGauss = xCorFastGaussOvSlowGauss( corrRange );

xCorrAver = xcorr(imSlice, avFilt); 
xCorrAver = xCorrAver(corrRange );

xCorFastGaussOvAv = xcorr(xCorrAver, gaussFastFilter ); 
xCorFastGaussOvAv = xCorFastGaussOvAv( corrRange );

figure('name', 'Raw signal + xCorrAver');
hp(:,1) = plot(imSlice,'b');title('Raw signal + xCorrAver'); hold on;
hp(:,2) = plot(xCorrAver,'r','linewidth',1.     ); hold off;
legend(hp(1,:),{'raw signal', 'xCorrAver' });

figure('name', 'Raw signal + xCorFastGauss + xCorFastGaussOvAv');
hp(:,1) = plot(imSlice,'b');title('Raw signal + xCorNormSlowGauss'); hold on;
hp(:,2) = plot(xCorFastGauss,'r','linewidth',1.     ); 
hp(:,3) = plot(xCorFastGaussOvAv,'g','linewidth',1.     );hold off;
legend(hp(1,:),{'raw signal', 'xCorFastGauss','xCorFastGaussOvAv'});

figure('name','filters in direct space');
roi = 1:length(xRange);
hp = plot(roi, gaussNormSlowFilter ,'b',roi, gaussFastFilter ,'r');
title('FFT of the filters');
legend(hp,'gaussNormSlowFilter', 'gaussFastFilter');

figure('name', 'Raw signal + xCorNormSlowGauss');
hp(:,1) = plot(imSlice,'b');title('Raw signal + xCorNormSlowGauss'); hold on;
hp(:,2) = plot(xCorNormSlowGauss,'r','linewidth',1.     ); 
hp(:,3) = plot(xCorFastGauss,'g','linewidth',1.     );hold off;
legend(hp(1,:),{'raw signal', 'xCorNormSlowGauss','xCorFastGauss'});

figure('name', 'Raw signal + xCorFastGaussOvSlowGauss');
hp(:,1) = plot(imSlice,'b');title('Raw signal + xCorFastGaussOvSlowGauss'); hold on;
hp(:,2) = plot(xCorFastGaussOvSlowGauss,'r','linewidth',1.5     );hold off;
legend(hp(1,:),{'raw signal', 'xCorNormSlowGauss'});


xCorrCGaussBip = xcorr(imSlice, biPolGaussFilter); 
xCorrCGaussBip = xCorrCGaussBip(corrRange );

xCorrCGaussBip = xcorr(imSlice, biPolGaussFilter); 
xCorrCGaussBip = xCorrCGaussBip(corrRange );


xCorGaussGaussBip = xcorr(xCorrCGauss, biPolGaussFilter); 
xCorGaussGaussBip = xCorGaussGaussBip(corrRange );

% gaussCosFilter = gaussGaussFunc ( filtRange(1):filtRange(end), mu, xmin(1), xmin(2));
% xCorrGaussCosFilt = xcorr(imSlice, gaussCosFilter); 
% xCorrGaussCosFilt = xCorrGaussCosFilt(corrRange );

% plotting
figure('name','signals in direct space');

nPlot = 5;
subplot(nPlot ,1,1); clear hp;
hp(:,1 ) = plot(imSlice,'b');title('imSlice + xCorGaussGaussBip'); hold on;
hp(:,2 ) = plot(xCorGaussGaussBip,'r');hold off;
legend(hp(1,:),{'raw signal', 'xCorGaussGaussBip'});

subplot(nPlot ,1,2); 
hp(:,1 ) = plot(imSlice,'b');title('imSlice + xCorrCGaussBip '); hold on;
hp(:,2 ) = plot(xCorrCGaussBip,'r');hold off;
legend(hp(1,:),{'raw signal', 'xCorrCGaussBip'});

subplot(nPlot ,1,3); 
hp(:,1 ) = plot(imSlice,'b');title('imSlice + xCorrCGauss'); hold on;
hp(:,2 ) = plot(xCorrCGauss, 'r');title('InterCGauss    ')
legend(hp(1,:),{'raw signal', 'xCorrCGauss'});

subplot(nPlot ,1,4); 
plot(xCorrCGaussBip,'r'     );title('InterCGaussBip ')

figure('name', 'xCorrCGauss + raw signal');
hp(:,1) = plot(imSlice,'b');title('xCorrCGauss + raw signal'); hold on;
hp(:,2) = plot(xCorrCGauss,'r','linewidth',1.5     );hold off;
legend(hp(1,:),{'raw signal', 'xCorrCGauss'});

% subplot(nPlot ,1,5); 
% plot(imSlice,'b');title('imSlice + xCorrGaussCosFilt (red)'); hold on;
% plot(xCorrGaussCosFilt,'r'     );hold off;

figure('name', 'xCorrCGauss + xCorrCGaussBip');
hp(:,1) = plot(imSlice,'b');title('xCorrCGauss + xCorrCGaussBip'); hold on;
hp(:,2) = plot(xCorrCGauss,'r','linewidth',1.5     );
hp(:,3) = plot(xCorrCGaussBip,'g');hold off;
legend(hp(1,:),{'raw signal', 'xCorrCGauss', 'xCorrCGaussBip'});

figure('name', 'Raw signal + xCorGaussGaussBip');
hp(:,1) = plot(imSlice,'b');title('Raw signal + xCorGaussGaussBip'); hold on;
hp(:,2) = plot(xCorGaussGaussBip,'r','linewidth',1.5     );hold off;
legend(hp(1,:),{'raw signal', 'xCorGaussGaussBip'});

figure('name','filters in direct space');
roi = (1:length(gaussFilter));
hp = plot(roi, gaussFilter(roi)  ,'b',roi,biPolGaussFilter ,'r');
title('FFT of the filters');
legend(hp,'Gauss filter', 'bipolar Gauss Filter');

Fs = 1e6;
gaussFilterZPadded = [gaussFilter ,zeros(1,length(imSlice) - length(gaussFilter)) ];
[ fftGaussFilter absFFTGaussFilter_Sg dspGaussFilter...
  avPowGaussFilter sigF_Sg ] = fft1D_Tools( gaussFilterZPadded' , Fs );
phasGaussFilter = unwrap(angle( fftGaussFilter ));

biPolGaussFilterZPadded  = [biPolGaussFilter ,zeros(1,length(imSlice) - length(biPolGaussFilter)) ];
[ fft_biPolGaussFilter absFFT_biPolGaussFilter_Sg dsp_biPolGaussFilter...
  avPow_biPolGaussFilter sigF_Sg ] = fft1D_Tools( biPolGaussFilterZPadded' , Fs );
phasBiPolGaussFilter = unwrap(angle( fft_biPolGaussFilter ));

myDoor = [-ones(1,length(gaussFilter))*mean(gaussFilter), zeros(1,length(imSlice) - length(gaussFilter)) ];
[ fft_door absFFT_doorSg dspDoor...
  avPowDoor sigF_Sg ] = fft1D_Tools( myDoor' , Fs );


fftGaussMinusfftDoor = fftGaussFilter + fft_door;
N = length(fftGaussMinusfftDoor);
absFftGaussMinusfftDoor = abs(fftGaussMinusfftDoor )/N;
absFftGaussMinusfftDoorSg (1:N/2+1) = absFftGaussMinusfftDoor (1:N/2+1);
absFftGaussMinusfftDoorSg (2:end) = 2*absFftGaussMinusfftDoorSg (2:end);

figure('name','filters in the Fourier space I');
hp = plot(sigF_Sg,absFFTGaussFilter_Sg ,'b',...
  sigF_Sg,absFFT_biPolGaussFilter_Sg,'r',...
  sigF_Sg,absFFT_doorSg,'g',...
  sigF_Sg,absFftGaussMinusfftDoorSg ,'bo');
title('filters in the Fourier space I');
legend(hp,'|FFT(Gauss filter)|', '|FFT(bipolar Gauss Filter)|', '|FFT(door)|',...
  '|FFT(Gauss filter) + FFT(door)|');

% figure('name','filters in the Fourier space II');
% hp =   plot(sigF_Sg,absFFT_biPolGaussFilter_Sg,'b',...
%   sigF_Sg, absFFT_gaussCosFilterSg,'r');
% title('filters in the Fourier space II');
% legend(hp,'|FFT(bipolar Gauss Filter)|', '|FFT(gauss.cos)|');

idStart = length(phasBiPolGaussFilter) - length(sigF_Sg)+1;
figure('name','filters in the Fourier space II (Phase)');
hp   = plot(sigF_Sg,phasBiPolGaussFilter(idStart:end),'b',...
  sigF_Sg,phasGaussFilter(idStart:end),'r');
title('filters in the Fourier space II');
legend(hp,'Phi[FFT(bipolar Gauss Filter)]', 'Phi[FFT(gauss filter)]');

[ fft_xCorrCGaussBip absFFTxCorrCGaussBip_Sg dspxCorrCGaussBip...
  avPowCorrCGaussBip corrF_Sg ] = fft1D_Tools( xCorrCGaussBip, Fs );

[ fft_imSlice absFFT_imSlice_Sg dsp_imSlice...
  avPow_imSlice sigF_Sg ] = fft1D_Tools( imSlice, Fs );

% [ fft_xCorrGaussCosFilt absFFTxCorrGaussCosFiltSg dspxCorrGaussCosFilt...
%   avPowxCorrGaussCosFilt corrF_Sg ] = fft1D_Tools( xCorrGaussCosFilt, Fs );

figure('name','filtered signal in the Fourier space');
hp = plot( sigF_Sg, absFFT_biPolGaussFilter_Sg ,'r',...
      sigF_Sg, absFFT_imSlice_Sg,'b',...
      corrF_Sg ,absFFTxCorrCGaussBip_Sg  ,'g'  );
title('filtered signal in the Fourier space');
legend(hp,'|FFT(Bi polar Gauss Filter)|',...
  '|FFT(Raw signal)|', '|FFT(bipolarGauss over raw signal)|');


fftImSliceXfftBiPolGaussFilter = fft_imSlice.*fft_biPolGaussFilter;
N = length(fft_imSlice);
absFftImSliceXfftBiPolGaussFilter = abs(fftImSliceXfftBiPolGaussFilter)/N;
absFftImSliceXfftBiPolGaussFilterSg  (1:N/2+1) = absFftImSliceXfftBiPolGaussFilter (1:N/2+1);
absFftImSliceXfftBiPolGaussFilterSg(2:end) = 2*absFftImSliceXfftBiPolGaussFilterSg(2:end);

figure;
plot(corrF_Sg,absFFTxCorrCGaussBip_Sg,'b',...
  sigF_Sg,absFftImSliceXfftBiPolGaussFilterSg,'r');
% title('absFFTxCorrCGaussBip_Sg (red) vs absFFT_imSlice_Sg (blue)');

%  idea 1:
%  do the FFT of the image filtered with the bipolar gaussian filter
%  Normally, blob amplitude is amplified
%  cut all fft coeff which amplitude is smaller than a threshold
%  perform FFT inverse => should keep only blobs

%  idea 2:
% Perform raw image filtering with the inverse of the bi-polar gaussian
% Should keep only the noise => FFT(noise)
% Compute IFFT [ FFT( raw image ) - FFT(noise) ] => should get blobs only

return;


N=256; %nombre de points
T0 = 200; f0=1/T0; %Fréquence
A=3; %amplitude du signal
fe=1000; %fréquence d'echantillonage
t=[1:N]/fe;

gaussFunc = @( X, mu, sig )(1/sig^2/sqrt(2*pi))*exp((-(X-mu).^2)/(2*sig^2)) ;

xmin = -10; xmax = -xmin;
gaussianInt = integral(@(x)gaussFunc(x, 1, 1) ,xmin,xmax)
gaussian_x_Int = integral(@(x)x.*gaussFunc(x, 1, 1) ,xmin,xmax)

% build noised signal
sigNoisSig = 1; muNoisSig = 4;
% noisedSig = sigNoisSig*randn(1, N) + muNoisSig;
noisedSig = sigNoisSig*randn(1, N) + muNoisSig*sin(2*pi*f0*(1:N));

% build a gaussian filter
filterSlot = (-2:.2:2);
sig = .5;mu = 0;
gFilter = gaussFunc( filterSlot, mu, sig ); 
figure('name','gauss Filter');
plot( gFilter);

% build a bipolar gaussian filter
sig = .5;mu = 0;
bipGFilter = gaussFunc( filterSlot, mu, sig );
bipGFilter = bipGFilter- mean(bipGFilter); 
figure('name','Bipolar gauss Filter');
plot( bipGFilter );

% build a cst filter
cstFilter = ones(1,length(filterSlot));
figure('name','cstFilter Filter');
plot(filterSlot  ,  cstFilter );


% intercorrelation
InterCGauss = xcov(noisedSig,gFilter ); 
InterCGaussBip = xcov(noisedSig,bipGFilter ); 
InterCCstFilter = xcov(noisedSig,cstFilter ); 

% plotting
figure('name','Intercorrelation');
subplot(4,1,1); 
plot(noisedSig);title('sigNoisSig')
subplot(4,1,2); 
plot(InterCGauss);title('interCor with gaussian filter')
subplot(4,1,3); 
plot(InterCGaussBip);title('interCor with bipolar gaussian filter')
subplot(4,1,4); 
plot(InterCCstFilter);title('interCor with constant filter')


return;

delay = 80;
imp= 4*square(2*pi*f0*[0:19]/fe); %Impulsion de duree egal a 20 points
emis=[imp zeros(1,N-size(imp,2))]; %mm signal q le premier sauf retardé
recu =[2*randn(1,delay) imp+2*randn(1,size(imp,2)) 2*randn(1,N-delay-size(imp,2))]; %signal recu

subplot(3,1,1); %on divise l'ecran en 2 partie, U1, on selectionne 1
plot(t,emis) 
title('emis')
subplot(3,1,2); %on divise l'ecran en 2 partie, U1, on selectionne 2
plot (recu)
title(['recu, retard = ', num2str(delay) ]);
subplot(3,1,3); %remise a la normale
InterC = xcov(emis,recu); %intercorrelation entre 2 signaux
plot(InterC); %affichage du resultat de l'intercorrélation 
title('xcov(emis,recu)')

[maxi,idMax]=max(InterC);
delayGuessed=N-idMax   % InterC(1) = xcov(N), InterC(2) = xcov(N-1)