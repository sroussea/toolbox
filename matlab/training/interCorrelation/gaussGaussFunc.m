function [myFilter] = gaussGaussFunc( xRange, mu, sig1, sig2 )

global gaussIn gaussNorm;
gIn = gaussIn(xRange, mu, sig1);
gIn = gIn - mean(gIn);
% myFilter = gIn.*gaussNorm(xRange, mu, sig2);
% myFilter = gIn.*gaussIn(xRange, mu, sig2);
myFilter = .5*(cos(2*pi*(xRange - mu)/length(xRange)) + 1).*gIn;