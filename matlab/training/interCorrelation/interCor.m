close all;clear all;clc;



N=256; %nombre de points
f0=50; %Fréquence
A=3; %amplitude du signal
fe=1000; %fréquence d'echantillonage
t=[1:N]/fe;

delay = 80;
imp= 4*square(2*pi*f0*[0:19]/fe); %Impulsion de duree egal a 20 points
emis=[imp zeros(1,N-size(imp,2))]; %mm signal q le premier sauf retardé
recu =[2*randn(1,delay) imp+2*randn(1,size(imp,2)) 2*randn(1,N-delay-size(imp,2))]; %signal recu

subplot(3,1,1); %on divise l'ecran en 2 partie, U1, on selectionne 1
plot(t,emis) 
title('emis')
subplot(3,1,2); %on divise l'ecran en 2 partie, U1, on selectionne 2
plot (recu)
title(['recu, retard = ', num2str(delay) ]);
subplot(3,1,3); %remise a la normale
InterC = xcov(emis,recu); %intercorrelation entre 2 signaux
plot(InterC); %affichage du resultat de l'intercorrélation 
title('xcov(emis,recu)')

[maxi,idMax]=max(InterC);
delayGuessed=N-idMax   % InterC(1) = xcov(N), InterC(2) = xcov(N-1)