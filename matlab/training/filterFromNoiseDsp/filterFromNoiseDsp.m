clc;close all;clear all;

T = 16;
N=1024;

Ts = T/(N-1);
Fs=1/Ts;
tVect = (0:N-1)*Ts;
F_axis = (0:N-1)*Fs/N;
F_axis_Single = F_axis(1:N/2+1);

% areaExact = 1/3*T^3
% meanExact = areaExact/T
% 
% areaNum   = Ts*sum(tVect.^2)
% meanNum = areaNum/T
% return

% rng default;
% Fs = 100;
% t = linspace(0,1,Fs);
% x = cos(2*pi*100*t)+randn(size(t));
% N = length(x);
% xdft = fft(x);
% xdft = xdft(1:N/2+1);
% psdx = (1/(Fs*N)).*abs(xdft).^2;
% psdx(2:end-1) = 2*psdx(2:end-1);
% 
% avPowFromPsd = sum(psdx)
% avPowFromSig = sum(x.^2)/N
% return

% fftNoise_Db = fft(noiseSig); 
% 
% avPowFromSig1 = sum(noiseSig.^2)/(N-1)
% avPowFromDsp1 = sum(abs(fftNoise_Db).^2)/(N-1)^2
% 
% fftNoise_Db_V  = fftNoise_Db; 
% sqModOfDft   = (abs(fftNoise_Db_V)).^2;
% sqModOfDft   = sqModOfDft/(N-1)^2;
% powFromPsd   = Fs/N*sum(sqModOfDft ) 
% avPowFromDsp = powFromPsd/T

A2=1;A=sqrt(A2);
% number of harmonics
nH = 10;
% build a set of nH random frequencies ranged from 0 to Fs/2
freqIDs = sort(round((N/2)*rand(1,nH )));
setOfFreqs =  F_axis_Single(freqIDs);
% buid a set of harmonics of frequencies defined above
noiseHarmos = A*sin(2*pi*tVect'*setOfFreqs);
noiseSig = sum(noiseHarmos,2);

% figure;plot(tVect,sigs);

fftNoise_Db = fft(noiseSig);
fftNoise_Sg = fftNoise_Db(1:N/2+1);   % FFT single sided coeff 
fftNoise_Sg(2:end) = 2*fftNoise_Sg(2:end);

% norm of FFT
absFftSig_Sg = abs(fftNoise_Sg); 
absFftSig_Sg_V = abs(fftNoise_Sg)/N; % /N  to get FFT in V
% DSP
dspSigSg = absFftSig_Sg.^2/N/N/2;

% Average powa
powFromSig = mean(noiseSig.^2)
% .5* because av power of one harmo of magnitude A is A^2/2
powFromDsp = sum(dspSigSg) 

% rg=(1:40);
rg=(1:length(F_axis_Single));
figure;
plot(F_axis_Single(rg),dspSigSg(rg));

B = 40; Tau = 1;
mySig = B*(tVect.*exp(-Tau.*tVect))';
myNoisedSig = mySig + noiseSig;
figure;plot(tVect, [mySig , myNoisedSig] );

fftNoisedSig_Db = fft(myNoisedSig);
fftNoisedSig_Sg = fftNoisedSig_Db(1:N/2+1);   % FFT single sided coeff 
fftNoisedSig_Sg(2:end) = 2*fftNoisedSig_Sg(2:end);

% norm of FFT
absFftNoisedSig_Sg = abs(fftNoisedSig_Sg); 
% DSP
dspNoisedSigSg = absFftNoisedSig_Sg.^2/N/N/2;
figure;plot(F_axis_Single(rg),[ dspNoisedSigSg(rg) ]);

% Remove noise 
dspMySigFiltered = dspNoisedSigSg - dspSigSg;

% FFT inverse
sigFiltered = ifft(fftNoisedSig_Db-fftNoise_Db);
figure;plot(tVect, mySig , 'b-o', tVect, sigFiltered,'r' );






