%% SR : 29th of January 2016
% TF of a sinus restriction

clear all;close all;clc;


% Ratio signal period over sampling period
T0ovTs= 10;
% signal duration in signal period multiple
nT0 = 1;
% signal period and freq
T0 = .1;F0 = 1/T0;
% Sampling period and freq
Ts = T0/T0ovTs;Fs = 1/Ts;
% Number of samples
nSamplesRecorded = T0ovTs*nT0 + 1;
% signal duration
sigDuration = nSamplesRecorded*Ts;
% Signal timing range
tMin = -sigDuration/2; tMax = -tMin;
tRange = linspace(tMin, tMax, nSamplesRecorded);
% FFT frequency range
F_axis_Sg = (0:round(nSamplesRecorded/2))*Fs/nSamplesRecorded;

% the signal
mySignal = sin(2*pi*F0.*tRange);

fftSignal = fft(mySignal)/nSamplesRecorded;
absFFT = abs(fftSignal);    % FFT Module in V 
absFFT_Sg = absFFT(1:round(nSamplesRecorded/2)+1);
absFFT_Sg(2:end) = 2*absFFT_Sg(2:end);

tit_ = 'Signal';
figure('name',tit_,'NumberTitle','off');
p(:,1)= plot(tRange, mySignal, 'b'); 
title(tit_);

tit_ = 'FFT of the Signal'; 
figure('name',tit_,'NumberTitle','off');
% freqIdRange = (1:50);
freqIdRange = (1:length(F_axis_Sg));
p(:,1)= plot(F_axis_Sg, absFFT_Sg, 'bo'); 
% p(:,1)= plot(F_axis_Sg(freqIdRange), absFFT_Sg(freqIdRange), 'bo'); hold on;
% p(:,2)= plot(F_axis_Sg(freqIdRange),sinOvSin(freqIdRange), 'rs'); hold off;
title(tit_);

% absFFT_theo = 

return;

% Draw tfSamplingComb  = sin(N*pi*F_axis*Ts)./sin(pi*F_axis*Ts);
% tit_ = 'sin(N*pi*F_axis*Ts)./sin(pi*F_axis*Ts)';
% figure('name',tit_,'NumberTitle','off');
% figure('name',tit_,'NumberTitle','off');
% plot(F_axis, myF, 'b-o');hold on;
% plot(F_axis, sin(pi*F_axis*Ts), 'r');hold off;
return;


% Function f(t) = sin(2*pi*t*F0) + sin(2*pi*t*2*F0) + ... +  sin(2*pi*t*(N-1)/2*F0)
% nStart = 1; nEnd = 2;
nStart = 100; nEnd = nStart ;
harmonicsFreq = F0*(nStart:1:nEnd);
sinArgs = 2*pi*tRange'*harmonicsFreq  ;
signalHarmo = sin(sinArgs);   % Each column is an harmonic
mySignal = sum(signalHarmo(:,1:end),2); % signal is the sum of each harmonic

tit_ = 'sinOvSin';
figure('name',tit_,'NumberTitle','off');
a1 = sin(pi*(F_axis-F0)*Ts*N);
b1 = sin(pi*(F_axis+F0)*Ts*N);
a2 = sin(pi*(F_axis-F0)*Ts);
b2 = sin(pi*(F_axis+F0)*Ts);
sinOvSin = a1./a2-b1./b2;
p(:,1)= plot(F_axis, a1./a2, 'bo'); hold on;
p(:,2)= plot(F_axis, -b1./b2, 'ro'); 

p(:,3)= plot(F_axis, sinOvSin, 'm'); hold off;
title(tit_);
% return;


% % sin(N*k*pi/(N-1))/sin(k*pi/(N-1)) = (-)^k
% sinOvSin = sin(N*pi*F_axis*Ts)./sin(pi*F_axis*Ts);
% % sinOvSin = (-1).^(1:length(F_axis));
% expArg = (2*pi*F_axis*Ts)'*(0:N-1);
% sumExp = exp(-1i*expArg);
% sumExp = sum(sumExp ,2);
% 
% tit_ = 'sumExp';
% figure('name',tit_,'NumberTitle','off');
% p(:,1)= plot(F_axis, sumExp, 'bo'); hold on;
% p(:,2)= plot(F_axis, sinOvSin, 'ro'); hold off;
% title(tit_);
% return;

tit_ = 'Signal';
figure('name',tit_,'NumberTitle','off');
p(:,1)= plot(tRange, mySignal, 'b'); 
title(tit_);

fftSignal = fft(mySignal);
absFFT = abs(fftSignal);    % FFT Module in V 
absFFT_Sg(1:round(N/2)+1,:) = absFFT(1:round(N/2)+1,:);
absFFT_Sg(2:end,:) = 2*absFFT_Sg(2:end,:)/N;

% convoluate FFT(signal) with FFT(sampling function)
% convSigSamp = conv(fftSignal, sumExp);

% tit_ = 'Convolution'; 
% figure('name',tit_,'NumberTitle','off');
% % freqIdRange = (1:length(F_axis_Sg));
% p(:,1)= plot((-N+1:1:N-1), abs(convSigSamp)/N , 'bo');
% title(tit_);
% 

tit_ = 'FFT Signal'; 
figure('name',tit_,'NumberTitle','off');
% freqIdRange = (1:50);
freqIdRange = (1:length(F_axis_Sg));
p(:,1)= plot(F_axis, absFFT, 'bo'); 
% p(:,1)= plot(F_axis_Sg(freqIdRange), absFFT_Sg(freqIdRange), 'bo'); hold on;
% p(:,2)= plot(F_axis_Sg(freqIdRange),sinOvSin(freqIdRange), 'rs'); hold off;
title(tit_);

return;

% tit_ = 'Mean error rates';
% figure('name',tit_,'NumberTitle','off');
% p(:,1)= plot(lambdRange, manuAvErrRate, 'r'); hold on;
% p(:,2)= plot(lambdRange, dealAvErrRate, 'b');  
% title(tit_); xlabel('lambda');
% legend(p(1,:),{['Manufacturer mean error rates'],...
% ['Dealer mean error rates']},'Location','NorthEast');
% hold off;


