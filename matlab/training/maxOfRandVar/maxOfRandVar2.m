clc;close all;clear all;
% Change default axes fonts.
set(0,'DefaultAxesFontName', 'Times New Roman')
set(0,'DefaultAxesFontSize', 16)

% Change default text fonts.
set(0,'DefaultTextFontname', 'Times New Roman')
set(0,'DefaultTextFontSize', 16)

Q = 500;
Imax = (1000:10:4000);
vB = exp(1)*Imax/Q;
vA = vB.^2*Q;

N=8e3;

Ts=7.e-5;
Fs=1/Ts;
tVect = (0:N-1)*Ts;

% Create signals
sigI = vA'*tVect.*exp(-(ones(1,size(vB,2)).*vB)'*tVect);

figure; 
plot(tVect,sigI(1:10:end,:));
title('Signals selection $$s_{i}(t)\ =\ a_{i}te^{-b_{i}t}$$ such as $$\int s_{i}(t)dt = 500$$ for i=[1..N]','interpreter', 'latex');
% xlim([0 10])
ylabel('$$s(t)$$','interpreter', 'latex');xlabel('t');

return;
%  Add noise to ideal signal
nToss = 1000;
sigma2_add_n = 9; sigma_add_n = sqrt(sigma2_add_n);
nbOfSignals = size(sigI,1);
maxI = ones(nbOfSignals,nToss);
varMaxI = ones(1,nbOfSignals );
% Retrieve Ids of Max
[Max IdsOfMax ] = max(sigI,[],2);

parfor iSig = 1:nbOfSignals 
  sigI_add_n  = ones(nToss, 1)*sigI(iSig,:)  + sigma_add_n *randn(nToss, size( sigI,2 ));
  maxI(iSig,:) = max( sigI_add_n, [], 2);
  varMaxI(iSig) = var(maxI(iSig,:), [], 2);
end




figure; 
plot(Max,varMaxI);
title('$$\sigma^2 (Z)$$ versus $$Q_{max}^{exact}$$','interpreter', 'latex');
ylabel('$$\sigma^2 (Z)$$','interpreter', 'latex');xlabel('$$S_{max}^{exact}(t)$$','interpreter', 'latex');
xlim([Imax(1) Imax(end)]);
return;


nToss = 1; nbOfVar = 1000000;
m=4;sigma2 = 9;sigma=sqrt(sigma2);
vMean = m*ones(nToss,nbOfVar);
x = vMean + sigma*randn(nToss,nbOfVar);
z=max(x,[],2);

varZExp = var(z)
meanZExp = mean(z)


nbOfVar = [1:5:1000];
xMax = 100;
dX = .1;
vX = [ -xMax:dX:xMax ];
pZ = (nbOfVar'*ones(1,length(vX))).*( ( ones(length(nbOfVar), 1)*normcdf(vX,m,sigma) ).^( (nbOfVar-1)'*ones(1, length(vX)) ) );
pZ = pZ.*(ones(length(nbOfVar), 1)*normpdf(vX,m,sigma));

normPz = sum(pZ,2)*dX;
meanZnum = sum(pZ*(vX)', 2)*dX;
varPzNum = sum(pZ*(vX.^2)',2)*dX - meanZnum.^2 ;

figure;
plot(nbOfVar, meanZnum);
title('E[z] versus N, the number of random variables $$X_{i\in \left[ 1..N \right]}$$ on which the max is computed','interpreter', 'latex');
ylabel('E[z]');xlabel('N');
xlim([1 nbOfVar(end)]);
text(50,4,'$$N=1 \Rightarrow E[Z] = E[X_{1}] = 4$$','interpreter', 'latex' );


figure;
plot(nbOfVar, varPzNum);
title('$$\sigma^2 (Z)$$ versus N, the number of random variables $$X_{i\in \left[ 1..N \right]}$$ on which the max is computed','interpreter', 'latex');
ylabel('\sigma^2 (Z)');xlabel('N');
xlim([1 nbOfVar(end)]);
text(50,4,'$$N=1 \Rightarrow \sigma^2(Z) = \sigma^2(X) = 9$$','interpreter', 'latex' );
return;

% Compute analytically the signal integral 
syms a b x;
repX = a*x*exp(-x*b);
SIG_x=int(sig_x);
formel_Int = subs(SIG_x,{a,b,x},{A,B,tVect(end)})...
  - subs(SIG_x,{a,b,x},{A,B,0})
