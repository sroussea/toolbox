syms  z1 z2 z3
f(z1,z2,z3) = (z1+z2+z3)*conj(z1+z2+z3);
g(z1,z2,z3) = abs(z1)^2+abs(z2)^2+abs(z3)^2 + 2*real( z1*conj(z2) + z1*conj(z3) + z2*conj(z3));

f_ = matlabFunction(f);
f_(1,1,1)
g_ = matlabFunction(g);
g_(1,1,1)

%simplify(f(z1,z2,z3),'Steps',50)


