%% SR : 1st of October 2015
% Implementation of the introductive example of the course :
% S:\svn\perso\Cours\Math\stats\tests\Philippe Besse - Toulouse\testParametrique.pdf
% 
% For each polling (1:pollsNumber), we take N samples and we compute N
% decisions then we derive the err type I rate.
% Then we have pollsNumber err rates and we look at its distibution.
% We take over the 2 points of view : from the manufacturer and from the
% dealer.





clear all;close all;clc;

% Sample size
N = 30;
%  exponential distribution : pdf(x) = lamb*exp(-lamb*x)
lambd0 = 1;
% number of polls
pollsNumber = 100;
% Monte Carlo size
monteSize = 100;
% Neyman Pearson parameters 
% test level
alfa = 0.05;

%% From manufacturer point of view
% H0 : lamb <= lamb0 ; H1 :lamb > lamb0 
%  manufacturer test threshold
testManuThresh = icdf('Chisquare', alfa ,2*N)/(lambd0*2*N)
testDealThresh = icdf('Chisquare', 1-alfa ,2*N)/(lambd0*2*N)


manuErrRate    = zeros(monteSize, 1);
dealErrRate    = manuErrRate;
AgreementRates =  manuErrRate;

% Lambda real value
lambdRange = linspace(lambd0*.1, 2*lambd0, 100);

manuAvErrRate    = zeros(length(lambdRange), 1);
dealAvErrRate    = manuAvErrRate;
avAgreementRates = manuAvErrRate;
iLambd = 1;

for lambd = lambdRange
% for lambd = lambd0*0.5

  % Store the actually true hypothesis
  bManuH0ActuallyTrue = lambd < lambd0;
  if ( bManuH0ActuallyTrue )
    dealErrType = 'Type II';
    manuErrType = 'Type I';
  else
    dealErrType = 'Type I';
    manuErrType = 'Type II';
  end

  figTitManuErrTypeRate = ['Manufacturer point of view. ' manuErrType ' Error rate distribution for a test size of ' num2str(alfa)];
  figTitDealErrTypeRate = ['Dealer point of view. ' dealErrType ' Error rate distribution for a test size of ' num2str(alfa)];

  parfor iMonte = 1:monteSize

    expSample = random('exp', 1/lambd, N, pollsNumber);
    meanOfSample = mean(expSample);

    %  Test computation from manufacturer side
    manuH0Test = meanOfSample >= testManuThresh;
    if ( bManuH0ActuallyTrue )
      manuErrRate( iMonte )  = sum(~manuH0Test )/pollsNumber; % err Type I
    else
      manuErrRate( iMonte )  = sum(manuH0Test )/pollsNumber;  % err Type II
    end

    %  Test computation from dealer side
    dealH0Test = meanOfSample < testDealThresh;
    if ( bManuH0ActuallyTrue )
      dealErrRate( iMonte )  = sum(dealH0Test )/pollsNumber; % err Type II
    else
      dealErrRate( iMonte )  = sum(~dealH0Test )/pollsNumber;  % err Type I
    end

    if ( bManuH0ActuallyTrue )
      AgreementRates ( iMonte ) = sum(manuH0Test & ~dealH0Test)/pollsNumber;
    else
      AgreementRates ( iMonte ) = sum(~manuH0Test & dealH0Test)/pollsNumber;    
    end

  end
  manuAvErrRate(iLambd)    = mean(manuErrRate);
  dealAvErrRate(iLambd)    = mean(dealErrRate);
  avAgreementRates(iLambd) = mean(AgreementRates);
  iLambd = iLambd + 1;

end

tit_ = 'Mean error rates';
figure('name',tit_,'NumberTitle','off');
p(:,1)= plot(lambdRange, manuAvErrRate, 'r'); hold on;
p(:,2)= plot(lambdRange, dealAvErrRate, 'b');  
title(tit_); xlabel('lambda');
legend(p(1,:),{['Manufacturer mean error rates'],...
['Dealer mean error rates']},'Location','NorthEast');



hold off;

tit_ = 'Manu/Deal mean agreement rates';
figure('name',tit_,'NumberTitle','off'); 
plot(lambdRange, avAgreementRates, 'b'); 
title(tit_); xlabel('lambda');


return;

figure('name',figTitManuErrTypeRate,'NumberTitle','off'); 
hist(manuErrRate,10)
% ;hold on;
% hist(xPop2,30);hold off;
title(figTitManuErrTypeRate); xlabel(manuErrType);
h = findobj(gca,'Type','patch');
set(h(1),'FaceColor','r','EdgeColor','w','FaceAlpha', 0.5);
% set(h(2),'FaceColor','g','EdgeColor','w' );
% legend('population 1','population 2');


figure('name',figTitDealErrTypeRate,'NumberTitle','off'); 
hist(dealErrRate,10)
title(figTitDealErrTypeRate); xlabel(dealErrType);
h = findobj(gca,'Type','patch');
set(h(1),'FaceColor','r','EdgeColor','w','FaceAlpha', 0.5);

figTitle = ['Dealer  / manufacturer agreement rates for a test size of ' num2str(alfa)];
figure('name',figTitle,'NumberTitle','off'); 
hist(AgreementRates,10)
title(figTitle); xlabel('Dealer  / manufacturer agreement rates');
h = findobj(gca,'Type','patch');
set(h(1),'FaceColor','r','EdgeColor','w','FaceAlpha', 0.5);

