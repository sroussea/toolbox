clear all;close all;clc;

% To know which value is an extrema, I compute extrema value distribution :
% http://www.mathworks.fr/fr/help/stats/extreme-value-distribution.html
% Then I consider a value is not a maximum if its probability of not
% being a maximum is greater than 30%


mySeries = [ rand(1,100) ];

% get parameters of the extrema values density function
[parmhat, parmci] = evfit(-mySeries );
% compute maxima values proba density function
yy= (0:.01:2);maxValPdf = evpdf(-yy, parmhat(1), parmhat(2));

% compute probabilities for any values to be greater than its current
% values = 1 - F(current Ratio) where F is the cumulative function of maxValPdf

values = [ 0.2 0.5 0.7 0.9 1 1.1 2];
for idRatio = 1:length(values)
  [xx, ids] = find( yy >= values(idRatio) );
  probaValueGt(idRatio) = 1 - sum(maxValPdf(1:ids(1)))*.01;
end
