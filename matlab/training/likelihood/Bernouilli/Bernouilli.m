%% SR : 25th of August 2015
% Try to see how the score and the Fisher information behaves for a
% Bernouilli random vector

addpath S:\svn\DSP\common\matlab


clear all;close all;clc;
set(0,'defaulttextinterpreter','latex');

% rand var vect size
nVar = 300;
% Sample number
nSamp = 100;
% Bernouilli rea parameter
muReal = 1/2;
% Generate bernouilli samples
% xObs = binornd(ones(nVar,nSamp),muReal);

% Generate a gaussian distribution, then move [0;1] with a threshold
% 
xObs = randn([nVar,nSamp]) + muReal;
geMuIds = find(xObs >= muReal/5);
ltMuIds = find(xObs < muReal/5);
xObs( ind2sub(size(xObs),geMuIds ) ) = 1;
xObs( ind2sub(size(xObs),ltMuIds ) ) = 0;

% likelihood function
% mu : column vector ; X : array of samples nVar X nSamp
%  column is sample constant; row is parameter constant
logLikeHood = @(mu, X)log(mu)*sum(X) + log( 1 - mu)*(size(X,1) - sum( X));

% score for each mu value
score = @(mu, X)(1./mu)*sum(X) - (1./( 1 - mu))*(size(X,1) - sum( X));

%  compute log likelihood for nSamp samples and a vector of means
muRange = (0.1:.01:.9)';
logLikeEstim = logLikeHood( muRange, xObs);

%  compute score
scoreEstim = score( muRange, xObs);
%  compute score mean
scoreMean = mean(scoreEstim, 2) ;

figure('name','log likelihood');
plot(muRange, logLikeEstim);

title_ = 'Score moyen';
figure('name', title_);
plot(muRange, scoreMean);
title(title_,'interpreter', 'latex');
xlabel('Parametre $\Theta$');ylabel('E[S($\Theta$)]');

