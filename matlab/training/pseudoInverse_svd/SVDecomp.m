% ----------------------------------------------------------
% https://fr.wikipedia.org/wiki/D%C3%A9composition_en_valeurs_singuli%C3%A8res

% Let f a linear application : E^m ->  E^n with m>n :
%                               u |->  v
% M [m,n] the matrix of f
% Then M = U.S.V'
% U and V are rotations (unitary transformation such as V.V' = id(E^m), U.U' = Id(E^n) )
% while S is an homothety
% Rank of M is equal to the rank of S which is equal to the number of non
% zero diagonal elements of S
% In the case where M is made of a non free column vectors family, the dim of
% M image is fewer than n.
% ----------------------------------------------------------

close all; clear all


% Let's define M= [m, n] matrix m>n
% First, let's assume that rank(M) = n => rank(S) = n
M = [ 1 3 5 7; 2 4 6 8; 1 3 6 8]';
[U, S,V] = svd(M);
 
% Let's define M= [m, n] matrix m>n
% Let's assume that rank(M) = p < n => rank(S) = p
M = [ 1 3 5 7; 2 4 6 8; 4 8 12 16]';
[U, S,V] = svd(M);


