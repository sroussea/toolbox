% ----------------------------------------------------------
% Let M [nxp] linear application from E^p to E^n with n>p
% and b belonging to E^p known , y0 belonging to E^n . Then we build y as :
%  y0 = M*b0
% Then we show that b0 is exactly pinv(M)*y0
% Meaning that if we know M and y is an image of M then we can deduce b0 exactly
%
% Conversely, let y != y0 belonging to E^n so that || y -y0|| << 1.
% For instance y can be derived from n noisy measures, even if M is
% the true model, pinv(M)*y = b != b0  
% b is only the approximation of b0 is the least square meaning
% It is the orthogonal projection of y on the space M(E^p)
% ----------------------------------------------------------

close all; clear all

M = [ 1 complex(1,1) ; complex(0,-1) 1 ; 2 0; -1 3]
b = [ 2 complex(1,-1)].'
y=M*b

pM = pinv(M);

pM*y


