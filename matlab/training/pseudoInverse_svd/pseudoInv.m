% ----------------------------------------------------------
% Check pseudo inv matlab algo and analytical formula
%
% See also pseudoInv2 for more insight

% ----------------------------------------------------------

close all; clear all

M = [ 1 1 0; -1 1 1]'
b = [ 2 1 0]';
pM = pinv(M)

% Since rank(M) = nbCol(M), there's an analytical form for the pseudo inv :
% pInv(M) = inv( adjoint(M).M) ).adjoint(M)
pMan = ( M'* M )\M'

% Least square result of b = M*x => x = pM*b
x = pMan*b;
M*x


M*pMan*M

return

M1 = [ 1 0 0; -1 1 1]';
M2 = [ 0 -1 0; 0 0 0]';
Z=horzcat( vertcat(M1,zeros(size(M1))) , vertcat( zeros(size(M2)), M2));
pZ = pinv(Z)
Z*pZ*Z

v1 = [1 1 2]'
pMan*v1
pZ*[1 1 2 1 1 2 ]'

