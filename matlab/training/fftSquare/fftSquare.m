clc;close all;clear all;

N=1000;
Fs=1e3;
Ts=1/Fs;
timeSlot = N*Ts;
tVect = (0:N-1)*Ts;
F_axis = (0:N-1)*Fs/N;
F_axis_Single = (0:N/2)*Fs/N;

F0=Fs/100;T0=1/F0;

% First we check the form of the FFT of a square
% We verify that FFT coeff are maximum for the odd multiple of the square 
% signal fundamental frequency  F0 with sides lobes amplitude decreasing like
% sinc
sqDuration=T0/Ts; % in Points
sqPulse = square(2*pi*F0*[0:sqDuration-1]/Fs);%Square Pulse of T0 duration
signal = [ sqPulse zeros(1,N-sqDuration) ]; 

plot(tVect, signal)

absFftSig=abs(fft(signal))/N;
absFftSig_Sg=absFftSig(1:N/2+1);   % FFT single sided coeff in Vpeak 
absFftSig_Sg(2:end)=2*absFftSig_Sg(2:end);

figure(2);
plot(F_axis_Single, absFftSig_Sg);
return;
