clc;close all;clear all;

syms a t f s;


f=a*t;
f=a*t*heaviside(-t+3);
F = laplace(f, t, s)

return

x=[0:.5:10];
f_eval=subs(f,{t,a},{x,1});

figure(1);plot(x,f_eval)

F = laplace(f, t, s)