clc;close all;clear all;

N=5000;
Fs=1e3;
Ts=1/Fs;
timeSlot = N*Ts;
tVect = (0:N)*Ts;
F_axis = (0:N)*Fs/N;
F_axis_Single = (0:N/2)*Fs/N;

sig=.5*sin(2*pi*Fs/5*tVect)';
noise=randn(size(sig,1),1);
sigNoised=sig+ noise;

fft1Noise_Db_V=fft(noise)/N;   % FFT double sided coeff in Vpeak / 2
fft1Sig_Db_V=fft(sig)/N;   % FFT double sided coeff in Vpeak / 2
fft1SigNoised_Db_V=fft(sigNoised)/N;   % FFT double sided coeff in Vpeak / 2
fft1SigNoisedSg=abs(fft1SigNoised_Db_V(1:N/2+1));
fft1SigNoisedSg(2:end,:) = 2*fft1SigNoisedSg(2:end,:);
dsp1_Sg = (0.5*fft1SigNoisedSg.^2);
dspMean1_Sg = mean(dsp1_Sg ,2);
avPow1FromDsp = sum(dspMean1_Sg);
avPow1FromSig = mean(mean(sigNoised.^2));

roiT=(1:round(N/10));
N2=length(roiT);
F_axis2 = (0:N2)*Fs/N2;
F_axis_Single2 = (0:N2/2)*Fs/N2;

sigNoised2=sigNoised(roiT);

fft2SigNoised_Db_V=fft(sigNoised2)/N2;   % FFT double sided coeff in Vpeak / 2
fft2SigNoisedSg=abs(fft2SigNoised_Db_V(1:length(fft2SigNoised_Db_V)/2+1));
fft2SigNoisedSg(2:end,:) = 2*fft2SigNoisedSg(2:end,:);
dsp2_Sg = (0.5*fft2SigNoisedSg.^2);
dspMean2_Sg = mean(dsp2_Sg ,2);
avPow2FromDsp = sum(dspMean2_Sg);
avPow2FromSig = mean(mean(sigNoised2.^2));

figure;
plot(F_axis_Single,dspMean1_Sg,'b');hold on;
plot(F_axis_Single2,dspMean2_Sg,'r');hold off;
title('DSP');

figure;
plot(F_axis_Single,fft1SigNoisedSg,'b');hold on;
plot(F_axis_Single2,fft2SigNoisedSg,'r');hold off;
title('FFT');

% sr 9th of June
% the more you've got signal samples, the lesser var of DSP
% Moreover, var(dspMean2_Sg)/var(dspMean1_Sg) ~1/N
% which means that my estimator is consistent
var(dspMean2_Sg)/var(dspMean1_Sg);


return;


figure(2);
plot(F_axis_Single,absFftSig_Sg_V,'b');
title('absFFT signal in Q peak');

figure(3);
plot(tVect,ifftSig_Db_V,'b');
title('iFftSig signal in Q peak');

figure(4);
plot(tVect,ifftDiffFFT,'b');
title('ifftDiffFFT signal in Q peak');


return;
