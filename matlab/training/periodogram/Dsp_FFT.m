close all;clear all;


N = 2048*1; T=.1; F = 1/T;
Fs = 4*(1/T);
Ts=1/Fs;
timeSlot = N*Ts;
tVect = (0:N-1)*Ts;

F_axis = (0:N-1)*Fs/N;

A=.02;
x(:,1)=A*sin(2*pi*F*tVect);
x(:,2)=A*sin(2*pi*F/2*tVect);
noiseVect = sqrt(A)*randn(1,N)' + x(:,1)+ x(:,2);

raisetime = 4;
AA = 8;
pulseSig =  tVect.^2.*exp(-tVect/raisetime);
pulseSigNoised = noiseVect + pulseSig';
figure;
plot( tVect, pulseSigNoised);


fft_x=fft(pulseSigNoised);
absFFT_Vrms=abs(fft_x)/sqrt(2)/N;    % FFT Module in V rms
absFFT_V=abs(fft_x)/N;    % FFT Module in V 


psd_V2Rms2=absFFT_Vrms.^2;  % Power spectrum density in V^2rms^2
psd_V2=absFFT_V.^2;  % Power spectrum density in V^2

df=Fs/N;      % Frequency resolution
psd_V2Rms2perHz=1/df*psd_V2Rms2; % PSD in V^2rms^2/Hz
psd_V2perHz=1/df*psd_V2; % PSD in V^2/Hz

f1=figure;
plot(F_axis,absFFT_V,'b');
title('absFFT_V');
f2=figure(2);
plot(F_axis,10*log10(psd_V2perHz),'b');
title('10*log10(psdV2perHz)');


avgPowFft_f = sum(psd_V2)
averPower_t = mean(pulseSigNoised.^2)

h = spectrum.periodogram('Blackman-Harris');
hopts = psdopts(h,pulseSigNoised)
set(hopts,'Fs',Fs,'SpectrumType','onesided');
f3=figure;
myPsd = psd(h,pulseSigNoised,hopts)
avgPowPerio_f = avgpower(myPsd)
plot(myPsd)

return

sizeOfOnePgram = (N/M);



%x=randn(1,N);



xb=reshape(x,sizeOfOnePgram,M); % xb est une matrice de dimension (N/M,M).
%s=1/sizeOfOnePgram*abs(fft(xb)).^2;% pour obtenir la moyenne des M spectres.
myDsp=abs(fft(x)).^2;% pour obtenir la moyenne des M spectres.
%s=mean(1/N*abs(fft(xb)).^2,2);% pour obtenir la moyenne des M spectres.

pow_t=1/N*sum(x.^2);
pow_f=1/(N*N)*sum(s);
%pow_f=1/(sizeOfOnePgram)*sum(s);

f2=figure(2);
plot(F_axis,s,'bo');
return

 
return

pow_t=1/sizeOfOnePgram*sum(xb.^2)
pow_f=sum(s);

meanDsp = mean(s)
varDsp=var(s)

f1=figure(1);
plot([1:sizeOfOnePgram],x(1:sizeOfOnePgram),'b');

