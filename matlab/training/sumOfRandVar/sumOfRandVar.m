clc;close all;clear all;

N = 10000;
sigma1 = 3; m1=10; sigma2 = 5; m2 = 20;
x1 = m1 + sigma1*randn(1,N);
x2 = m2 + sigma2*randn(1,N);

varX1 = var(x1)
varX2 = var(x2)
varX1pX2 = var(x1+x2)
meanX1pX2 = mean(x1+x2)

var3fX1=var(3*x1)
