clear all; close all;clc;

setConso = dataset('File','consommation.txt', 'ReadObsNames',true)
matConso = double(setConso,setConso.Properties.VarNames)
% Reduction/centering of data
centerData = bsxfun(@minus,matConso,mean(matConso))
redData = bsxfun(@rdivide,centerData,sqrt(var(centerData,0)))

[pc,score,latent,tsquare] = princomp(redData);
% pc : each column contains vect p. of correlation Matrix
% i.e princ. dir. in vars space
% The columns are in order of decreasing component variance.
% To get princ. comp. , compute : X.pc where X is centered/reduced
pc
% score = X.pc = Fs the representation of observations in the principal component space.
% Rows of SCORE correspond to observations, columns to components
score
%latent :  containing the eigenvalues of the covariance matrix of ingredients
latent
%tsquare : Hotelling's T2 statistic for each data point.



% 
% biplot(pc(:,1:2),'VarLabels',...
%  setConso.Properties.VarNames)


% figure; boxplot(redData)
% Correlation matrix
C = corr(redData)
% Columns of Us give the principal axis basis {Us} in the variable space 
% <=> space of synthetic variables
[Us,D] = eig(C)

%rows of Fs = Individuals in the Us basis <=> principal components
disp('rows =Individus (Composantes principales) dans l''espace des variables synthétiques')
Fs = redData*Us

% columns of Vs = principal axes in individuals space along the 
disp('rows = Composantes principales dans l''espace des variables synthétiques')
Vs = bsxfun(@rdivide,Fs,std(Fs))

% rows of Gs = Vars in the Vs basis 
Gs = corr(redData,Vs)
% We should have redData'*Vs/8 = corr(redData,Vs)
% but we have actually redData'*Vs/7 = corr(redData,Vs) => corr should
% implement the unbiased std estimator using 1/(N-1) normalization factor

figure;
% subplot(2,1);

biplot(pc(:,1:2),'Scores',score(:,1:2),'VarLabels',...
 setConso.Properties.VarNames )

figure;
biplot(Us(:,1:2),'Scores',Fs(:,1:2),'VarLabels',...
 setConso.Properties.VarNames )


