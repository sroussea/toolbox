% % SR 23rd of February, 2014
%  Check if PCA could disentangle some degenerescence.
%  



clear all; close all;clc;

% we split observations in 3 orthogonal spaces with equivalent 
% weights
nVar =  3;
nObs = 60;

% 2 population patterns very close to each others (1/10 average
% on 2 vars) cannot get discriminate if we add a noise with std = 1/20
w1 = linspace(0,10,nObs)';
a = .1; b = 2;
w2 = exp(a*w1)+b;
w3 = exp(a*1.05*w1)+b;
w4 = 0.1*w1;

p1 = [w1 w2 w4]; 
p2 = [w1 w3 (w4+.087)]; 

sig = 0.01;
nPops = 2;
% we add a slight noise to make all vars different
X = vertcat(p1, p2);
X = X + sig*randn(size(X));

% we add more noise on var2
%%% !!!! Good news, we still able to distinguish 2 separate clouds in 
% the 2 princip axes
% X(:,2:3) = X(:,2:3) + .05*randn(size(X(:,2:3)));

%X((1:iSkip:nObs),:) = randn(nObs/iSkip,nVar)
redData = bsxfun(@minus,X,mean(X));
redData = bsxfun(@rdivide,redData,sqrt(var(redData,0)));


[pc,score,latent,tsquare] = princomp(redData);
% pc : each column contains vect p. of correlation Matrix
% i.e princ. dir. in vars space
% The columns are in order of decreasing component variance.
% To get princ. comp. , compute : X.pc where X is centered/reduced
pc
% score = X.pc = Fs the representation of observations in the principal component space.
% Rows of SCORE correspond to observations, columns to components
score
%latent :  containing the eigenvalues of the covariance matrix of ingredients
latent
%tsquare : Hotelling's T2 statistic for each data point.



% 
% biplot(pc(:,1:2),'VarLabels',...
%  setConso.Properties.VarNames)


% figure; boxplot(redData)
% Correlation matrix
C = corr(redData);
% Columns of Us give the principal axis basis {Us} in the variable space 
% <=> space of synthetic variables
[Us,D] = eig(C);

% elbow
dD = diag(D);
figure;plot(dD(1:3)/sum(dD),'b-o')

%rows of Fs = Individuals in the Us basis <=> principal components
disp('rows =Individus (Composantes principales) dans l''espace des variables synthétiques')
Fs = redData*Us;

% columns of Vs = principal axes in individuals space along the 
disp('rows = Composantes principales dans l''espace des variables synthétiques')
Vs = bsxfun(@rdivide,Fs,std(Fs));

% rows of Gs = Vars in the Vs basis 
Gs = corr(redData,Vs);
% We should have redData'*Vs/8 = corr(redData,Vs)
% but we have actually redData'*Vs/7 = corr(redData,Vs) => corr should
% implement the unbiased std estimator using 1/(N-1) normalization factor
% 
% figure;
% biplot(Us(:,1:2),'Scores',Fs(:,1:2),'VarLabels',...
%  {'A','B','C','D','E','F','G','F','H'} )

% subplot(2,1);

% varLab = {'A','B','C','D','E','F','G','F','H','I','J','K'};
varLab = {'A','B','C'};
figure;
biplot(pc(:,1:2),'Scores',score(:,1:2),'VarLabels',varLab');title('pc1 vs pc2');
figure;
biplot(pc(:,2:3),'Scores',score(:,2:3),'VarLabels',varLab');title('pc2 vs pc3');
figure;
biplot(pc(:,[1 3]),'Scores',score(:,[1 3]),'VarLabels',varLab');title('pc1 vs pc3');
% 
figure;
p(:,1)=plot(X(:,1),X(:,2),'bo');hold on;
p(:,2)=plot(X(:,2),X(:,3),'ro');
p(:,3)=plot(X(:,1),X(:,3),'go');hold off;
title('Cartesian representation');
legend(p(1,:),{['2 vs 1'],['3 vs 2'],...
['3 vs 1']});
% 
