%% SR : 15th of April 2014
% We considere here 2 populations
% we have variables distributions so that : 
% population1 ~ N(mu1, sigma)
% population2 ~ N(mu2 + 3*sigma, sigma)

% So, looking separatly each var histo does lead to an efficient population
% discrimination
% Here I show that the more we add variables of the same overlap, the more
% we improve the population dicrimination
% Comes from the distance definition between 2 individuals which is the sqrt of a 
% squared numbers sum where the number of terms is equal to the number
% of variables. The more we have dimensions, the more we increase the
% distance between the individuals.
% So, even though each variable taken separatly brings the same
% discrimination ability, then considering a linear combination of them
% brings more discrimination ability.  

%% SR : 18th of April 2014
% Eventually I'll take the linear combination of reducted vars to avoid
% performing algebraic operation between values of different unity.
% In other hand I don't center the distribution of the var : the average
% value is the parameter intended to discriminate the populations.

% Discrimination efficiency depends on the given realization of the random vars.
% Means that the standard distribution computed from the reference 
% dosimetry batch is kind of random itself.
% I think we should derive parameters of the standard deriviation along
% with a confident range. Then, we'll compare the standard confident range
% with the parameters confident range of the distribution obtained with the
% studied dosimeter to guess its genuine population.

clear all; close all;clc;

addpath S:\srGir\Toolbox\matlab

% we study 2 populations
nPop = 2;
nObs = nPop*1000;
nHitPerPop = nObs/nPop;

stdX = 1; stdY = 1.5;stdZ = 1; stdW = 1;
% xMeanPop1 = 5; xMeanPop2 = xMeanPop1 + stdX*2;
xMeanPop1 = 1; xMeanPop2 = xMeanPop1 + 4;
xPop1 = xMeanPop1 + stdX*randn( nHitPerPop, 1);
xPop2 = xMeanPop2 + stdX*randn( nHitPerPop, 1);

yMeanPop1 = 1; yMeanPop2 = yMeanPop1 + 4;
yPop1 = yMeanPop1 + stdY*randn( nHitPerPop, 1);
yPop2 = yMeanPop2 + stdY*randn( nHitPerPop, 1);

zMeanPop1 = 50; zMeanPop2 = zMeanPop1 + 1;
zPop1 = zMeanPop1 + stdZ*randn( nHitPerPop, 1);
zPop2 = zMeanPop2 + stdZ*randn( nHitPerPop, 1);

wMeanPop1 = 150; wMeanPop2 = wMeanPop1 + 1;
wPop1 = wMeanPop1 + stdW*randn( nHitPerPop, 1);
wPop2 = wMeanPop2 + stdW*randn( nHitPerPop, 1);

figTitle = ['Hits FWMH (in pixels). Number of hits per population = ', num2str(nHitPerPop)];
figure('name',figTitle,'NumberTitle','off'); 
hist(xPop1,30);hold on;
hist(xPop2,30);hold off;
title(figTitle); xlabel('FWMH in pixels');
h = findobj(gca,'Type','patch');
set(h(1),'FaceColor','r','EdgeColor','w');
set(h(2),'FaceColor','g','EdgeColor','w');
legend('population 1','population 2');

figTitle = ['Hits profile peak-to-peak gap (in gray level). Number of hits per population = ', num2str(nHitPerPop)];
figure('name',figTitle,'NumberTitle','off'); 
hist(yPop1,30);hold on;
hist(yPop2,30);hold off;
title(figTitle); xlabel('Hits profile peak-to-peak gap in gray level');
h = findobj(gca,'Type','patch');
set(h(1),'FaceColor','r','EdgeColor','w');
set(h(2),'FaceColor','g','EdgeColor','w');
legend('population 1','population 2');

figTitle = ['z. Number of hits per population = ', num2str(nHitPerPop)];
figure('name',figTitle,'NumberTitle','off'); 
hist(zPop1,30);hold on;
hist(zPop2,30);hold off;
title(figTitle); xlabel('z');
h = findobj(gca,'Type','patch');
set(h(1),'FaceColor','r','EdgeColor','w');
set(h(2),'FaceColor','g','EdgeColor','w');
legend('population 1','population 2');

figTitle = ['w. Number of hits per population = ', num2str(nHitPerPop)];
figure('name',figTitle,'NumberTitle','off'); 
hist(wPop1,30);hold on;
hist(wPop2,30);hold off;
title(figTitle); xlabel('w');
h = findobj(gca,'Type','patch');
set(h(1),'FaceColor','r','EdgeColor','w');
set(h(2),'FaceColor','g','EdgeColor','w');
legend('population 1','population 2');


% figTitle = ['Composite variable. Number of hits per population = ', num2str(nHitPerPop)];
% figure('name',figTitle,'NumberTitle','off'); 
% hist((xMeanPop2 - xMeanPop1)*xPop1 + (yMeanPop2 - yMeanPop1)*yPop1,30);hold on;
% hist((xMeanPop2 - xMeanPop1)*xPop2 + (yMeanPop2 - yMeanPop1)*yPop2,30);hold off;
% title(figTitle); xlabel('Arbitrary Unit');
% h = findobj(gca,'Type','patch');
% set(h(1),'FaceColor','r','EdgeColor','w');
% set(h(2),'FaceColor','g','EdgeColor','w');
% legend('population 1','population 2');

figTitle = ['Amplitude pic-pic VS FWHM'];
figure('name',figTitle,'NumberTitle','off'); 
p(:,1) = plot(xPop1,yPop1,'go','MarkerSize',3); hold on;
p(:,2) = plot(xPop2,yPop2,'ro','MarkerSize',3); hold off;
xlabel('FWMH en pixels');ylabel('Amplitude pic-pic en niveau de gris');
title(figTitle );
legend(p(1,:),{['Population 1'], ['Population 2']});

x = vertcat( xPop1, xPop2 );
y = vertcat( yPop1, yPop2 );
z = vertcat( zPop1, zPop2 );
w = vertcat( wPop1, wPop2 );

%  We only take the first 2 vars into account
X = horzcat(x,y);
redData = bsxfun(@minus,X,mean(X));
redData = bsxfun(@rdivide,redData,sqrt(var(redData,0)));
 [pc,score,latent,tsquare] = pca(redData);
varLab = {'x','y'};
 figure;
 plotPCA(varLab, pc, score, latent);

 

return 

% add a third variable
X = horzcat(x,y,z);
redData = bsxfun(@minus,X,mean(X));
redData = bsxfun(@rdivide,redData,sqrt(var(redData,0)));
% [pc,score,latent,tsquare] = pca(redData);
varLab = {'x','y','z'}; 
% figure;plotPCA(varLab, pc, score, latent);

% add a fourth variable
X = horzcat(x,y,z,w);
redData = bsxfun(@minus,X,mean(X));
redData = bsxfun(@rdivide,redData,sqrt(var(redData,0)));
[pc,score,latent,tsquare] = pca(redData);
varLab = {'x','y','z','w'}; 
% figure;plotPCA(varLab, pc, score, latent);

%  We compute the standard distributions of populations 1 & 2 for the
%  principal component containing the most of inertia
Xpop1 = horzcat(xPop1,yPop1,zPop1,wPop1);
redXpop1 = bsxfun(@rdivide,Xpop1,sqrt(var(Xpop1,0)));
scoreRedPop1 = redXpop1*pc;
mean(scoreRedPop1)
var(scoreRedPop1)

Xpop2 = horzcat(xPop2,yPop2,zPop2,wPop2);
redXpop2 = bsxfun(@rdivide,Xpop2,sqrt(var(Xpop2,0)));
scoreRedPop2 = redXpop2*pc;
mean(scoreRedPop2)
var(scoreRedPop2)


figTitle = ['Xpop1 Xpop2'];
figure('name',figTitle,'NumberTitle','off'); 
hist( Xpop1(:,1),30);hold on;
hist( Xpop2(:,1),30);hold off;
title(figTitle); xlabel('Arbitrary Unit');
h = findobj(gca,'Type','patch');
set(h(1),'FaceColor','r','EdgeColor','w');
set(h(2),'FaceColor','g','EdgeColor','w');
legend('population 1','population 2');



figTitle = ['Best principal component distribution with scoreRedPop'];
figure('name',figTitle,'NumberTitle','off'); 
hist( scoreRedPop1(:,1),30);hold on;
hist( scoreRedPop2(:,1),30);hold off;
title(figTitle); xlabel('Arbitrary Unit');
h = findobj(gca,'Type','patch');
set(h(1),'FaceColor','r','EdgeColor','w');
set(h(2),'FaceColor','g','EdgeColor','w');
legend('population 1','population 2');


figure('name','raw data','NumberTitle','off'); 
scatter(redXpop1(:, 1), redXpop1(:, 2),'fill'); hold on;
scatter(redXpop2(:, 1), redXpop2(:, 2)); hold off;
title('raw data');


% Y = tsne(redData, 'Algorithm','exact','Distance','seuclidean');
% Y = tsne(redData, 'Algorithm','exact','Distance','cosine' );
% Y = tsne(redData, 'Algorithm','exact','Distance','correlation');
% Y = tsne(redData, 'Algorithm','exact','Distance','spearman');
% Y = tsne(redData, 'Algorithm','exact','Distance','hamming');

Y = tsne(redData, 'Algorithm','exact','Distance','cosine');
figure('name','t-SNE Visualization','NumberTitle','off'); 
scatter(Y(1:1000,1), Y(1:1000, 2),'filled'); hold on;
scatter(Y(1001:end,1), Y(1001:end, 2)); hold off;
title('t-SNE cosine ');

figure('name','PCA','NumberTitle','off'); 
scatter(scoreRedPop1(:,1), scoreRedPop1(:, 2),'fill'); hold on;
scatter(scoreRedPop2(:,1), scoreRedPop2(:, 2)); hold off;
title('PCA');
return

% Generate a little sample of pop1
nHitPerPop = 1000;
xSampPop1 = xMeanPop1 + stdX*randn( nHitPerPop, 1);
ySampPop1 = yMeanPop1 + stdY*randn( nHitPerPop, 1);
zSampPop1 = zMeanPop1 + stdZ*randn( nHitPerPop, 1);
wSampPop1 = wMeanPop1 + stdW*randn( nHitPerPop, 1);



Y = horzcat(xSampPop1,ySampPop1,zSampPop1,wSampPop1);
redDataSample = Y;
redDataSample = bsxfun(@rdivide,redDataSample,sqrt(var(redDataSample,0)));
scoreSample = redDataSample*pc;
% scoreSample = Y*pc;
mean(scoreSample)
var(scoreSample)


negLL = @(teta, Y, n)(-1)*(-n/2*log(2*pi*teta(1))-1/(2*teta(1))*sum( (Y-teta(2)).^2 ) );
[tetaEstim,fval] = fminsearch(@(teta) negLL(teta, scoreSample(:,1),nHitPerPop) ,[1, 100])




