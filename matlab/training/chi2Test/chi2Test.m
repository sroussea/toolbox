
clear all;close all;clc;

sampleSize = 500;
sig1 = .1; m1 = 1;
s1 = m1 + sig1 * randn(sampleSize,1);  
s2 = .4 + 0.3 * randn(sampleSize,1);  
% mySample = [s1;s2];
mySample = s1;

% % Compute bin centers of the original sample
[mySampElts,mySampCenters] = hist(mySample,sampleSize);

% Chi2 test on the original data
[h,p,st] = chi2gof(mySample,'Alpha',0.05,'NParams',2)

% Resample the data
reSampBinSize  = 50;
[nelements,centers] = hist(mySample,reSampBinSize);
myReSampling = zeros(sampleSize,1);
iStart = 1;iEnd = 1;
for i=1:length(centers)
  if nelements(i) > 0
    iEnd = iStart + nelements(i) - 1;
    myReSampling(iStart:iEnd) = centers(i);
    iStart = iStart + nelements(i) ;
  end
end

% Chi2 test on the resampled data
[h,p,st] = chi2gof(myReSampling,'Alpha',0.05,'NParams',2)

% figure; plot(

return;


sampFilt = binornd(1,.05,1,length(mySample));
% disp 'outliers nb = ', length(find(sampFilt>0))
length(find(sampFilt>0))
mySampFiltered = mySample ;
mySampFiltered (logical(sampFilt)) = 1.5;

dist = makedist('normal','mu',m1,'sigma',sig1);

[h,p] = chi2gof(mySampFiltered,'Alpha',0.05,'NParams',0)
[h,p] = adtest(mySampFiltered,'Distribution',dist)
[h,p] = kstest((mySampFiltered-m1)/sig1,'Alpha',0.05)

return;
[h,p] = chi2gof(mySampFiltered ,'Alpha',0.05,'NParams',2)
return;


%   y = randn(3e4,1); % vector with random numbers
  pdfs = PDFsampler(mySample,nBins );

  newSampleSize = length(mySample);
  r = zeros(newSampleSize,1);
  for i=1:newSampleSize
    r(i) = pdfs.nextRandom;
  end
    
  figure; hist(mySample,nBins);
  
  figure; hist(r,nBins);
return;

xi = -3:0.1:3;
s1 = 1 + 0.3 * randn(100,1);  
s2 = -1 + 0.3 * randn(100,1);  
[f, xs] = ksdensity([s1;s2]);


 % sample = 1.9*rand(1000,1);           % sample from Uniform distribution

x=[-1:.1:1]';
sample = -(x).^3;
figure;plot(x,sample);

pd = fitdist(sample, 'Normal');
[h,p,st] = chi2gof(sample, 'CDF',pd, 'Alpha',0.05)

return
N=100;
x = 1*randn(1,N)+3;
mean(x)
std(x)
[h,p] = chi2gof(x,'Alpha',0.05)

figure;hist(x)

