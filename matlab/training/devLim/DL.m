
%% SR : 27th of October 2015
% 
% Taylor dev coeffs and polynomial model coeff derived from a least square 
% fit are the same for a non noised signal
% As soon as you add a slight noise in the curve to be fitted, a large
% difference betwen coeffs occurs.


clc;close all;clear all;
% Change default axes fonts.
set(0,'DefaultAxesFontName', 'Times New Roman')
set(0,'DefaultAxesFontSize', 16)

% Change default text fonts.
set(0,'DefaultTextFontname', 'Times New Roman')
set(0,'DefaultTextFontSize', 16)

addpath S:\svn\DSP\common\matlab


syms x tayF
a= 1/2;
taylorOrder = 5;
polyDeg = taylorOrder - 1;

f(x) = x.*exp(-a*x);

x0 = 1/a - 1/2;
tayF(x) = taylor( f(x), x, x0, 'Order', taylorOrder)
taylorCoeffs = sym2poly(tayF)  % return coeff in descending power order

xShift = 1/2;
xShiftPlot = 5;
dX = 0.05;
xRange = ( x0 - xShift:dX:x0 + xShift );
plotXRange = (xRange(1) - xShiftPlot:dX:xRange(end) + xShiftPlot);

theoCurv = f(plotXRange);
tayCurv = tayF(plotXRange);

[xData, yData] = prepareCurveData( xRange, f(xRange) );
% Set up fittype and options.
fitType = ['poly' num2str(polyDeg)];
ft = fittype( fitType );

% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft );
fitCoeffs  = coeffvalues(fitresult) % return coeff in ascending power order
fitCurv = feval( fitresult, plotXRange);

figure; 
hPlot(:,1) = plot(plotXRange, theoCurv, 'b');
hold on;
hPlot(:,2) = plot(plotXRange, tayCurv, 'rx');
hPlot(:,3) = plot(plotXRange, fitCurv, 'go');
hold off;
legend(hPlot(1,:),{ 'Theo curv', 'taylor approx', 'fit '} );

noisedF = f(xRange) + .01*randn(size(xRange)) ;
[xData, yData] = prepareCurveData( xRange,noisedF );
% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft );
fitCoeffs  = coeffvalues(fitresult) % return coeff in ascending power order
fitCurv = feval( fitresult, plotXRange);
figure; 
hPlot(:,1) = plot(xRange, noisedF, 'b');hold on;
hPlot(:,2) = plot(xRange, f(xRange),'r');
hPlot(:,3) = plot(plotXRange, fitCurv, 'go');
xlim([xRange(1) xRange(end)]);
hold off;
legend(hPlot(1,:),{ 'noisedF', 'theo', 'fit '} );

return


x0 = 2
tayF(x) = taylor(f(x),x,x0,'Order',5)
sym2poly(tayF)
