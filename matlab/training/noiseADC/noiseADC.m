clc;close all;clear all;

Fs=1e3;
N=Fs*50;
Ts=1/Fs;
timeSlot = N*Ts;
tVect = (0:N-1)*Ts;
F_axis = (0:N-1)*Fs/N;
F_axis_Single = (0:N/2)*Fs/N;

F0=Fs/40;T0=1/F0;

% -1 & 1
signal=sign( rand( 1, N*Ts/T0 ) -.5);
signal_a=repmat(signal,T0/Ts/2,1);
signal_b=repmat(-signal,T0/Ts/2,1);
signal=[signal_a ;signal_b];
signal=reshape(signal, 1, N);

% % 0 & 1 & 2 
% tmp=sign(rand(1,N*Ts/T0)-.5);
% signal_1=repmat(10*(tmp>0.2),2*T0/Ts/10,1);
% signal_m1=repmat(1*(tmp<0),3*T0/Ts/10,1);
% tmp=sign(rand(1,N*Ts/T0)-.5);
% signal_2=repmat(1*(tmp>0),5*T0/Ts/10,1);
% 
% signal=[signal_1 ;signal_m1;signal_2];
% signal=reshape(signal, 1, N);

figure;
plot(tVect, signal);

absFftSig=abs(fft(signal))/N;
absFftSig_Sg=absFftSig(1:N/2+1);   % FFT single sided coeff in Vpeak 
absFftSig_Sg(2:end)=2*absFftSig_Sg(2:end);

figure(2);
plot(F_axis_Single, absFftSig_Sg);


absFftSig_Sg_F=movAver(absFftSig_Sg,15);
figure(3);
rat=10;
plot(F_axis_Single(1:N/rat), absFftSig_Sg_F(1:N/rat));

set(gca,'XTick',0:Fs/100:Fs/2)
return;
