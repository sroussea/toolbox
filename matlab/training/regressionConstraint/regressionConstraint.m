clc;close all;clear all;
% Change default axes fonts.
set(0,'DefaultAxesFontName', 'Times New Roman')
set(0,'DefaultAxesFontSize', 16)

% Change default text fonts.
set(0,'DefaultTextFontname', 'Times New Roman')
set(0,'DefaultTextFontSize', 16)

addpath E:\tmp\GaspardData\matlabRdy\cuts
addpath S:\svn\DSP\common\matlab
addpath S:\svn\DSP\Analysis\matlab\noiseStudy1\res

%  addpath /vol0/rousseau/locData/run_2012_10/cuts
%  addpath /vol0/rousseau/curAn/common/matlab
%  addpath /vol0/rousseau/curAn/matlab/noiseStudy1/res/


% ===========> TO FIT
fileName = 'run_0157.dat.11Oct12_02h51m06s._307905Tks_2560Pts_00_01_300VpCut.bin';
% fileName = 'run_0151.dat.10Oct12_11h29m00s._112020Tks_2560Pts_00_01_275VpCut.bin';
% fileName = 'run_0156.dat.10Oct12_22h28m43s._143964Tks_2560Pts_00_01_250VpCut.bin';

headr=zeros(1,2);
% fid = fopen('S:\svn\DSP\Analysis\matlab\noiseStudy1\res\dspMean300V2.bin','r');
% fid = fopen('S:\svn\DSP\Analysis\matlab\noiseStudy1\res\dspMean300V1.bin','r');
% fid = fopen('S:\svn\DSP\Analysis\matlab\noiseStudy1\res\dspMean275V2.bin','r');
% fid = fopen('S:\svn\DSP\Analysis\matlab\noiseStudy1\res\dspMean275V1.bin','r');
% fid = fopen('S:\svn\DSP\Analysis\matlab\noiseStudy1\res\dspMean250V1.bin','r');
% fid = fopen('S:\svn\DSP\Analysis\matlab\noiseStudy1\res\dspMean250V2.bin','r');

%fid = fopen('dspMean300V1.bin','r');
fid = fopen('dspMean300V2.bin','r');


tic;

head = zeros(1,2);
head = fread(fid, size(head), '*ushort') ;
FaxDspBL = fread(fid, head, '*double') ;

head = fread(fid, size(head), '*ushort') ;
[dspBL ] = fread(fid, head, '*double') ;
head = fread(fid, size(head), '*ushort') ;
[dspTail ] = fread(fid, head, '*double') ;
fclose(fid);


% ===========> TO FIT
%% Perform  selection into (E-m) graph
OneMev300V = round(1/1.4999e-2);
% Read curves from file
[tksIRaw tksQRaw Imax, Qmax ] = readCurves( fileName );

% % SR 31st ofMay : cannot go down 500 since signal must softly fade away
% to 0 to get a relevant FFT
roiT=(290:550);
% roiT=(300:2500);

% remove null tracks
[xxx id] = find( max(abs(tksIRaw(roiT,:))) < 20);
validIds = (1:size(tksIRaw,2));
validIds=validIds(setdiff(1:length(validIds),id));
tksIRaw = tksIRaw(:,validIds);
tksQRaw = tksQRaw(:,validIds);
Imax = Imax(validIds);
Qmax = Qmax(validIds);

%sort tracks by ascendent energy
energSortedIds = sortrows([Qmax',(1:length(Qmax))']);
tksIRaw = tksIRaw(:,energSortedIds(:,2));
tksQRaw = tksQRaw(:,energSortedIds(:,2));
Imax = Imax(energSortedIds(:,2));
Qmax = Qmax(energSortedIds(:,2));


% Create a cut to select a particular energy range
minQmax =  min(Qmax); maxQmax = max(Qmax);
dE = (maxQmax - minQmax )/length(Qmax)*100;
binsQmax = (0 : round((maxQmax-minQmax)/dE));

nBin = 1;
myCut300V_2 = find( Qmax >= minQmax+dE*binsQmax(nBin) & Qmax < minQmax+dE*binsQmax(end));
 %myCut300V_2 = find( Qmax >= minQmax+dE*binsQmax(nBin) & Qmax < minQmax+dE*binsQmax(nBin+1));
myCut = myCut300V_2;
myCut=myCut([1:50]);

E300V = Qmax.*1.4999e-2 -4.96922e-1;

Ecur = E300V(myCut);


% OneMev275V = round(1/1.4e-2);
% QRange275V_1 = [150,150+OneMev275V];
% QRange275V_2 = [469-OneMev275V, 469];
% [ Imax, Qmax, myCut275V_1 ] = biDimCut( fileName, QRange275V_1 );
% [ Imax, Qmax, myCut275V_2 ] = biDimCut( fileName, QRange275V_2 );
% % myCut = myCut275V_2;
% myCut = myCut275V_1;
% E275V = Qmax.*1.4e-2 + 3.0e-2;
% Ecur = E275V;


% OneMev250V = round(1/1.4e-2);
% QRange250V_1 = [154,154+OneMev250V];
% QRange250V_2 = [471-OneMev250V, 471];
% [ Imax, Qmax, myCut250V_1 ] = biDimCut( fileName, QRange250V_1 );
% [ Imax, Qmax, myCut250V_2 ] = biDimCut( fileName, QRange250V_2 );
% E250V = Qmax.*1.4e-2 + 3.0e-2;
% myCut = myCut250V_1;
% myCut = myCut250V_2;
% E250V = Qmax.*1.4e-2 + 3.0e-2;
% Ecur = E250V;

figure; 
h=plot(E300V,Imax,'o','MarkerSize',1);
hold on;
plot(E300V(myCut),Imax(myCut),'ro','MarkerSize',3);
title(['Sélection de ',num2str(size(myCut,2)), ' signaux dans la bande d''énergie [',...
  num2str(min(Ecur),3),'-',num2str(max(Ecur),3),'] Mev']);
ylabel('Amplitude maximum du signal de courant en LSB');xlabel('Energie en Mev');
hold off;


% keep only E selected tracks in memory
tksIRaw = tksIRaw(:,myCut);

% select only region of interest 
Itk(:,:) = tksIRaw(roiT,:);
figure; plot(Itk,'b'); 

% figure;hold on;
ft_ = fittype('poly1');
parfor i = 1:size(Itk,2)

  [maxI maxID] = max( Itk( :, i ) );
  dataSetId = find ( Itk(1:maxID,i) > .1 * maxI & Itk(1:maxID,i) < .5 * maxI );
 
%   plot(dataSetId,Itk(dataSetId,i),'bo');

  [fitRes gof] = fit(dataSetId, Itk(dataSetId,i), ft_);

%  SR 7th of November : dataSetId(1)-10 : 10 to make linear interpolation
%  cross the abscisse axe
  xRangeInter = (dataSetId(1)-10:dataSetId(end) );
  interpolI =fitRes.p1.*xRangeInter +fitRes.p2;
  plot(xRangeInter ,interpolI,'r');
  
  [row iDs] = find(interpolI<0);
  
  startID(i) = xRangeInter(iDs(end));

%% 9th of January 2014: seems to deteriorate the fit quality !!!
%% Has to be understood
%   newStartID = startID(i);
%   if Itk( startID(i) , i ) > 5
%     pulseAtStart = Itk( newStartID ,i );
%     while ( pulseAtStart > 5 )
%       newStartID = newStartID - 1;
%       pulseAtStart = Itk( newStartID ,i );
%     end
%     startID(i) = newStartID;
%   end
  
end
% hold off;



% --------------------------------------------------------------------


% ----------------------------------------------------------------------
% Create a blurred curve from model
% ----------------------------------------------------------------------
tVec = (0:100)';
tauFirst_ = 1.100;
tauSec_ = 10.10;
tau1_ = .4711; tau2_ = -15.5449; 
a_ = 30.4046;b_ = 1.1417;
t2_ = 21.3254;
param0 = [tauFirst_; tauSec_; tau1_; tau2_; t2_; a_; b_];

expTk = myModel3rdOrder( tVec, tauFirst_, tauSec_, tau1_, tau2_,  t2_, a_, b_);
fuzzCurve(:,1) = expTk + 0*randn(length(tVec),1);
fuzzCurve(:,2) = expTk + 1*randn(length(tVec),1);
fuzzCurve(:,3) = expTk + 2*randn(length(tVec),1);

% figure; plot(tVec,fuzzCurve);

% ----------------------------------------------------------------------
% Fit the model over the fuzzy curve
% ----------------------------------------------------------------------
% figure;hold on;

% xc1 = tVec;
roiStart = 0;
xc1 = (roiStart:length(roiT)-1+roiStart)';

% Good results with myModelFunc1Splitted  & myCut300V_1
% start :  [ tauFirst  tauSec tau1   tau2  t1      t2     a       b ]
cellFitInput = {... 
 ['tauFirst    ' 'tauSec    ' 'tau1    ' 'tau2    '  '   t2  ' '     a   ' '     b  '];
 [.1               1           .1        -35            5          1        .3];...
 [.5                12           1        -15           21         37         1];...
 [100               1000          160        -.1          40        1000       300]};


% % Good results with myModelFunc1 & myCut300V_2
% cellFitInput = {... 
% ['tau    ' 'tau1    ' 'tau2    ' '   t1  ' '   t2  ' '     a   ' '     b  '];...
% [  11        1        -15            1         18         250          2 ];...
% [  10       .5        -16            .1        10          1          .01 ];...
% [  23       16        -12.5          20        20         200          2]};
% 
% cellFitInput = {... 
% ['tau    ' 'tau1    ' 'tau2    ' '   t1  ' '   t2  ' '     a   ' '     b  '];...
% [  11        1        -15            1         18         250          2 ];...
% [ 10       .015      -50          5.        5             1         .001];...
% [ 30       16        -10          20        40         300          5]};

% for iFit = 1:size(fuzzCurve,2)
% parfor iFit = 1:size(fuzzCurve,2)
parfor iFit = 1:size(Itk,2)
  
  
  tmp = tksIRaw( roiT,iFit);
%   tksIExp(:,iFit)=[tmp(startID(iFit)+roiStart:end); zeros(startID(iFit)-1+roiStart,1 ) ];  
  ttmp=[tmp(startID(iFit)+roiStart-6:end); zeros(startID(iFit)-1+roiStart-6,1 ) ];  
  tksIExp(:,iFit) = ttmp(1:length(xc1));

%   tksIExp(:,iFit) = fuzzCurve(:,iFit);
  
%   plot(xc1,tksIExp(:,iFit),'r');

% dataW = ones(length(tksIExp(:,iFit)),1);
% dataW(3:11) = 100;
fo_ = fitoptions('method','NonlinearLeastSquares',...
  'Lower' ,    cellFitInput{2,:},...
  'Upper',     cellFitInput{4,:},...
  'StartPoint',cellFitInput{3,:});
% ...
%   , 'Weights',dataW);



% ft_ = fittype('myModelFunc1Splitted( x, tau, tau1, tau2, t1, t2, a)',...
%     'dependent',{'y'},'independent',{'x'},...
%     'coefficients',{'tau', 'tau1', 'tau2', 't1', 't2', 'a'});

ft_ = fittype('myModel3rdOrder( x, tauFirst, tauSec, tau1, tau2, t2, a, b)',...
    'dependent',{'y'},'independent',{'x'},...
    'coefficients',{'tauFirst', 'tauSec',  'tau1', 'tau2', 't2', 'a', 'b'});

% ft_ = fittype('myModelFunc1( x, K, tau, tau1, tau2, t1, t2, a, b)',...
%     'dependent',{'y'},'independent',{'x'},...
%     'coefficients',{'K', 'tau', 'tau1', 'tau2', 't1', 't2', 'a', 'b'});
  
  % Fit this model using new data

  %[cf_ gof_]= nlinfit(xc1,tksIExp(:,iFit) ,ft_,fo_);
 [cf_ gof_]= fit(xc1,tksIExp(:,iFit) ,ft_,fo_);
  
  theoC(:,iFit) = zeros( length(xc1), 1);
   
  rSq(iFit)  = double(gof_(1).rsquare);
%  K(iFit)  = cf_.K;
  tauFirst(iFit)  = cf_.tauFirst;
  tauSec(iFit)  = cf_.tauSec;
  tau1(iFit) = cf_.tau1;
  tau2(iFit) = cf_.tau2;
  t2(iFit)   = cf_.t2;
  a(iFit)    = cf_.a;
  b(iFit)    = cf_.b;
   
  paramFit(:,iFit) = [tauFirst(iFit); tauSec(iFit); tau1(iFit); tau2(iFit); t2(iFit); a(iFit); b(iFit)];
                           
  theoC(:,iFit) = myModel3rdOrder( xc1, tauFirst(iFit), tauSec(iFit), tau1(iFit), tau2(iFit),...
                                  t2(iFit),  a(iFit), b(iFit) );
  
%   [paramLsq(:,iFit),resnorm] = lsqcurvefit(@myModel3rdOrderParam, cellFitInput{3,:},xc1,tksIExp(:,iFit));
%   theoCLsq(:,iFit) = myModel3rdOrder( xc1, paramLsq(1,iFit), paramLsq(2,iFit), paramLsq(3,iFit), paramLsq(4,iFit),...
%                                       paramLsq(5,iFit),paramLsq(6,iFit),paramLsq(7,iFit) );

  
  %   plot(xc1,theoC(:,iFit),'b');  
 
  
end

%b = a./3;

% title('theoC vs exp curves');hold off;
figTheoVsExp = figure('name','curves theoC vs Exp','NumberTitle','off'); 
 plot(xc1,tksIExp,'r');hold on;
 plot(xc1,theoC,'b');hold off;
title('Modele Vs mesures bande basse d''energie � 300V');
ylabel('Amplitude en LSB');xlabel('temps en ns');


figTau2 = figure('name','tau2','NumberTitle','off');hist(tau2,100);title('tau2') 
figTau1 = figure('name','tau1','NumberTitle','off');hist(tau1,100);title('tau1') 
figTauFirst = figure('name','tauFirst','NumberTitle','off');hist(tauFirst,100);title('tauFirst') 
figTauSec = figure('name','tauSec','NumberTitle','off');hist(tauSec,100);title('tauSec') 
figA = figure('name','a','NumberTitle','off');hist(a,100);title('a') 
figB = figure('name','b','NumberTitle','off');hist(b,100);title('b') 
% figT1 = figure('name','t1','NumberTitle','off');hist(t1,100);title('t1') 
figT2 = figure('name','t2','NumberTitle','off');hist(t2,100);title('t2') 

figMosaic1 = figs2subplots([figTau1 figA figTau2 figT2 figB ],[2 3] )
set(figMosaic1 ,'name','Mosaic 1 : Fit parameters','NumberTitle','off');

figRsq =figure('name','Determination Coeff','NumberTitle','off');
[rSqBins rSqPop] = hist(rSq,100);xlim([0.99 1]);
title({['Coefficients de d�termination des r�gressions des ',...
  num2str(length(myCut))],' signaux de la bande haute d''�nergie'});
ylabel('Coefficients de d�termination');xlabel('nombre de signaux');


difTheoExp = theoC(:,:)- tksIExp(:,:) ;

meandifTheoExp = mean(difTheoExp,1 );
stdDifTheoExp  = std(difTheoExp,1 );
% figure;
% plot(meandifTheoExp  );title('meandifTheoExp ');
% figure;
% plot(vardifTheoExp  );title('vardifTheoExp  ');

% 300V_2 .9993 =>  71 tks
% [vals ids ]=find(rSq >.9993 )
% 300V_1 .995 =>  99 tks
% [sortRSq iDsRSq]= sort(rSq,'descend')
% propOfBest = 1;
% ids = iDsRSq(1:round(propOfBest*length(iDsRSq)));
ids= (1:length(rSq));
% 275V_2 .999 => 99 tks
% [vals ids ]=find(rSq >.999 )
% 275V_1 .975 =>   99 tks
% [vals ids ]=find(rSq >.975 );
% 250V_1 .979 =>   61 tks
% [vals ids ]=find(rSq >=.979 );
% 250V_2 .97 =>  all tks => 266
% 250V_2 .999 =>  100 tks
% 250V_2 .9991 =>  66 tks
% [vals ids ]=find(rSq >.9991 );

% %SR30th of May,  To check if residus vary slowly => true
% difTheoExp =  movAver( tksIExp(:,:),5) - movAver(  theoC(:,:),5);

% difTheoExpMeanOff = bsxfun(@minus, difTheoExp, mean(difTheoExp,1)); 

N = size(difTheoExp,1);
Fs = 1e9;
Ts=1/Fs;
F_axis = (0:N)*Fs/N;
F_axis_Sg = (0:N/2)*Fs/N;


figDifTheoExp = figure('name','Diff Theo-Exp','NumberTitle','off');
% roiT=(1:N-50);
roiT=(1:N);
plot(roiT,difTheoExp(roiT,ids) ); 
% title('Différence entre modèle et mesures bande haute d''énergie à 300V');
title('Diff�rence entre mod�le et mesures bande basse d''�nergie � 300V');
ylabel('Amplitude en LSB');xlabel('temps en ns');
xlim([1 roiT(end)]);

clear fft_difTheoExp;clear absFFTdifTheoExp_Sg;

fft_difTheoExp = fft(difTheoExp(:,ids));
% fft_difTheoExp = fft(difTheoExp);
absFFTdifTheoExp = abs(fft_difTheoExp);    % FFT Module in V 
absFFTdifTheoExp_Sg(1:round(N/2)+1,:) = absFFTdifTheoExp(1:round(N/2)+1,:);
absFFTdifTheoExp_Sg(2:end,:) = 2*absFFTdifTheoExp_Sg(2:end,:);
dspDifTheoExp = (.5*absFFTdifTheoExp_Sg.^2)/N;

dspMeanResi = mean(dspDifTheoExp,2); 
avPowDifTheoExp = sum(dspDifTheoExp,1)/N;

% % SR to compute avPow from DSP over a bandwidth
% sum(dspMeanResi(max(find(F_axis_Sg/1e6 < 300)):size(F_axis_Sg,2)))/size(difTheoExp,1)
% sum(1.e9*dspBL(max(find(FaxDspBL/1e6 < 1)):length(FaxDspBL)))/length(FaxDspBL)/2

parfor iF = 1:length(F_axis_Sg)
  
  curF = F_axis_Sg(iF)/1e6;
  if  iF == 1
    curF = 1;
  end
  
  avPowRes = sum(dspMeanResi(max(find(F_axis_Sg/1e6 < curF)):size(F_axis_Sg,2)))/size(difTheoExp,1);
  avPowTail = sum(1.e9*dspTail(max(find(FaxDspBL/1e6 < curF)):length(FaxDspBL)))/length(FaxDspBL)/2;
  avPowDif(iF) = abs(avPowRes-avPowTail);
end

fRoi=(max(find(F_axis_Sg/1e6 <60)):length(F_axis_Sg));
% fRoi=(1:min(find(F_axis_Sg/1e6 >60)));
% fRoi = (1:length(F_axis_Sg));
figDifDspResidualsBL = figure('name','Diff residuals-baseline average noise power ','NumberTitle','off');
plot(F_axis_Sg(fRoi)/1e6,avPowDif(fRoi))
title({'Diff�rence entre puissance moyenne du bruit calcul�e apr�s ' ...
  'l''impulsion et puissance moyenne du bruit calcul�e � partir des r�sidus '...
  '(en valeur absolue)'});
ylabel('LSB  ^2');xlabel('Fr�quence in MHz');
% xlim([F_axis_Sg(fRoi(1))/1.e6 60]);
xlim([60 F_axis_Sg(fRoi(end))/1.e6 ]);

figDSP=figure('name','DSP','NumberTitle','off');
fMin = 1;
roiFPulse = (max(find(F_axis_Sg/1e6 < fMin)):size(F_axis_Sg,2));

roiBL=( max(find(FaxDspBL/1e6 < fMin)):length(FaxDspBL));
pDsp(:,1)=plot(FaxDspBL(roiBL)/1e6,dspBL(roiBL),'r','LineWidth',1);hold on;
pDsp(:,2)=plot(FaxDspBL(roiBL)/1e6,dspTail(roiBL),'g','LineWidth',1);
pDsp(:,3)=plot(F_axis_Sg(roiFPulse)/1e6,mean(dspDifTheoExp(roiFPulse,:)/Fs,2),'b');
title(['Ensemble des densit�s spectrales de puissance']);
ylabel('DSP en LSB  ^2/Hz');xlabel('Fr�quence in MHz');

% 275V_1
% ylim([0 0.6e-7]);xlim([fMin 500]);
% 275V_2
ylim([0 0.9e-7]);xlim([fMin 500]);

hold off;
legend(pDsp(1,:),{['DSP de l''avant-impulsion'],...
['DSP de l''apr�s-impulsion'],...
['Moyenne des DSP de ',num2str(size(dspDifTheoExp,2)),' r�sidus']});
% text(50, 1.6e-7, '(Calcul étendu à 2560 points par bourrage de zéros)')


% %  SR 30th  of May : apply a derivative filter over the difTheoExp
% %  to suppress low freq 

% derDifTheoExp = filter([-1 +1],1,difTheoExp);
% fft_difTheoExp = fft(derDifTheoExp);
% absFFTdifTheoExp = abs(fft_difTheoExp)/N;    % FFT Module in V 
% absFFTdifTheoExp_Sg(1:N/2+1,:) = absFFTdifTheoExp(1:N/2+1,:);
% absFFTdifTheoExp_Sg(2:end,:) = 2*absFFTdifTheoExp_Sg(2:end,:);
% dspDifTheoExp = .5*absFFTdifTheoExp_Sg.^2;
% avPowDifTheoExp = sum(dspDifTheoExp,1)
% 
% figure;
% roiF = (40:size(F_axis_Sg,2));
% plot(F_axis_Sg(roiF),mean(absFFTdifTheoExp_Sg(roiF,:),2),'b')
% title('mean(derDifTheoExp)');

figMosaic2 = figs2subplots([figTauFirst figTauSec figTheoVsExp figDSP figDifDspResidualsBL figRsq figDifTheoExp ],[2 3] )
set(figMosaic2 , 'name','Mosaic 2 : DSP, Détermination...','NumberTitle','off');

 
% figT1vsE=figure('name','t1 vs E','NumberTitle','off'); plot(Ecur,t1,'bo');title('t1 vs E');
figT2vsE=figure('name','t2 vs E','NumberTitle','off'); plot(Ecur,t2,'bo');title('t2 vs E');
figTauFirstVsE=figure('name','tauFirst vs E','NumberTitle','off'); plot(Ecur,tauFirst,'bo');title('tauFirst vs E');
figTauSecVsE=figure('name','tauSec vs E','NumberTitle','off'); plot(Ecur,tauSec,'bo');title('tauSec vs E');
figTau1VsE=figure('name','tau1 vs E','NumberTitle','off'); plot(Ecur,tau1,'bo');title('tau1 vs E');
figTau2VsE=figure('name','tau2 vs E','NumberTitle','off'); plot(Ecur,tau2,'bo');title('tau2 vs E');
figAvsE=figure('name','a vs E','NumberTitle','off'); plot(Ecur,a,'bo');title('a vs E');
figBvsE=figure('name','b vs E','NumberTitle','off'); plot(Ecur,b,'bo');title('Amplitude du courant d''ions à t=0 vs E');
ylabel('Amplitude du courant d''ions à t=0');xlabel('Energie en MeV');

figR2vsE=figure('name','R² vs E','NumberTitle','off'); plot(Ecur,rSq,'bo');title('R² vs E');
ylabel('Coefficients de détermination R²');xlabel('Energie en MeV');

figMosaic3 = figs2subplots([ figAvsE figT2vsE figBvsE],[2 2] );
set(figMosaic3 , 'name','Mosaic 3 : t1,t2,a,b vs E','NumberTitle','off');
figMosaic4 = figs2subplots([figTauSecVsE figTauFirstVsE figTau1VsE figTau2VsE figR2vsE],[2 3] );
set(figMosaic4 , 'name','Mosaic 4 : tauSec,tauFirst , tau1,tau2,R² vs E','NumberTitle','off');

cellFig = {figMosaic1, figMosaic2, figMosaic3, figMosaic4};
writeResToFile( tauFirst, tauSec, tau1, tau2, t2, a, b,...
    stdDifTheoExp, rSq, cellFitInput, cellFig);

ovrHead=toc

return;
  f3=figure;hold on;
for iTrack = 1:size( difTheoExp,2)
  
  h = spectrum.periodogram('Blackman-Harris');
%   h = spectrum.periodogram;
  hopts = psdopts(h)
  set(hopts,'Fs',Fs,'SpectrumType','onesided');
  myPsd = psd(h,difTheoExp(:,iTrack),hopts)
  avgPowPerio_f(iTrack) = avgpower(myPsd)
%   plot(myPsd.Frequencies(40:end),10.^(myPsd.data(40:end)/10))
%   plot(myPsd.Frequencies(40:end),10*log10(myPsd.data(40:end)))
  myPsdData(:,iTrack) = myPsd.data;

end
hold off;

QmaxTheo = 2*a./b.^3;
ImaxTheo = a.*((2./(b*exp(1)).^2));
%  figure(7);plot(2*a./b.^3,a.*((2./(b*exp(1)).^2)),'bo');
figure(8); plot(QmaxTheo/ mean(QmaxTheo./Qmax),ImaxTheo/mean(ImaxTheo./Imax),'ro',Qmax,Imax,'bo')

figure(9);
 m2=moment(Isync,2);
 plot(Qmax,m2,'ro');
 
 
