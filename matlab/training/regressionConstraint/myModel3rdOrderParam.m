% second order low pass transfer function  * 
% sum of rising exponential + decreasing exponential
% h(s) = 1/((tau*s)^2 + tau*s/Q + 1)
% with Q = qualtiy factor

function [y] = myModel3rdOrderParam( param, tt );

tauFirst = param(1);
tauSec   = param(2);
tau1     = param(3);
tau2     = param(4);
t2       = param(5);
a        = param(6);
b        = param(7);

Q=.48;
K = 100;
%b=a/3;

t=tt;
r=1+(-4).*Q.^2;

y= K.*...
a.*tau1.*(exp(1).^((-1).*t.*tau1.^(-1)).*Q.*tau1.^2.*(tau1+(-1).*...
  tauFirst).^(-1).*((-1).*tau1.*tauSec+Q.*(tau1.^2+tauSec.^2)).^(-1)...
  +exp(1).^((-1).*t.*tauFirst.^(-1)).*Q.*tauFirst.^2.*((-1).*tau1+...
  tauFirst).^(-1).*((-1).*tauFirst.*tauSec+Q.*(tauFirst.^2+ ...
  tauSec.^2)).^(-1)+(1/2).*exp(1).^((-1/2).*Q.^(-1).*(1+r.^(1/2)).*...
  t.*tauSec.^(-1)).*Q.*r.^(-1/2).*tauSec.*((-1).*tau1.*tauSec+Q.*(...
  tau1.^2+tauSec.^2)).^(-1).*((-1).*tauFirst.*tauSec+Q.*( ...
  tauFirst.^2+tauSec.^2)).^(-1).*((-1).*tau1.*tauFirst+2.*Q.^2.* ...
  tau1.*tauFirst+r.^(1/2).*tau1.*tauFirst+Q.*tau1.*tauSec+(-1).*Q.*...
  r.^(1/2).*tau1.*tauSec+Q.*tauFirst.*tauSec+(-1).*Q.*r.^(1/2).* ...
  tauFirst.*tauSec+(-2).*Q.^2.*tauSec.^2+(-1).*exp(1).^(Q.^(-1).*...
  r.^(1/2).*t.*tauSec.^(-1)).*(((-1)+2.*Q.^2+(-1).*r.^(1/2)).*tau1.*...
  tauFirst+Q.*(1+r.^(1/2)).*(tau1+tauFirst).*tauSec+(-2).*Q.^2.* ...
  tauSec.^2)))+b.*tau2.*(exp(1).^((-1).*t.*tau2.^(-1)).*Q.*tau2.^2.*...
  (tau2+(-1).*tauFirst).^(-1).*((-1).*tau2.*tauSec+Q.*(tau2.^2+ ...
  tauSec.^2)).^(-1)+exp(1).^((-1).*t.*tauFirst.^(-1)).*Q.* ...
  tauFirst.^2.*((-1).*tau2+tauFirst).^(-1).*((-1).*tauFirst.*tauSec+...
  Q.*(tauFirst.^2+tauSec.^2)).^(-1)+(1/2).*exp(1).^((-1/2).*Q.^(-1)...
  .*(1+r.^(1/2)).*t.*tauSec.^(-1)).*Q.*r.^(-1/2).*tauSec.*((-1).*...
  tau2.*tauSec+Q.*(tau2.^2+tauSec.^2)).^(-1).*((-1).*tauFirst.* ...
  tauSec+Q.*(tauFirst.^2+tauSec.^2)).^(-1).*((-1).*tau2.*tauFirst+...
  2.*Q.^2.*tau2.*tauFirst+r.^(1/2).*tau2.*tauFirst+Q.*tau2.*tauSec+(...
  -1).*Q.*r.^(1/2).*tau2.*tauSec+Q.*tauFirst.*tauSec+(-1).*Q.*r.^(...
  1/2).*tauFirst.*tauSec+(-2).*Q.^2.*tauSec.^2+(-1).*exp(1).^(Q.^(...
  -1).*r.^(1/2).*t.*tauSec.^(-1)).*(((-1)+2.*Q.^2+(-1).*r.^(1/2)).*...
  tau2.*tauFirst+Q.*(1+r.^(1/2)).*(tau2+tauFirst).*tauSec+(-2).* ...
  Q.^2.*tauSec.^2)))+(-1).*b.*exp(1).^((-1).*t2.*tau2.^(-1)).*tau2.*...
  (exp(1).^(((-1).*t+t2).*tau2.^(-1)).*Q.*tau2.^2.*(tau2+(-1).* ...
  tauFirst).^(-1).*((-1).*tau2.*tauSec+Q.*(tau2.^2+tauSec.^2)).^(-1)...
  +exp(1).^(((-1).*t+t2).*tauFirst.^(-1)).*Q.*tauFirst.^2.*((-1).*...
  tau2+tauFirst).^(-1).*((-1).*tauFirst.*tauSec+Q.*(tauFirst.^2+ ...
  tauSec.^2)).^(-1)+(1/2).*exp(1).^((-1/2).*Q.^(-1).*(1+r.^(1/2)).*(...
  t+(-1).*t2).*tauSec.^(-1)).*Q.*r.^(-1/2).*tauSec.*((-1).*tau2.*...
  tauSec+Q.*(tau2.^2+tauSec.^2)).^(-1).*((-1).*tauFirst.*tauSec+Q.*(...
  tauFirst.^2+tauSec.^2)).^(-1).*((-1).*tau2.*tauFirst+2.*Q.^2.* ...
  tau2.*tauFirst+r.^(1/2).*tau2.*tauFirst+Q.*tau2.*tauSec+(-1).*Q.*...
  r.^(1/2).*tau2.*tauSec+Q.*tauFirst.*tauSec+(-1).*Q.*r.^(1/2).* ...
  tauFirst.*tauSec+(-2).*Q.^2.*tauSec.^2+(-1).*exp(1).^(Q.^(-1).*...
  r.^(1/2).*(t+(-1).*t2).*tauSec.^(-1)).*(((-1)+2.*Q.^2+(-1).*r.^(...
  1/2)).*tau2.*tauFirst+Q.*(1+r.^(1/2)).*(tau2+tauFirst).*tauSec+(...
  -2).*Q.^2.*tauSec.^2))).*heaviside(t+(-1).*t2);
  
  vecOfNan = isnan(y);
idsOfNan = find(vecOfNan>0);
if ~isempty(idsOfNan) 
%   disp('NaN  in my model, parameters values are',tau, tau1, tau2, t1, t2, a, b);
if ( idsOfNan(1)==1 )
  y(idsOfNan) = ones(length(idsOfNan),1);
else
  y(idsOfNan) = ones(length(idsOfNan),1)*y(idsOfNan(1)-1);
end
end

vecOfInf = isinf(y);
idsOfInf = find(vecOfInf>0);
if ~isempty(idsOfInf) 
if ( idsOfInf(1)==1  )
  y(idsOfInf) = ones(length(idsOfInf),1);
else
%   disp('Inf in my model, parameters values are',tau, tau1, tau2, t1, t2, a, b);
  y(idsOfInf) = ones(length(idsOfInf),1)*y(idsOfInf(1)-1);
end
end

end