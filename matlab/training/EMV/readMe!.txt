nonLinearRegEmv4thOrder.m

 The expressions generated for Ai involve big number differences and
 ratios which produce some big numerical error propagation.
 As a consequence, we obtain the Ai with a large variance.
nonLinearRegEmv.m Treats the second order case. If we treats this case
with the 4th order formulas, we obtain a parameters variance ten times larger !


Eventually, I noticed that deriving the EMVs of a model parameters in a gaussain noise is strictly equivalent to perform a least square optimisation !
The parameters of the noise (mu, sigma) aren't taken into account.

