%% SR : 16th of October 2015
% 




clear all;close all;clc;

% Sample size
N = 100;
%  Gaussian distribution 
sig = 1;
% Monte Carlo size
monteSize = 5000;
% Neyman Pearson parameters 
% test level
alfa = 0.05;

% linear model : y = ax
a = 1;

x = linspace(0, 5, N)';
yTheo = a*x;

gNoise = sig*randn( N, monteSize );
yExp = repmat(yTheo, 1, monteSize) + gNoise;

% Estimate a with EMV
aChap = sum(repmat(x, 1, monteSize).*yExp)/sum(x.*x)

figure('name','Exp data','NumberTitle','off'); 
plot(x,yExp(:,1),'bo', x,yTheo, 'r');

figure('name','a parameter distribution','NumberTitle','off'); 
hist(aChap,10)

std(aChap)

return;



return;

figure('name',figTitManuErrTypeRate,'NumberTitle','off'); 
hist(manuErrRate,10)
% ;hold on;
% hist(xPop2,30);hold off;
title(figTitManuErrTypeRate); xlabel(manuErrType);
h = findobj(gca,'Type','patch');
set(h(1),'FaceColor','r','EdgeColor','w','FaceAlpha', 0.5);
% set(h(2),'FaceColor','g','EdgeColor','w' );
% legend('population 1','population 2');


figure('name',figTitDealErrTypeRate,'NumberTitle','off'); 
hist(dealErrRate,10)
title(figTitDealErrTypeRate); xlabel(dealErrType);
h = findobj(gca,'Type','patch');
set(h(1),'FaceColor','r','EdgeColor','w','FaceAlpha', 0.5);

figTitle = ['Dealer  / manufacturer agreement rates for a test size of ' num2str(alfa)];
figure('name',figTitle,'NumberTitle','off'); 
hist(AgreementRates,10)
title(figTitle); xlabel('Dealer  / manufacturer agreement rates');
h = findobj(gca,'Type','patch');
set(h(1),'FaceColor','r','EdgeColor','w','FaceAlpha', 0.5);

