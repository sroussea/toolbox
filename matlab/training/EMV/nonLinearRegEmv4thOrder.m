%% SR : 27th of October 2015
% 
%  The expressions generated for Ai involve big number differences and
%  ratios which produce some big numerical error propagation.
%  As a consequence, we obtain the Ai with a large variance.
% nonLinearRegEmv.m Treats the second order case. If we treats this case
% with the 4th order formulas, we obtain a parameters variance ten times larger !




clear all;close all;clc;

% Sample size
N = 100;
%  Gaussian distribution 
sig = 1;
% Monte Carlo size
monteSize = 1000;
% Neyman Pearson parameters 
% test level
alfa = 0.05;

% non linear model : y = a4.x^4 +a3.x^3 +a2.x^2 + a1.x 
a1 = 1;
a2 = 1;
a3 = .1;
a4 = .3;

x = linspace(.1, 5, N)';
yTheo = a4*x.^4 + a3*x.^3 + a2*x.^2 + a1*x ;

gNoise = sig*randn( N, monteSize );
yExp = repmat(yTheo, 1, monteSize) + gNoise;

% Estimate a with EMV
xnMat  = @(x, n) repmat(x.^n, 1, monteSize);

% aChap expression
% Sn = sum(x^n)
S1 = sum(xnMat(x,1));
S2 = sum(xnMat(x,2));
S3 = sum(xnMat(x,3));
S4 = sum(xnMat(x,4));
S5 = sum(xnMat(x,5));
S6 = sum(xnMat(x,6));
S7 = sum(xnMat(x,7));
S8 = sum(xnMat(x,8));

YX1 = sum( xnMat(x,1).*yExp );
YX2 = sum( xnMat(x,2).*yExp );
YX3 = sum( xnMat(x,3).*yExp );
YX4 = sum( xnMat(x,4).*yExp );

A1chap = ...
((-1).*S5.^4+3.*S4.*S5.^2.*S6+(-1).*S4.^2.*S6.^2+(-2).*S3.*S5.* ...
S6.^2+S2.*S6.^3+(-2).*S4.^2.*S5.*S7+2.*S3.*S5.^2.*S7+2.*S3.*S4.* ...
S6.*S7+(-2).*S2.*S5.*S6.*S7+(-1).*S3.^2.*S7.^2+S2.*S4.*S7.^2+ ...
S4.^3.*S8+(-2).*S3.*S4.*S5.*S8+S2.*S5.^2.*S8+S3.^2.*S6.*S8+(-1).* ...
S2.*S4.*S6.*S8).^(-1).*(S6.^3.*YX1+(-2).*S5.*S6.*S7.*YX1+S4.* ...
S7.^2.*YX1+S5.^2.*S8.*YX1+(-1).*S4.*S6.*S8.*YX1+(-1).*S5.*S6.^2.* ...
YX2+S5.^2.*S7.*YX2+S4.*S6.*S7.*YX2+(-1).*S3.*S7.^2.*YX2+(-1).*S4.* ...
S5.*S8.*YX2+S3.*S6.*S8.*YX2+S5.^2.*S6.*YX3+(-1).*S4.*S6.^2.*YX3+( ...
-1).*S4.*S5.*S7.*YX3+S3.*S6.*S7.*YX3+S4.^2.*S8.*YX3+(-1).*S3.*S5.* ...
S8.*YX3+(-1).*S5.^3.*YX4+2.*S4.*S5.*S6.*YX4+(-1).*S3.*S6.^2.*YX4+( ...
-1).*S4.^2.*S7.*YX4+S3.*S5.*S7.*YX4);

A2chap = ...
((-1).*S5.^4+3.*S4.*S5.^2.*S6+(-1).*S4.^2.*S6.^2+(-2).*S3.*S5.* ...
S6.^2+S2.*S6.^3+(-2).*S4.^2.*S5.*S7+2.*S3.*S5.^2.*S7+2.*S3.*S4.* ...
S6.*S7+(-2).*S2.*S5.*S6.*S7+(-1).*S3.^2.*S7.^2+S2.*S4.*S7.^2+ ...
S4.^3.*S8+(-2).*S3.*S4.*S5.*S8+S2.*S5.^2.*S8+S3.^2.*S6.*S8+(-1).* ...
S2.*S4.*S6.*S8).^(-1).*((-1).*S5.*S6.^2.*YX1+S5.^2.*S7.*YX1+S4.* ...
S6.*S7.*YX1+(-1).*S3.*S7.^2.*YX1+(-1).*S4.*S5.*S8.*YX1+S3.*S6.* ...
S8.*YX1+S5.^2.*S6.*YX2+(-2).*S4.*S5.*S7.*YX2+S2.*S7.^2.*YX2+ ...
S4.^2.*S8.*YX2+(-1).*S2.*S6.*S8.*YX2+(-1).*S5.^3.*YX3+S4.*S5.*S6.* ...
YX3+S3.*S5.*S7.*YX3+(-1).*S2.*S6.*S7.*YX3+(-1).*S3.*S4.*S8.*YX3+ ...
S2.*S5.*S8.*YX3+S4.*S5.^2.*YX4+(-1).*S4.^2.*S6.*YX4+(-1).*S3.*S5.* ...
S6.*YX4+S2.*S6.^2.*YX4+S3.*S4.*S7.*YX4+(-1).*S2.*S5.*S7.*YX4);
  
A3chap = ...
((-1).*S5.^4+3.*S4.*S5.^2.*S6+(-1).*S4.^2.*S6.^2+(-2).*S3.*S5.* ...
S6.^2+S2.*S6.^3+(-2).*S4.^2.*S5.*S7+2.*S3.*S5.^2.*S7+2.*S3.*S4.* ...
S6.*S7+(-2).*S2.*S5.*S6.*S7+(-1).*S3.^2.*S7.^2+S2.*S4.*S7.^2+ ...
S4.^3.*S8+(-2).*S3.*S4.*S5.*S8+S2.*S5.^2.*S8+S3.^2.*S6.*S8+(-1).* ...
S2.*S4.*S6.*S8).^(-1).*(S5.^2.*S6.*YX1+(-1).*S4.*S6.^2.*YX1+(-1).* ...
S4.*S5.*S7.*YX1+S3.*S6.*S7.*YX1+S4.^2.*S8.*YX1+(-1).*S3.*S5.*S8.* ...
YX1+(-1).*S5.^3.*YX2+S4.*S5.*S6.*YX2+S3.*S5.*S7.*YX2+(-1).*S2.* ...
S6.*S7.*YX2+(-1).*S3.*S4.*S8.*YX2+S2.*S5.*S8.*YX2+S4.*S5.^2.*YX3+( ...
-2).*S3.*S5.*S6.*YX3+S2.*S6.^2.*YX3+S3.^2.*S8.*YX3+(-1).*S2.*S4.* ...
S8.*YX3+(-1).*S4.^2.*S5.*YX4+S3.*S5.^2.*YX4+S3.*S4.*S6.*YX4+(-1).* ...
S2.*S5.*S6.*YX4+(-1).*S3.^2.*S7.*YX4+S2.*S4.*S7.*YX4);

A4chap = ...
((-1).*S5.^4+3.*S4.*S5.^2.*S6+(-1).*S4.^2.*S6.^2+(-2).*S3.*S5.* ...
S6.*S7+(-2).*S2.*S5.*S6.*S7+(-1).*S3.^2.*S7.^2+S2.*S4.*S7.^2+ ...
S4.^3.*S8+(-2).*S3.*S4.*S5.*S8+S2.*S5.^2.*S8+S3.^2.*S6.*S8+(-1).* ...
S2.*S4.*S6.*S8).^(-1).*((-1).*S5.^3.*YX1+2.*S4.*S5.*S6.*YX1+(-1).* ...
S3.*S6.^2.*YX1+(-1).*S4.^2.*S7.*YX1+S3.*S5.*S7.*YX1+S4.*S5.^2.* ...
YX2+(-1).*S4.^2.*S6.*YX2+(-1).*S3.*S5.*S6.*YX2+S2.*S6.^2.*YX2+S3.* ...
S4.*S7.*YX2+(-1).*S2.*S5.*S7.*YX2+(-1).*S4.^2.*S5.*YX3+S3.*S5.^2.* ...
YX3+S3.*S4.*S6.*YX3+(-1).*S2.*S5.*S6.*YX3+(-1).*S3.^2.*S7.*YX3+ ...
S2.*S4.*S7.*YX3+S4.^3.*YX4+(-2).*S3.*S4.*S5.*YX4+S2.*S5.^2.*YX4+ ...
S3.^2.*S6.*YX4+(-1).*S2.*S4.*S6.*YX4);

figure('name','Exp data','NumberTitle','off'); 
plot(x,yExp(:,1),'bo', x, yTheo, 'r');

figure('name','a1 parameter distribution','NumberTitle','off'); 
hist(A1chap,10)
figure('name','a2 parameter distribution','NumberTitle','off'); 
hist(A2chap,10)

std(A1chap)
std(A2chap)
std(A3chap)
std(A4chap)
mean(A1chap)
mean(A2chap)
mean(A3chap)
mean(A4chap)

return;



return;

figure('name',figTitManuErrTypeRate,'NumberTitle','off'); 
hist(manuErrRate,10)
% ;hold on;
% hist(xPop2,30);hold off;
title(figTitManuErrTypeRate); xlabel(manuErrType);
h = findobj(gca,'Type','patch');
set(h(1),'FaceColor','r','EdgeColor','w','FaceAlpha', 0.5);
% set(h(2),'FaceColor','g','EdgeColor','w' );
% legend('population 1','population 2');


figure('name',figTitDealErrTypeRate,'NumberTitle','off'); 
hist(dealErrRate,10)
title(figTitDealErrTypeRate); xlabel(dealErrType);
h = findobj(gca,'Type','patch');
set(h(1),'FaceColor','r','EdgeColor','w','FaceAlpha', 0.5);

figTitle = ['Dealer  / manufacturer agreement rates for a test size of ' num2str(alfa)];
figure('name',figTitle,'NumberTitle','off'); 
hist(AgreementRates,10)
title(figTitle); xlabel('Dealer  / manufacturer agreement rates');
h = findobj(gca,'Type','patch');
set(h(1),'FaceColor','r','EdgeColor','w','FaceAlpha', 0.5);

