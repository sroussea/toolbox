%% SR : 26th of October 2015
% 




clear all;close all;clc;

% Sample size
N = 100;
%  Gaussian distribution 
sig = 1;
% Monte Carlo size
monteSize = 1000;
% Neyman Pearson parameters 
% test level
alfa = 0.05;

% non linear model : y = ax^2 +bx
a = 1;
b = 1;

x = linspace(0.1, 5, N)';
x2 = x.^2;
x3 = x.^3;
x4 = x.^4;
yTheo = a*x2 + b*x;

gNoise = sig*randn( N, monteSize );
yExp = repmat(yTheo, 1, monteSize) + gNoise;

% Estimate a with EMV
xMat  = repmat(x, 1, monteSize);
x2Mat = repmat(x2, 1, monteSize);
x3Mat = repmat(x3, 1, monteSize);
x4Mat = repmat(x4, 1, monteSize);

% aChap expression
% Sn = sum(x^n)
S2 = sum(x2Mat);
S3 = sum(x3Mat);
S4 = sum(x4Mat);
% sum1 = sum( yx(S2x-S3) )
sum1 = S2.*sum( x2Mat.*yExp) -S3.*sum (xMat.*yExp);

% aChap 
aChap = sum1./(S4.*S2 - S3.^2);

figure('name','Exp data','NumberTitle','off'); 
plot(x,yExp(:,1),'bo', x,yTheo, 'r');

figure('name','a parameter distribution','NumberTitle','off'); 
hist(aChap,10)

std(aChap)

return;



return;

figure('name',figTitManuErrTypeRate,'NumberTitle','off'); 
hist(manuErrRate,10)
% ;hold on;
% hist(xPop2,30);hold off;
title(figTitManuErrTypeRate); xlabel(manuErrType);
h = findobj(gca,'Type','patch');
set(h(1),'FaceColor','r','EdgeColor','w','FaceAlpha', 0.5);
% set(h(2),'FaceColor','g','EdgeColor','w' );
% legend('population 1','population 2');


figure('name',figTitDealErrTypeRate,'NumberTitle','off'); 
hist(dealErrRate,10)
title(figTitDealErrTypeRate); xlabel(dealErrType);
h = findobj(gca,'Type','patch');
set(h(1),'FaceColor','r','EdgeColor','w','FaceAlpha', 0.5);

figTitle = ['Dealer  / manufacturer agreement rates for a test size of ' num2str(alfa)];
figure('name',figTitle,'NumberTitle','off'); 
hist(AgreementRates,10)
title(figTitle); xlabel('Dealer  / manufacturer agreement rates');
h = findobj(gca,'Type','patch');
set(h(1),'FaceColor','r','EdgeColor','w','FaceAlpha', 0.5);

