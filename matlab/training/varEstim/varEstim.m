% SR 29th of April 2014 : when reducing the variable x, it introduce a
% factor different for each relalization of the x sample : this is the 
% empirical variance of the sample.
% 
% Moreover he variance of mean(xRed) seems to depend on muX !

clear all; close all;clc;

addpath S:\svn\DSP\common\matlab

nObs = 100;
nMonte = 1000;
stdX = 2; 

muX = 100;

x = muX + stdX*randn( nObs, nMonte);
xRed = bsxfun(@rdivide, x, std(x) );

estVarX = var(x);
invEstVarX = 1./estVarX;
varInvEstVarX = var(invEstVarX);
meanInvEstVarX = mean(invEstVarX);

estMeanXRed = mean(xRed);

varEstMeanXRed = var(estMeanXRed)
% Naive theoritical value
varEstMeanXRedTheo = (1/(nMonte -1)*sum(estMeanXRed.^2)-nMonte/(nMonte -1)*mean(estMeanXRed)^2)

varEstMeanXRedTheo2 = (1/(nObs -1)*sum(xRed(:,1).^2)-nObs/(nObs -1)*mean(xRed(:,1))^2)/nObs


% varXMeanTheo = stdX^2/nObs

