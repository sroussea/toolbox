clc;close all;clear all;


syms s T0 T1 K w
H = (K+s*T1)/(1+s*T0);
inSIG = w/(s^2 + w^2);
Y=H*inSIG
syms h(t,K,T0,T1)
syms y(t,w,K,T0,T1)
syms inSig(t,w)

inSig = sin(w*t)
h(t,K,T0,T1)=ilaplace(H)
y(t,w,K,T0,T1)=ilaplace(Y)


syms tau real;
    
    % By the definition of convolution:
    atau = subs(h, t, t - tau); % transform all to the tau axis
    btau = subs(inSig, t, tau); % give one the t-tau
    c = int(atau * btau, tau, -inf, inf)
    
    % Preemptively repair dumb expressions:
    c = rewrite(c, 'heaviside')
c = rewrite(c, 'exp')


return

K=double(1);
T0=double(1);
T1=double(10);

% Define time slot
N = 200 ; tmax = 15;
t = linspace(0,tmax,N);
w=2*pi*(1/4);
inSig = sin(w.*t);

% numerical conv with a sinus
h_num = double(h(t,K,T0,T1));
y_num=y(t,w,K,T0,T1);

tConv = linspace(t(1),2*t(end),length(y_num));
figure;plot(t,inSig, 'b',t,y_num,'r')
return

