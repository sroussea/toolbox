clc;close all;clear all;

% Study case : T0.y' + y = T1.u' + K.u

% Define time slot
N = 400;
tmax = 15;
K=2; 
tau0 = 1;tau1 = 1;
w=2*pi*(1/4);
t = linspace(0,tmax,N);
% impulse response
h=1./tau0*exp(-t/tau0).*(K - tau1/tau0);
myDirac = zeros(1,length(h)); myDirac(1) = 1;
%h=h+myDirac*tau1/tau0;
% input signal
inSig = sin(w.*t);
alf=K/(1+(tau0*w)^2);

%Theoritical derivation of the signal 
sigTheo1=alf*( sin(w*t) - w*tau0*cos(w*t) + w*tau0*exp(-t/tau0) );
sigTheo2=alf/K*tau1*w*( cos(w*t) + sin(w*t) - exp(-t/tau0) );
%sigTheoOut=sigTheo1 ;
sigTheoOut=sigTheo1 + sigTheo2;

% Numerical derivation of the signal 
sigOutNum=conv(h,inSig);
% !!!! Have to normalize the conv result by the discretization step tmax/N 
sigOutNum=sigOutNum*tmax/N;

figure('name','numerical vs Theo');
fig(:,1) = plot(t,sigTheoOut,'bo'); hold on;
fig(:,2) = plot(linspace(t(1),2*t(end),length(sigOutNum)),sigOutNum,'g'); hold off;
legend(fig(1,:),{['theo'], ['Num']});

return

figure('name','numerical');plot(t,inSig,'b',t,h,'r',linspace(t(1),2*t(end),length(sigOut)),sigOut,'g');
figure;plot(t,sigPermTheo1+sigPermTheo2,'b',t,sigTransTheo,'r',t,sigTheoOut,'g');
figure('name','theo vs numerical');plot(t,sigTheoOut,'r',linspace(t(1),2*t(end),length(sigOut)),sigOut,'g');

