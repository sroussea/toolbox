clc;close all;clear all;

% Define time slot
N = 400;
tmax = 15;
K=2; 
tau = 1;w=2*pi*(1/4);
t = linspace(0,tmax,N);
h=K/tau.*exp(-t/tau);
inSig = sin(w.*t);
alf=K/(1+(tau*w)^2);

%Theoritical derivation of the signal 
sigPermTheo1=alf*sin(w*t);
sigPermTheo2=-alf*w*tau*cos(w*t);
sigTransTheo=alf*w*tau*exp(-t/tau);
sigTheoOut=sigPermTheo1+sigPermTheo2+sigTransTheo;

% Numerical derivation of the signal 
sigOut=conv(h,inSig);
% !!!! Have to normalize the conv result by the discretization step tmax/N 
sigOut=sigOut*tmax/N;


figure('name','numerical');plot(t,inSig,'b',t,h,'r',linspace(t(1),2*t(end),length(sigOut)),sigOut,'g');
figure;plot(t,sigPermTheo1+sigPermTheo2,'b',t,sigTransTheo,'r',t,sigTheoOut,'g');
figure('name','theo vs numerical');plot(t,sigTheoOut,'r',linspace(t(1),2*t(end),length(sigOut)),sigOut,'g');

