function [y] = myLeastSquare(  polyCoeffs, polyDeg, tRestriction, expSignal )

polyDegs = 0:polyDeg;

tPowered = bsxfun(@power, tRestriction,polyDegs);
fUnSummed = bsxfun(@times, tPowered, polyCoeffs); 
polyModel = sum(fUnSummed,2);

% compute the derivation of the model, then sum its value for each t value
% Trying to maximize this sum is equivalent to seek for the most monotonic
% rising edge => minimize the noise absorption into the model... ?
% tPowered = bsxfun(@power, tRestriction',(0:length(polyCoeffs)-1));
% dfCoeffs = bsxfun(@times, polyCoeffs, (1:length(polyCoeffs))); 
% dfUnSummed = bsxfun(@times, tPowered, dfCoeffs); 
% dfWeight = sum(sum(abs(dfUnSummed),2));
% 
% if ( minWeight > dfWeight )
%   minWeight = dfWeight;
% end

y =  (expSignal - polyModel)'*(expSignal - polyModel) ;

end
