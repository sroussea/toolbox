
titre = ['Sélection de ',num2str(size(myCut,2)), ' signaux dans la bande d''énergie [',...
  num2str(min(Ecur),3),'-',num2str(max(Ecur),3),'] Mev'];
 figure('name',titre,'NumberTitle','off');
plot(Ecur,ImaxCur,'bo','MarkerSize',2);
title(titre);
ylabel('Amplitude maximum du signal de courant en LSB');xlabel('Energie en Mev');
xlim([3.65 4.55]);ylim([280 550]);

hold on;
pop1 = -60+130*Ecur;
plot(Ecur ,pop1 , 'r')

pop2 = -115+130*Ecur;
plot(Ecur ,pop2 , 'g')

dQ = 20;
[val idsPop1]  = find (ImaxCur > pop1 - dQ & ImaxCur < pop1 + dQ )
plot(Ecur(idsPop1), ImaxCur(idsPop1),'ro')

[val idsPop2]  = find (ImaxCur > pop2 - dQ & ImaxCur < pop2 + dQ )
plot(Ecur(idsPop2), ImaxCur(idsPop2),'ro')

figure;
plot(score(:,1),score(:,2),'bo'); hold on;
plot(score(idsPop1,1),score(idsPop1,2),'ro'); 
plot(score(idsPop2,1),score(idsPop2,2),'go'); 

figure;
plot(score(:,2),score(:,3),'bo'); hold on;
plot(score(idsPop1,2),score(idsPop1,3),'ro'); 
plot(score(idsPop2,2),score(idsPop2,3),'go'); 

figure;
plot(score(:,1),score(:,3),'bo'); hold on;
plot(score(idsPop1,1),score(idsPop1,3),'ro'); 
plot(score(idsPop2,1),score(idsPop2,3),'go'); 
