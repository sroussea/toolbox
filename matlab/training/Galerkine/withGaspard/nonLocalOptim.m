function [y, meanSq] = nonLocalOptim(  f1Restrictions, polyDegs )

  popVarArray = zeros(1, sum(polyDegs) + size(f1Restrictions , 2));
  meanSqTmp = zeros(1, size(f1Restrictions , 2));
  title_ = 'exp/model signals';
  figure('name',title_,'NumberTitle','off'); 

  for iRange = 1:size(f1Restrictions , 2)
    % non local fit 
    gs = GlobalSearch;
    lowB = -20*ones(1,polyDegs(iRange)+1);upB = 20*ones(1,polyDegs(iRange)+1);
    lowB(1) = 0; 
    x0 = 0.1*ones(1,polyDegs(iRange)+1);  
 
    expSignal    = f1Restrictions{1,iRange};
    upB(1) = 10*max(expSignal);
    
    tRestriction = f1Restrictions{2,iRange};
    myFunc = @(polyCoeffs )(myLeastSquare(  polyCoeffs, polyDegs(iRange), tRestriction, expSignal  ));
    
    problem = createOptimProblem('fmincon','x0',x0,...
      'objective',myFunc,'lb',lowB,'ub',upB);
    [polyCoeffsFitted, fmin, flag, outpt, allmins] = run( gs, problem );
   
    popVarArray( iRange + sum(polyDegs(1:iRange)) - polyDegs(iRange)   : sum(polyDegs(1:iRange)) + iRange ) = polyCoeffsFitted;
    meanSqTmp(iRange) = fmin;
    
    tPowered  = bsxfun(@power, tRestriction,0:polyDegs(iRange));
    fUnSummed = bsxfun(@times, tPowered, polyCoeffsFitted); 
    polyModel = sum(fUnSummed,2);
    hPlot(:,1) = plot(tRestriction,expSignal,'r');hold on;    
    hPlot(:,2) = plot(tRestriction,polyModel,'bo');

  end
  
  hold off;
  y  = popVarArray;
  meanSq = meanSqTmp;
end
