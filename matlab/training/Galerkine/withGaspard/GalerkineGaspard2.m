clc;close all;clear all;
% Change default axes fonts.
set(0,'DefaultAxesFontName', 'Times New Roman')
set(0,'DefaultAxesFontSize', 16)

% Change default text fonts.
set(0,'DefaultTextFontname', 'Times New Roman')
set(0,'DefaultTextFontSize', 16)

addpath D:\tmp\GaspardData\matlabRdy\cuts
addpath S:\svn\DSP\common\matlab
addpath S:\svn\DSP\Analysis\matlab\noiseStudy1\res

% addpath /vol0/rousseau/locData/run_2012_10/cuts
% addpath /vol0/rousseau/locData/run_2012_10/morphCleaned
% addpath /vol0/rousseau/curAn/common/matlab
%  addpath /vol0/rousseau/curAn/matlab/noiseStudy1/res/


% ===========> TO FIT
%fileName = 'run_0157.dat.11Oct12_02h51m06s._307905Tks_2560Pts_00_01_300V_MultiM1.bin';
fileName = 'run_0157.11Oct12_02h51m06s._307905Tks_2560Pts_00_01_300V_pCut_Q280_600_I300_1000.bin';

tic;


% ===========> TO FIT
%% Perform  selection into (E-m) graph
OneMev300V = round(1/1.4999e-2);
% Read curves from file
[tksIRaw tksQRaw Imax, Qmax ] = readCurves( fileName );

% % SR 31st ofMay : cannot go down 500 since signal must softly fade away to 0
roiT=(290:550);
% roiT=(300:2500);

% remove null tracks
[xxx id] = find( max(abs(tksIRaw(roiT,:))) < 25);
validIds = (1:size(tksIRaw,2));
validIds=validIds(setdiff(1:length(validIds),id));
tksIRaw = tksIRaw(:,validIds);
tksQRaw = tksQRaw(:,validIds);
Imax = Imax(validIds);
Qmax = Qmax(validIds);

%sort tracks by ascendent energy
energSortedIds = sortrows([Qmax',(1:length(Qmax))']);
tksIRaw = tksIRaw(:,energSortedIds(:,2));
tksQRaw = tksQRaw(:,energSortedIds(:,2));

%% Haar filter SR 12th of March  2014
% => cannot use those curves since I have no model to fit with.... have to
% think about this way
% k=2; K0 = 1/2^((k+1)/2);
% numF = zeros(1, 2^(k+1)+1);
% denF = zeros(1, 2^(k+1)+1);
% numF(1) = 1; 
% numF(2^k+1) = -2;
% numF(2^(k+1)+1) = 1;
% numF = K0*numF;
% denF(1) = 1;
% denF(2) = -1;

% tksIRawHaar = filter(numF, denF, tksIRaw);
% tksIRaw = tksIRawHaar;

Imax = Imax(energSortedIds(:,2));
Qmax = Qmax(energSortedIds(:,2));


% Create a cut to select a particular energy range
minQmax =  min(Qmax); maxQmax = max(Qmax);
dE = (maxQmax - minQmax )/length(Qmax)*100;
binsQmax = (0 : round((maxQmax-minQmax)/dE));

nBin = 1;
myCut300V_2 = find( Qmax >= minQmax+dE*binsQmax(nBin) & Qmax < minQmax+dE*binsQmax(end));
%myCut300V_2 = find( Qmax >= minQmax+dE*binsQmax(nBin) & Qmax < minQmax+dE*binsQmax(nBin+1));
myCut = myCut300V_2;
myCut=myCut(1:1);

E300V = Qmax.*1.4999e-2 -4.96922e-1;

Ecur       = E300V(myCut);
ImaxCur = Imax(myCut);

% OneMev275V = round(1/1.4e-2);
% QRange275V_1 = [150,150+OneMev275V];
% QRange275V_2 = [469-OneMev275V, 469];
% [ Imax, Qmax, myCut275V_1 ] = biDimCut( fileName, QRange275V_1 );
% [ Imax, Qmax, myCut275V_2 ] = biDimCut( fileName, QRange275V_2 );
% % myCut = myCut275V_2;
% myCut = myCut275V_1;
% E275V = Qmax.*1.4e-2 + 3.0e-2;
% Ecur = E275V;


% OneMev250V = round(1/1.4e-2);
% QRange250V_1 = [154,154+OneMev250V];
% QRange250V_2 = [471-OneMev250V, 471];
% [ Imax, Qmax, myCut250V_1 ] = biDimCut( fileName, QRange250V_1 );
% [ Imax, Qmax, myCut250V_2 ] = biDimCut( fileName, QRange250V_2 );
% E250V = Qmax.*1.4e-2 + 3.0e-2;
% myCut = myCut250V_1;
% myCut = myCut250V_2;
% E250V = Qmax.*1.4e-2 + 3.0e-2;
% Ecur = E250V;


titre = ['Sélection de ',num2str(size(myCut,2)), ' signaux dans la bande d''énergie [',...
  num2str(min(Ecur),3),'-',num2str(max(Ecur),3),'] Mev'];
 figure('name',titre,'NumberTitle','off');
plot(Ecur,ImaxCur,'bo','MarkerSize',2);
title(titre);
ylabel('Amplitude maximum du signal de courant en LSB');xlabel('Energie en Mev');
xlim([3.65 8.55]);ylim([280 1000]);


% return;

% keep only E selected tracks in memory
tksIRaw = tksIRaw(:,myCut);

% select only region of interest 
Itk(:,:) = tksIRaw(roiT,:);
figure;
 plot(Itk,'b'); 

% figure;hold on;
ft_ = fittype('poly1');
parfor i = 1:size(Itk,2)

  [maxI maxID] = max( Itk( :, i ) );
  
  alf = .5;
  dataSetId = find ( Itk(1:maxID,i) > .1 * maxI & Itk(1:maxID,i) < alf * maxI );
 % if rising edge skewness is very steep, we enlarge the data set
 while ( (length(dataSetId) < 3) & ( alf <= 1 ) )
  alf = alf+.1;
  dataSetId = find ( Itk(1:maxID,i) > .1 * maxI & Itk(1:maxID,i) < alf * maxI );
 end   
  
%   plot(dataSetId,Itk(dataSetId,i),'bo');

  [fitRes gof] = fit(dataSetId,Itk(dataSetId,i),ft_);

%  SR 7th of November : dataSetId(1)-10 : 10 to make linear interpolation
%  cross the abscisse axe
  xRangeInter = (dataSetId(1)-10:dataSetId(end) );
  interpolI =fitRes.p1.*xRangeInter +fitRes.p2;
 % plot(xRangeInter ,interpolI,'r');
  
  [row iDs] = find(interpolI<0);
  
  startID(i) = xRangeInter(iDs(end));

%% 9th of January 2014: seems to deteriorate the fit quality !!!
%% Has to be understood
%   newStartID = startID(i);
%   if Itk( startID(i) , i ) > 5
%     pulseAtStart = Itk( newStartID ,i );
%     while ( pulseAtStart > 5 )
%       newStartID = newStartID - 1;
%       pulseAtStart = Itk( newStartID ,i );
%     end
%     startID(i) = newStartID;
%   end
  
end
% hold off;

% figure;hold on;

% Some parameters
nbPtHighRes = 900;
nbOfPoints = 100;
roiStart = 0;
% polynome degree for each function slice
polyDegs = [5 6 3];
rangeNb = length( polyDegs );
movAvSize = 3;  % MA windows size to apply to the signal derivation
% Nb of points where each signal slices overlap each other
ovLapNb = 2 ;
ovLapNbHiRes = ovLapNb*nbPtHighRes/nbOfPoints ;    
maxIdShift = 7; % Nb of points to take into account after the max in the second slice

xc1 = (roiStart:length(roiT)-1+roiStart)';
% To make length(xc1)/rangeNb an integer
xc1 = xc1(1:nbOfPoints);

xc1HighRes = linspace(xc1(1), xc1(end),nbPtHighRes) ;
% array of variables. One Line per individual, each column is a variable
popVarArray = zeros( size(Itk,2),  sum(polyDegs) + rangeNb );

theoCurv = zeros(nbOfPoints, size(Itk,2));
theoCurvHiRes = zeros(nbPtHighRes, size(Itk,2));

fOptimHiRes = zeros(1,nbPtHighRes);
fOptim = zeros(1,nbOfPoints);

% parfor iFit = 1:size(Itk,2)
for iFit = 1:size(Itk,2)
f1Restrictions = cell(2,3);
  
  
  tmp = tksIRaw( roiT,iFit);
  tksIExp(:,iFit) = [tmp(startID(iFit)+roiStart:end); zeros(startID(iFit)-1+roiStart,1 ) ];  
  fToFIt = tksIExp(:,iFit);
  
  fSplined = spline(xc1, fToFIt(1:nbOfPoints), xc1HighRes);
  
  % Build sub-ranges : { [1:idX0]; 
%   dfSplined = movAver( diff( fSplined ), movAvSize );
%   [m, idX0 ] = max( dfSplined );
%   [m, idX1 ] = max( fSplined ); 
  dfRaw = movAver( diff( fToFIt ), movAvSize );
  [m, idX0 ] = max( dfRaw );
  [m, idX1 ] = max( fToFIt );
  idX1 = idX1 + maxIdShift;

  xFitIds = (1 : idX0 + ovLapNb);
%   xFitRange1 = xc1HighRes(xFitIds);
%   f1Restrictions{1,1} = fSplined(xFitIds);
  xFitRange1 = xc1(xFitIds);
  f1Restrictions{1,1} = fToFIt(xFitIds);
  f1Restrictions{2,1} = xFitRange1;
  
  xFitIds = (idX0 - ovLapNb : idX1 + ovLapNb );
%   xFitRange2 = xc1HighRes(xFitIds);
%   f1Restrictions{1,2} = fSplined(xFitIds);
  xFitRange2 = xc1(xFitIds);
  f1Restrictions{1,2} = fToFIt(xFitIds);
  f1Restrictions{2,2} = xFitRange2;

%   xFitIds = (idX1 - ovLapNb : length(fSplined));
%   xFitRange3 = xc1HighRes(xFitIds);
%   f1Restrictions{1,3} = fSplined(xFitIds);
  xFitIds = (idX1 - ovLapNb : length(xc1));
  xFitRange3 = xc1(xFitIds);
  f1Restrictions{1,3} = fToFIt(xFitIds);
  f1Restrictions{2,3} = xFitRange3;
 

%   
%   popVarArray(iFit,:  ) = makeFit( rangeNb, polyDeg, tRangeSlots,...
%                              f1Restrictions);
  popVarArray(iFit,:  ) = makeFit( polyDegs, f1Restrictions);

 
  coefs = popVarArray(iFit, :  );
%   xRangeIds = [ 0 idX0 idX1 length(xc1HighRes)];
  xRangeIds = [ 0 idX0 idX1 length(xc1)];
  myColors = ['r' 'g' 'b'];
 
%   figure;
  
  fTemp =  zeros(1,nbOfPoints);
  for iRangeId = 1:rangeNb
    xxIds = xRangeIds(iRangeId)+1:xRangeIds(iRangeId+1);
    xxPowered = bsxfun(@power, xc1(xxIds),(0:polyDegs(iRangeId)));
%     xxPowered = bsxfun(@power, xc1HighRes(xxIds)',(0:polyDegs(iRangeId)));
    fUnSummed = bsxfun(@times, xxPowered, coefs(sum(polyDegs(1:iRangeId))+iRangeId:-1:sum(polyDegs(1:iRangeId))+iRangeId - polyDegs(iRangeId))); 
%     fOptimHiRes(xxIds ) = sum(fUnSummed,2);
    fTemp(xxIds) = sum(fUnSummed,2);
    
%     plot( f1Restrictions{2,iRangeId}, f1Restrictions{1,iRangeId}, myColors(iRangeId),...
%           xc1(xxIds), fOptim(xxIds ),myColors(iRangeId) ); hold on;    
  end  
  fOptim = fTemp;
  hold off;
  
%   theoCurvHiRes(:,iFit) = fOptim(:);
  theoCurv(:,iFit) = fOptim(:);
%   figure; plot(xc1, tksIExp(1:nbOfPoints,:),'b'); hold on;
%   plot(xc1, theoCurv(:,:),'r');


end

% ------------ > add Energy in the var list
% popVarArrayTmp = zeros(size(popVarArray ,1), size(popVarArray ,2) + 1);
% popVarArrayTmp(:,1:size(popVarArray ,2)) = popVarArray;
% popVarArrayTmp(:,end) = Ecur';
% popVarArray = popVarArrayTmp;
% clear popVarArrayTmp;

popVarArray = zeros(size(popVarArray ,1), 2);
popVarArray(:,1) = Ecur;
popVarArray(:,2) = ImaxCur;


redData = bsxfun(@minus,popVarArray,mean(popVarArray));
redData = bsxfun(@rdivide,redData,sqrt(var(redData,0)));
[pc,score,latent,tsquare] = princomp(redData);
varLab = cell(1,size(popVarArray,2));
for iVar = 1:size(popVarArray,2)
 varLab{iVar} = ['var' num2str(iVar)];
end
plotPCA(varLab, pc , score , latent,2 );

figure;
plot(score(:,1),score(:,2),'bo'); 
figure;
plot(score(:,1),score(:,3),'bo'); 
figure;
plot(score(:,2),score(:,3),'bo'); 
figure;plot(E300V(myCut), Imax(myCut),'bo','MarkerSize',2);
figure; plot(xc1, tksIExp(1:nbOfPoints,:),'b'); hold on;
% plot(xc1HighRes, theoCurvHiRes(:,:),'r'); hold off;
plot(xc1, theoCurv(:,:),'r'); hold off;
return;

% try with the moments
M1 = mymoment(xc1, theoC, 1);
M2 = mymoment(xc1, theoC, 2);
M3 = mymoment(xc1, theoC, 3);
M4 = mymoment(xc1, theoC, 4);

% compute E/Imax from theo curves
Etheo = sum(theoC);
Etheo = Etheo.*1.4999e-2 -4.96922e-1;
ImaxTheo = max(theoC);
%figure; plot( Etheo, ImaxTheo,'bo');

% figure; plot( Etheo,M1,'bo');
% figure; plot( Etheo,M2,'bo');
% figure; plot( Etheo,M3,'bo');
% figure; plot( Etheo,M4,'bo');


% build observation matrix
% X = [ Ecur' Imax(myCut)' tau' b' t2']
% redData = bsxfun(@minus,X,mean(X));
% redData = bsxfun(@rdivide,redData,sqrt(var(redData,0)));
% [pc,score,latent,tsquare] = princomp(redData);
%X = [ Ecur' Imax(myCut)' tau' (M1e)' b' t2' ];

% best combination SR : 05th of March 2014
% Better when using M1 with theo curves
X = [ Etheo' ImaxTheo' tau' M1' b' t2'];

redData = bsxfun(@minus,X,mean(X));
redData = bsxfun(@rdivide,redData,sqrt(var(redData,0)));
[pc,score,latent,tsquare] = princomp(redData);
varLab = {'E','Imax','tau','M1', 'b', 't2'};
plotPCA(varLab, pc, score, latent);

% X = [ Etheo' ImaxTheo' tau' M4' b' t2'];
% redData = bsxfun(@minus,X,mean(X));
% redData = bsxfun(@rdivide,redData,sqrt(var(redData,0)));
% [pc,score,latent,tsquare] = princomp(redData);
% varLab = {'E','Imax','tau','M4', 'b', 't2'};
% plotPCA(varLab, pc, score, latent);
% return;


% Select punch through protons from pc1 vs pc2
figure;plot(score(:,1),score(:,2),'bo','MarkerSize',1);hold on;
d1 =  -1.46+score(:,1)*0.85;plot(score(:,1),d1,'go','MarkerSize',1);  
pTpIds = find( ( score(:,1)> -1 &  score(:,2) > -2.5 &  score(:,2)  < 1.05) ... 
  &abs(d1-score(:,2)) <.8 );
pTpLessIds = setdiff(myCut, pTpIds);
plot(score(pTpIds,1),score(pTpIds,2) ,'ro','MarkerSize',3);hold off;


figure('name','Cluster 1 selection from pc1 Vs pc2','NumberTitle','off');
%ylabel('arbitrary units');xlabel('arbitrary units ');
plot(Ecur,ImaxCur,'bo','MarkerSize',1);hold on;
plot(Ecur(pTpIds),ImaxCur(pTpIds),'go','MarkerSize',3);hold off;
ylabel('Amplitude maximum du signal de courant en LSB');xlabel('Energie en Mev');
title('Cluster 1 selection from pc1 Vs pc2');

% remove cluster 1 from pc2 vs pc3 diagram
% it turns out that we have just p, d, t clearly separated
score2PTPLess = score(pTpLessIds,2);
score3PTPLess = score(pTpLessIds,3);
titre = 'Cluster 1 dans graphe pc3 vs pc2';
figure('name',titre,'NumberTitle','off');

plot(score(:,2),score(:,3),'go','MarkerSize',2);hold on;
plot(score2PTPLess,score3PTPLess  ,'bo','MarkerSize',2); hold off;
ylabel('Unité arbitraire');xlabel('Unité arbitraire');
xlim([-2.4 4.5]);ylim([-3 3]);
title(titre);

% Select cluster 2 from pc3 vs pc2
d2 =  .5+ score2PTPLess*0.77;
cut2 = find( (    abs( d2 - score3PTPLess ) <.6  ));
figure;
plot( score2PTPLess,score3PTPLess ,'ro','MarkerSize',2);hold on;
plot(score2PTPLess ,d2,'go','MarkerSize',1);  
plot(score2PTPLess(cut2) ,score3PTPLess(cut2)  ,'bo','MarkerSize',3); hold off;

% Select cluster 3 from pc3 vs pc2
d3 =  -1+ score2PTPLess*0.6;
cut3 = find( (    score3PTPLess < 1.85) ... 
  &abs( d3 - score3PTPLess ) <.6 );
figure;
plot( score2PTPLess,score3PTPLess ,'ro','MarkerSize',2);hold on;
plot(score2PTPLess ,d3,'go','MarkerSize',1);  
plot(score2PTPLess(cut3) ,score3PTPLess(cut3)  ,'bo','MarkerSize',3); hold off;

% Select cluster 4 from pc3 vs pc2
d4 =  -2+ score2PTPLess*0.6;
cut4 = find( (    abs( d4 - score3PTPLess ) <.5  ));
figure;
plot( score2PTPLess,score3PTPLess ,'ro','MarkerSize',2);hold on;
plot(score2PTPLess ,d4,'go','MarkerSize',1);  
plot(score2PTPLess(cut4) ,score3PTPLess(cut4)  ,'bo','MarkerSize',3); hold off;

% Plot des clusters dans le graphe pc3 vs pc2
titre = 'Sélection de clusters dans le graphe pc3 vs pc2';
figure('name',titre,'NumberTitle','off');
fig(:,1) = plot(score(:,2),score(:,3),'go','MarkerSize',2);hold on;
fig(:,2) = plot(score2PTPLess(cut2) ,score3PTPLess(cut2)  ,'bo','MarkerSize',3); 
fig(:,3) = plot(score2PTPLess(cut3) ,score3PTPLess(cut3)  ,'ro','MarkerSize',3); 
fig(:,4) = plot(score2PTPLess(cut4) ,score3PTPLess(cut4)  ,'co','MarkerSize',3); hold off;
ylabel('Unité arbitraire');xlabel('Unité arbitraire');
xlim([-2.4 4.5]);ylim([-2 3]);
title(titre);
legend(fig(1,:),{['Cluster 1'], ['Cluster 2'], ['Cluster 3']}, ['Cluster 4']);
clear fig;

% Plot des clusters 2,3 et 4 dans le graphe pc3 vs pc2
titre = 'Sélection de clusters dans le graphe pc3 vs pc2';
figure('name',titre,'NumberTitle','off');
fig(:,1) = plot(score2PTPLess(cut2) ,score3PTPLess(cut2)  ,'bo','MarkerSize',3); hold on;
fig(:,2) = plot(score2PTPLess(cut3) ,score3PTPLess(cut3)  ,'ro','MarkerSize',3); 
fig(:,3) = plot(score2PTPLess(cut4) ,score3PTPLess(cut4)  ,'co','MarkerSize',3); hold off;
ylabel('Unité arbitraire');xlabel('Unité arbitraire');
xlim([-2.4 4.5]);ylim([-2 3]);
title(titre);
legend(fig(1,:),{ ['Cluster 2'], ['Cluster 3']}, ['Cluster 4']);
clear fig;

% Plot des clusters dans le graphe Imax vs Q
e300VpTpLessIds = Ecur(pTpLessIds);
iMaxpTpLessIds   = ImaxCur(pTpLessIds);
titre = ' Correspondances entre clusters du graphe pc3 Vs pc2 et du graphe I_{max} vs E ';
figure('name',titre,'NumberTitle','off');
fig(:,1) = plot(Ecur(pTpIds), ImaxCur(pTpIds) ,'go','MarkerSize',2);hold on;
fig(:,2) = plot(  e300VpTpLessIds(cut2),iMaxpTpLessIds (cut2) ,'bo','MarkerSize',2); 
fig(:,3) = plot(  e300VpTpLessIds(cut3),iMaxpTpLessIds (cut3) ,'ro','MarkerSize',2); 
fig(:,4) = plot(  e300VpTpLessIds(cut4),iMaxpTpLessIds (cut4) ,'co','MarkerSize',2); 
hold off;
ylabel('Amplitude maximum du signal de courant en LSB');xlabel('Energie en Mev');
title(titre);
legend(fig(1,:),{['Cluster 1'], ['Cluster 2'], ['Cluster 3']}, ['Cluster 4']);
xlim([3.7 8.5]);ylim([290 1000]);
clear fig;



% plot absorded protons descrimined from punch through ones in (Imax-Q)
% graph.
% We can see that the PCA improves the discrimination up to 7Mev while
%  (Imax-Q) space succeeds only until 5.5 Mev
%% Trying to identify only NON punch through protons selected from pc2 vs pc1 diagram
% orientation line of the protons in the Q/Imax diagram
dProt = -105 + Ecur*127;
plot(Ecur, dProt,'r');
% Select closest point from the orientation line
cutProtOnly = find( (dProt -  ImaxCur  < 10 ));
% keep only points belonging to pTpLessIds
cutProtOnly = intersect( cutProtOnly, pTpLessIds);

% Highlight absorbed protons over the cluster extracted from pc1 vs pc2
titre = ' Cluster 1';
figure('name',titre,'NumberTitle','off');;
fig(:,1) = plot(e300VpTpLessIds,iMaxpTpLessIds,'bo','MarkerSize',2); hold on;
%fig(:,2) = plot(Ecur(cutProtOnly), ImaxCur(cutProtOnly) , 'go', 'MarkerSize', 5 ); 
fig(:,2) = plot(Ecur(pTpIds), ImaxCur(pTpIds), 'ro', 'MarkerSize',2); hold off;
xlim([3.65 8.55]);ylim([280 1000]);
ylabel('Amplitude maximum du signal de courant en LSB');xlabel('Energie en Mev');
title(titre);
legend(fig(1,:),{['Toutes traces \ cluster 1'], ['Cluster 1']});
clear fig;

% Show NON punch through protons on pc1 vs pc2 diagram
figure;plot(score(:,1),score(:,2),'bo','MarkerSize',1);hold on;
plot(score(pTpIds,1),score(pTpIds,2) ,'ro','MarkerSize',3);
plot(score(cutProtOnly,1),score(cutProtOnly,2) ,'go','MarkerSize',3);hold off;



cellFig = {figMosaic1, figMosaic2, figMosaic3, figMosaic4};
writeResToFile( tau, tau1, tau2, t1, t2, a, b,...
    stdDifTheoExp, rSq, cellFitInput, cellFig);

ovrHead=toc


return;
  f3=figure;hold on;
for iTrack = 1:size( difTheoExp,2)
  
  h = spectrum.periodogram('Blackman-Harris');
%   h = spectrum.periodogram;
  hopts = psdopts(h)
  set(hopts,'Fs',Fs,'SpectrumType','onesided');
  myPsd = psd(h,difTheoExp(:,iTrack),hopts)
  avgPowPerio_f(iTrack) = avgpower(myPsd)
%   plot(myPsd.Frequencies(40:end),10.^(myPsd.data(40:end)/10))
%   plot(myPsd.Frequencies(40:end),10*log10(myPsd.data(40:end)))
  myPsdData(:,iTrack) = myPsd.data;

end
hold off;

QmaxTheo = 2*a./b.^3;
ImaxTheo = a.*((2./(b*exp(1)).^2));
%  figure(7);plot(2*a./b.^3,a.*((2./(b*exp(1)).^2)),'bo');
figure(8); plot(QmaxTheo/ mean(QmaxTheo./Qmax),ImaxTheo/mean(ImaxTheo./Imax),'ro',Qmax,Imax,'bo')

figure(9);
 m2=moment(Isync,2);
 plot(Qmax,m2,'ro');
 
 
