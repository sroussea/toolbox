--------------------------------------------------------------------------------------------
GalerkineGaspard.m
--------------------------------------------------------------------------------------------

We split the time slot in equal parts.
If the degree of the polynome is too large, we include some noise in the model.
=> we should keep the polynome degree equal or less than 3.

--------------------------------------------------------------------------------------------


--------------------------------------------------------------------------------------------
GalerkineGaspard2.m
--------------------------------------------------------------------------------------------

We split the time slot in 3 parts :
1) The raising edge + the hump
2) the top hill
3) The decreasing tail

We keep the polynome degree equal or less than 3.

--------------------------------------------------------------------------------------------
