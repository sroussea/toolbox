clc;close all;clear all;
addpath S:\svn\DSP\common\matlab

% Define time slot
N = 100;
t0 = 0;
tMax = 10;
tRange = linspace(t0,tMax,N);
rangeNb = 4;
tRangeSlots = reshape(tRange,N/rangeNb,rangeNb);
polyDeg = 3;

funcTypeNb = 2;
funcNb = 300;

sigNoise = 0.03;
A = [1, 1.1]'*ones(1,funcNb) + sigNoise*randn(funcTypeNb,funcNb) ;
Tau = [-1/2, -1/2*1.1]'*ones(1,funcNb) + sigNoise*randn(funcTypeNb,funcNb) ;
popVarArray = zeros( funcNb*funcTypeNb, rangeNb*(polyDeg+1));
funcArray = zeros(funcNb*funcTypeNb, N  );

popVarArrayTmp = zeros( funcNb, rangeNb*(polyDeg+1));
funcArrayTmp = zeros(funcNb, N);

for iFType = 1:funcTypeNb 
  for iFId = 1:funcNb 

    f1 =(A(iFType, iFId)*tRange.*exp(tRange.*Tau(iFType, iFId)));
    funcArrayTmp(iFId, :) = f1;   
    f1Restrictions = reshape(f1, N/rangeNb, rangeNb);    

    popVarArrayTmp(iFId,:  ) = makeFit( rangeNb, polyDeg, tRangeSlots,...
      f1Restrictions);
    
  end
  funcArray((iFType -1)*funcNb + 1:iFType*funcNb,:) = funcArrayTmp;
  popVarArray((iFType -1)*funcNb + 1:iFType*funcNb,:) = popVarArrayTmp;
end

figure; plot(tRange,funcArray(1:funcNb,:),'b'); hold on;
plot(tRange,funcArray(funcNb+1:2*funcNb,:),'r'); hold off;

redData = bsxfun(@minus,popVarArray,mean(popVarArray));
redData = bsxfun(@rdivide,redData,sqrt(var(redData,0)));
[pc,score,latent,tsquare] = princomp(redData);
% varLab = {'x','y','z'};
% figure;plotPCA(varLab, pc, score, latent);
figure;
plot(score(1:funcNb,1),score(1:funcNb,2),'bo'); title('1 vs 2');
hold on;
plot(score(funcNb+1:2*funcNb,1),score(funcNb+1:2*funcNb,2),'ro'); 
figure;
plot(score(1:funcNb,1),score(1:funcNb,3),'bo'); title('1 vs 3');
hold on;
plot(score(funcNb+1:2*funcNb,1),score(funcNb+1:2*funcNb,3),'ro'); 
figure;
plot(score(1:funcNb,2),score(1:funcNb,3),'bo'); title('2 vs 3');
hold on;
plot(score(funcNb+1:2*funcNb,2),score(funcNb+1:2*funcNb,3),'ro'); 

return;

  
figure;plot(tRange,reshape(f1,1,N),'b');hold on;
plot(rang1,restrictionF1,'r')

