%% SR : 27th of October 2015
% 
% Try to fit the rising edge of an exponential sum with a polynomial model
% Perform a non local least square optimization, then try to prove that the
% variance explained by the model decreases as the coeff order increases.

clc;close all;clear all;
addpath S:\svn\DSP\common\matlab

% Define time slot
N = 100;
t0 = 0;
tMax = 10;
tRestrictID = 20;
tRange = linspace(t0,tMax,N);
% idStart = 1; idStop = 20; 
idStart = 20; idStop = 40; 
tRestriction = tRange(idStart:idStop);

% f(t) = A1.t.exp(-t/tau1) + A2.t^3.exp(-t/tau2)
% the t^3 in f2 to slow down the rising edge
% Build 2 families of pulses
funcNbPerFamily = 100; familyNb = 2;
sig = 0.01;
A1 = [0.6, 0.61]'*ones(1,funcNbPerFamily) + sig*randn(familyNb,funcNbPerFamily) ;
A2 = [0.25, 0.27]'*ones(1,funcNbPerFamily) + sig*randn(familyNb,funcNbPerFamily) ;
tau1 = [0.55, 0.6]'*ones(1,funcNbPerFamily) + sig*randn(familyNb,funcNbPerFamily) ;
tau2 = [0.9, .92]'*ones(1,funcNbPerFamily) + sig*randn(familyNb,funcNbPerFamily) ;

aT = bsxfun(@times, tRange',reshape(A1',1,length(A1(:)))); 
tOvTau = bsxfun(@rdivide, tRange',reshape(tau1',1,length(tau1(:)))); 
f1 = aT.*exp(-tOvTau);
tPow3 = bsxfun(@power, tRange',3);
aTPow3 = bsxfun(@times, tPow3,reshape(A2',1,length(A2(:)))); 
tOvTau = bsxfun(@rdivide, tRange',reshape(tau2',1,length(tau2(:)))); 
f2 = aTPow3.*exp(-tOvTau);
pureSig = f1 + f2;

pureSig = pureSig + 0.01*randn(size(pureSig));

title_ = 'pure signal';
figure('name',title_,'NumberTitle','off'); 
title(title_);
% plot(tRange,f1,'b',tRange,f2,'g',tRange,pureSig,'r');
plot(tRange,pureSig(:,1:funcNbPerFamily),'r',...
  tRange,pureSig(:,funcNbPerFamily+1:2*funcNbPerFamily),'b' );



% non local fit 
gs = GlobalSearch;
polyExp = [1 2 3 4 5];
polyDeg = length(polyExp);
lowB = -20*ones(1,polyDeg);upB = 20*ones(1,polyDeg);
x0 = 0.1*ones(1,polyDeg);  
%  to keep polynom coeffs
popVarArray     = zeros( funcNbPerFamily*familyNb, polyDeg);
fitPolyCoeffTmp = zeros( funcNbPerFamily, polyDeg );

% title_ = 'exp/model signals';
% figure('name',title_,'NumberTitle','off'); 

for iFType = 1:familyNb 
  parfor iFId = 1:funcNbPerFamily 

    expSignal = pureSig(idStart:idStop,(iFType-1)*funcNbPerFamily+iFId)';
    myFunc = @(polyCoeffs )(myLeastSquare(  polyCoeffs, tRestriction, expSignal  ));

    
    problem = createOptimProblem('fmincon','x0',x0,...
      'objective',myFunc,'lb',lowB,'ub',upB);
    [fitPolyCoeffTmp(iFId,:) , fmin, flag, outpt, allmins] = run( gs, problem );
    
    
%     funcArrayTmp(iFId, :) = f1;   
%     f1Restrictions = reshape(f1, N/rangeNb, rangeNb);    
    
    tPowered = bsxfun(@power, tRestriction',(1:length(polyExp)));
    fUnSummed = bsxfun(@times, tPowered, fitPolyCoeffTmp(iFId,:)); 
    polyModel = sum(fUnSummed,2)';
    
%     hPlot(:,1) = plot(tRestriction,expSignal,'r');hold on;    
%     hPlot(:,2) = plot(tRestriction,polyModel,'bo');
% 
  end
%   funcArray((iFType -1)*funcNb + 1:iFType*funcNb,:) = funcArrayTmp;
  popVarArray((iFType -1)*funcNbPerFamily + 1:iFType*funcNbPerFamily,:) = fitPolyCoeffTmp;
end

% hold off;


redData = bsxfun(@minus,popVarArray,mean(popVarArray));
redData = bsxfun(@rdivide,redData,sqrt(var(redData,0)));
[pc,score,latent,tsquare] = princomp(redData);
% varLab = {'x','y','z'};
% figure;plotPCA(varLab, pc, score, latent);
figure;
plot(score(1:funcNbPerFamily,1),score(1:funcNbPerFamily,2),'bo'); title('1 vs 2');
hold on;
plot(score(funcNbPerFamily+1:2*funcNbPerFamily,1),score(funcNbPerFamily+1:2*funcNbPerFamily,2),'ro'); 
figure;
plot(score(1:funcNbPerFamily,1),score(1:funcNbPerFamily,3),'bo'); title('1 vs 3');
hold on;
plot(score(funcNbPerFamily+1:2*funcNbPerFamily,1),score(funcNbPerFamily+1:2*funcNbPerFamily,3),'ro'); 
figure;
plot(score(1:funcNbPerFamily,2),score(1:funcNbPerFamily,3),'bo'); title('2 vs 3');
hold on;
plot(score(funcNbPerFamily+1:2*funcNbPerFamily,2),score(funcNbPerFamily+1:2*funcNbPerFamily,3),'ro'); 

return;


lowB = -20*ones(1,polyDeg);upB = 20*ones(1,polyDeg);
x0 = 0.1*ones(1,polyDeg);
problem = createOptimProblem('fmincon','x0',x0,...
  'objective',myFunc,'lb',lowB,'ub',upB);
[fitPolyCoeffs, fmin, flag, outpt, allmins] = run( gs, problem );
fitPolyCoeffs

tPowered = bsxfun(@power, tRestriction',(1:length(polyExp)));
fUnSummed = bsxfun(@times, tPowered, fitPolyCoeffs); 
pureSigFit = sum(fUnSummed,2)';



sigmaNoise = 0.02;
noisedSignal = pureSig(1:tRestrictID) + sigmaNoise*randn(1,tRestrictID) ;
expSignal = noisedSignal;

[fitPolyCoeffs, fmin, flag, outpt, allmins] = run( gs, problem );
fitPolyCoeffs

tPowered = bsxfun(@power, tRestriction',(1:length(polyExp)));
fUnSummed = bsxfun(@times, tPowered, fitPolyCoeffs); 
noisedSigFit = sum(fUnSummed,2)';


title_ = 'exp/model signals';
figure('name',title_,'NumberTitle','off'); 
hPlot(:,1) = plot(tRestriction,pureSig(1:tRestrictID),'r');hold on;
hPlot(:,2) = plot(tRestriction,noisedSignal,'g'); 

hPlot(:,3) = plot(tRestriction,pureSigFit,'bo');
hPlot(:,4) = plot(tRestriction,noisedSigFit,'rs');;hold off;
title(title_);
legend(hPlot(1,:),{ 'pure signal', 'noised signal', 'pure sig fit', 'noised sig fit '} );

return;


funcTypeNb = 2;
funcNb = 300;

sigNoise = 0.03;
A = [1, 1.1]'*ones(1,funcNb) + sigNoise*randn(funcTypeNb,funcNb) ;
Tau = [-1/2, -1/2*1.1]'*ones(1,funcNb) + sigNoise*randn(funcTypeNb,funcNb) ;
popVarArray = zeros( funcNb*funcTypeNb, rangeNb*(polyDeg+1));
funcArray = zeros(funcNb*funcTypeNb, N  );

popVarArrayTmp = zeros( funcNb, rangeNb*(polyDeg+1));
funcArrayTmp = zeros(funcNb, N);

for iFType = 1:funcTypeNb 
  for iFId = 1:funcNb 

    f1 =(A(iFType, iFId)*tRange.*exp(tRange.*Tau(iFType, iFId)));
    funcArrayTmp(iFId, :) = f1;   
    f1Restrictions = reshape(f1, N/rangeNb, rangeNb);    

    popVarArrayTmp(iFId,:  ) = makeFit( rangeNb, polyDeg, tRangeSlots,...
      f1Restrictions);
    
  end
  funcArray((iFType -1)*funcNb + 1:iFType*funcNb,:) = funcArrayTmp;
  popVarArray((iFType -1)*funcNb + 1:iFType*funcNb,:) = popVarArrayTmp;
end

figure; plot(tRange,funcArray(1:funcNb,:),'b'); hold on;
plot(tRange,funcArray(funcNb+1:2*funcNb,:),'r'); hold off;

redData = bsxfun(@minus,popVarArray,mean(popVarArray));
redData = bsxfun(@rdivide,redData,sqrt(var(redData,0)));
[pc,score,latent,tsquare] = princomp(redData);
% varLab = {'x','y','z'};
% figure;plotPCA(varLab, pc, score, latent);
figure;
plot(score(1:funcNb,1),score(1:funcNb,2),'bo'); title('1 vs 2');
hold on;
plot(score(funcNb+1:2*funcNb,1),score(funcNb+1:2*funcNb,2),'ro'); 
figure;
plot(score(1:funcNb,1),score(1:funcNb,3),'bo'); title('1 vs 3');
hold on;
plot(score(funcNb+1:2*funcNb,1),score(funcNb+1:2*funcNb,3),'ro'); 
figure;
plot(score(1:funcNb,2),score(1:funcNb,3),'bo'); title('2 vs 3');
hold on;
plot(score(funcNb+1:2*funcNb,2),score(funcNb+1:2*funcNb,3),'ro'); 

return;

  
figure;plot(tRange,reshape(f1,1,N),'b');hold on;
plot(rang1,restrictionF1,'r')

