%% SR : 11th of july 2014
% Try to understand linear correlation

% http://fr.wikipedia.org/wiki/Corr%C3%A9lation_%28statistiques%29

% 
% There are several issues here:
% 
% 1) regarding why you are getting not significant values in your example:
% 
%  N = 1000;
%  X = 1*rand(N,2);
%  Y = [2*X(:,1),-2*X(:,2)];
%  [r p] = corrcoef(X,Y);
% 
% the last line is NOT computing the correlation between each column of X and each column of Y, 
% as you might expect. It is instead (look at help corrcoef) computing the correlation:
% 
%  [r p] = corrcoef([X(:),Y(:)]);
% 
% Since the two halves of X(:) are oppositely associated with the two halves 
% of Y(:) you get a very low, and not significant, correlation.
% 
% If you tried instead:
% 
%  [r p] = corrcoef([X Y]);
%  r = r(1:2,3:4);
%  p = p(1:2,3:4);
% 
% you will see that each column of X is very strongly associated with each column of Y, 
% as you would expect from your definitions of X and Y.
% 
% 2) regarding what is going on with the diagonals of p
% 
% The diagonals of p returned by corrcoef are always set to 1 (looking at 
% the corrcoef.m code it might seem that they were intended to be NaN's instead). 
% In any way, you should simply disregard those values, as they never represent 
% any meaningful test (the diagonals of r are by definition 1's). 
% To check this, you may do the following:
% 
%  x = randn(10,2);
%  [r,p] = corrcoef(x);
% 
% (the diagonal of p are 1's)
% 
% But then,
% 
%  [r,p] = corrcoef(x(:,1),x(:,1));
% 
% (and the values p(1,2) == 0, as expected from two perfectly correlated series)
% 
% 3) regarding the interpretation of p-values
% 
% Your interpretation is perfectly correct, small p-values (e.g. p<.05) 
% mean that you can reject the null hypothesis (the null hypothesis for these 
% analyses is that r = 0; i.e. that the samples are uncorrelated).



clear all;close all;clc;

N = 1000;
X = 1*randn(N,2);
%  Y = [ v1 v2 v3]. Here v1 and v2 are not correlated. v1 is correlated to
%  v3.
%  F1(corrcoef) tells that R(i,j) = C(i,j)/sqrt(C(i,i)*C(j,j))
%                                 = C(i,j)/sqrt(var(i)*var(j))
%                                 = C(i,j)/(std(i)*std(j))
%  We can easily show that corr(v1,v3) = 1/sqrt(2) :
%  1st develop C(v1,v3) = E[(v1 -mean(v1))(v3 -mean(v3))] which yield to var(v1)
%  2nd develop std(v1)*std(v3), notice that it can be put as 
%  var(v1)+var(v2)-2*E[(v1 -mean(v1))(v2 -mean(v2))] = var(v1)+var(v2)
Y = [X(:,1) X(:,2) X(:,1) + X(:,2) - 1 ];

Ysort = sortrows(Y,1);

figure;plot(Ysort(:,1),Ysort(:,2),'bo');hold on;
plot(Ysort(:,1),Ysort(:,3),'ro'); hold off;


%  p(i,j) tests the hypothesis of no correlation. The greater p, the
%  greater the probability of no correlation.
[r p] = corrcoef(Ysort)

%  Nice Example of perfectly correlated rand var with null correlation
%  factor.
% The p value matrix is a good way to prove that some low R(i,j) values are 
%  not significant, p proves that a correlation (non linear) exists yet.
nTrials = 1000; N = 1000;
pVal = zeros(nTrials,1);
for i=1:nTrials
X = rand(N,1) - 1/2;
Y = [X(:) X(:).*X(:)];
[r p] = corrcoef(Y);
pVal(i) = p(1,2);
end

std(pVal)
mean(pVal)

% Ysort = sortrows(Y,1);


%  p(i,j) tests the hypothesis of no correlation. The greater p, the
%  greater the probability of no correlation.
[r p] = corrcoef(Ysort)



