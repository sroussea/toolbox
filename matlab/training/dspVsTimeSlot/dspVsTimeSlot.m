clc;close all;clear all;




N = 2048*2; T=.1; F = 1/T;
Fs = 4.7*F;
Ts=1/Fs;
timeSlot = N*Ts;
tVect = (0:N-1)*Ts;
F_axis = (0:N)*Fs/N;
F_axis_Sg = (0:N/2)*Fs/N;
A=2;
nbOfSig = 1;

% A=3.5; 
% harmonics = (A)*sin(2*pi*F*tVect) + (A)*sin(2*pi*F/2*tVect) ;
% 
% fft_harmo = fft(harmonics);
% absFFTharmo = abs(fft_harmo)/N;    % FFT Module in V 
% absFFTharmo_Sg = absFFTharmo(1:N/2+1);
% absFFTharmo_Sg(2:end) = 2*absFFTharmo_Sg(2:end);
% 
% % SR 22nd of May : 
%  Average power of an harmonic of amplitude A ( 2*A peak to Peak) is
%  0.5*A^2. Since absFFT_sg provides the amplitudes "A" of each harmonic, 
%  one should divide the sum of FFT coeff module square by 2.
%  Let's finally add that the variance of a signal of null mean is equal 
%  to its average power
% avPowFromFFT_Vrms = 0.5*sum((absFFTharmo_Sg).^2)
% avPowFromSig_Vrms = mean((harmonics).^2)

noiseRange = (1:round(size(tVect,2)/4));
% noiseRange = (1:N);
noisRgUpBound = size(noiseRange ,2);

% coeffHarmo = tVect;
coeffHarmo = 1;
harmonic1 = coeffHarmo.*((A/2)*sin(2*pi*F*tVect(1:N/2)));
harmonic2 = coeffHarmo.*((A/2)*sin(2*pi*F*tVect(N/2+1:end)));
raisetime=100*Ts;
pulseSig = A*10*tVect.*exp(-tVect/raisetime);

noiseVec1 = A*randn( N/2, nbOfSig );
noiseVec2 = A/2*randn( N/2, nbOfSig );
% fullSignal = repmat( harmonic' + pulseSig', 1, nbOfSig ) + noiseVec ;
% fullSignal = repmat( harmonic' , 1, nbOfSig ) + noiseVec ;
fullSignal = repmat( [harmonic1' ;harmonic2']  , 1, nbOfSig ) + [noiseVec1 ;noiseVec2 ] ;
figure;
plot(tVect, fullSignal ,'b');
title('fullSignal ');

% noiseVec = repmat( harmonic' , 1, nbOfSig ) ;

% tmp = repmat(noiseVec(1:N/4,:) , 4, 1);
% clear noiseVec;
% noiseVec = tmp;

% meanNoiseVec = mean( noiseVec,2 );
% figure;
% plot(tVect,meanNoiseVec ,'b');
% title('meanNoiseVec');
% figure;
% plot(tVect,noiseVec(:,1) ,'b');
% title('noiseVec');


%  Compute DSP of the noise which lastes all the signal long
% roiF=(400:600);
roiF=(1:size(F_axis_Sg,2));

fft_nois = fft(fullSignal);
absFFTNoise = abs(fft_nois)/N;    % FFT Module in V 
absFFTNoise_Sg(1:N/2+1,:)=absFFTNoise(1:N/2+1,:);
absFFTNoise_Sg(2:end,:)=2*absFFTNoise_Sg(2:end,:);
meanOfabsFFTNoise_Sg = mean(absFFTNoise_Sg,2);
figure;
plot(F_axis_Sg(roiF) , meanOfabsFFTNoise_Sg(roiF)  ,'b');
title('meanOfabsFFTNoise_Sg if noise lastes all over the signal long');
return;

%  Compute DSP of the noise which lastes all the signal long
noiseVecRestriction = [ noiseVec(noiseRange,:) ; zeros(N-noisRgUpBound,nbOfSig)];
meanNoiseVecRestriction = mean(noiseVecRestriction,2);

fft_noisRestrict = fft(noiseVecRestriction);
absFFTNoiseRestrict = abs(fft_noisRestrict)/N;    % FFT Module in V 
absFFTNoiseRestrict_Sg(1:N/2+1,:) = absFFTNoiseRestrict(1:N/2+1,:);
absFFTNoiseRestrict_Sg(2:end,:) = 2*absFFTNoiseRestrict_Sg(2:end,:);
meanOfabsFFTNoiseRestrict_Sg = mean(absFFTNoiseRestrict_Sg,2);
figure;
plot(F_axis_Sg , meanOfabsFFTNoiseRestrict_Sg  ,'b');
title(['meanOfabsFFTNoiseRestrict_Sg if noise lastes only ', ...
    num2str(noisRgUpBound), ' points long'] );

ifft_noisRestrict = ifft(mean(fft_noisRestrict,2));

figure;
p(:,1) = plot(tVect , meanNoiseVecRestriction  ,'b');
figure;
p(:,2) = plot(tVect , ifft_noisRestrict  ,'r');

return;
title('meanNoiseVecRestriction & FFT inverse of meanOfabsFFTNoiseRestrict');
legend(p(1,:),{'meanNoiseVecRestriction','FFT inverse of meanOfabsFFTNoiseRestrict'});

return;

fft_nois = fft(noiseVec);
absFFTNoise = abs(fft_nois)/N;    % FFT Module in V 
absFFTNoise_Sg(1:N/2+1,:)=absFFTNoise(1:N/2+1,:);
absFFTNoise_Sg(2:end,:)=2*absFFTNoise_Sg(2:end,:);

return;
% x(:,1) = A*sin(2*pi*F*tVect);
raisetime=30*Ts;
x(:,1) = A*10*tVect.*exp(-tVect/raisetime);

% x(:,2)=A*sin(2*pi*F/2*tVect);
x = x + noiseVec' ;
figure;
plot(tVect,x,'b');
title('Signal');

fft_x = fft(x);
absFFTX = abs(fft_x)/N;    % FFT Module in V 
absFFTX_Sg = absFFTX( 1:N/2+1 );
absFFTX_Sg(2:end) = 2*absFFTX_Sg( 2:end );
axis

figure;
plot(F_axis_Sg,absFFTNoise_Sg,'b');
title('absFFTNoise_Sg');
figure;
plot(F_axis_Sg,absFFTX_Sg ,'b');
title('absFFTX_Sg ');
return;

avgPowFft_f = sum(psd_V2)
averPower_t = mean(x.^2)

h = spectrum.periodogram;
hopts = psdopts(h,x(:,1))
set(hopts,'Fs',Fs,'SpectrumType','onesided');
f3=figure(3);
myPsd = psd(h,x(:,1),hopts)
avgPowPerio_f = avgpower(myPsd)
plot(myPsd)

return

sizeOfOnePgram = (N/M);



%x=randn(1,N);



xb=reshape(x,sizeOfOnePgram,M); % xb est une matrice de dimension (N/M,M).
%s=1/sizeOfOnePgram*abs(fft(xb)).^2;% pour obtenir la moyenne des M spectres.
myDsp=abs(fft(x)).^2;% pour obtenir la moyenne des M spectres.
%s=mean(1/N*abs(fft(xb)).^2,2);% pour obtenir la moyenne des M spectres.

pow_t=1/N*sum(x.^2);
pow_f=1/(N*N)*sum(s);
%pow_f=1/(sizeOfOnePgram)*sum(s);

f2=figure(2);
plot(F_axis,s,'bo');
return

 
return

pow_t=1/sizeOfOnePgram*sum(xb.^2)
pow_f=sum(s);

meanDsp = mean(s)
varDsp=var(s)

f1=figure(1);
plot([1:sizeOfOnePgram],x(1:sizeOfOnePgram),'b');

