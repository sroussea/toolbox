close all;clear all;clc;

A = [ 6 2 3; 3 7 3 ; 5 4 10];
vDiagA = diag(A);

% Jacobi

epsilon = 1e-3;
err = 2*epsilon;
Y = [1 1 1]';
Xk = round(randn(3,1) + 2);
% Xk = Y;
R = A - diag(vDiagA);
while ( err > epsilon )
  
  Xk = diag(diag(A).^-1)*( Y - R*Xk);
  Ytemp = A*Xk;
  err = sqrt(sum((Y-Ytemp).*(Y-Ytemp)));
end


disp('Jacobi sol :')
Xk
Ytemp