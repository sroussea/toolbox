clear all;close all

addpath('S:\svn\DSP\common\matlab')

N = 5000;
Ts = 0.2*1e-3; % .2 ms o sampling rate
Fs=1/Ts;
F1 = 1000;
myOmeg1 = F1*2*pi; A1 = 8;
myOmeg2 = (F1 + 4*Fs/N)*2*pi; A2 = A1;
myOmeg3 = (F1 + 12*Fs/N)*2*pi; A3 = A1;
myT = [0:N-1]*Ts;
mySig = A1*sin( myT*myOmeg1 ) + A2*sin( myT*myOmeg2)+ A3*sin( myT*myOmeg3);
%mySig = reshape( mySig , sectionLength, nbVect);  
[powSpectra F_axis_Sg] = powDensitySpectra( mySig', Ts, 1 );

% Zero padding : have to multiply the DSP by the factor length(mySig2) / length(mySig)
%mySig2 = horzcat(mySig, zeros(1,length(mySig)));
%[powSpectra2 F_axis_Sg] = powDensitySpectra( mySig2');




% Parseval : norme conservation between thedirect and inverse spaces
V2 = mySig.^2;
Vrms_2 = mean(mySig.^2)
meanOfDsp = sum(powSpectra)

% Relation between Vrms and Vpeak
% For a sinusoide, Vrms = int(V(t)^2,0,T) = Vpeak/sqrt(2)
idsNonZeroCoeffs = find(powSpectra > 7);
% Below the square of Vrms corresponding to harmo1 =  A1*sin( myT*myOmeg1) 
dsp_harmo1 = powSpectra(idsNonZeroCoeffs(1))
% To get the corresponding Vpeak
Vpeak = sqrt(dsp_harmo1*2)
