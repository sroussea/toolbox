clc;close all;clear all;

x=(0.001:0.0001:.01);
[X,Y] = meshgrid(x);

Fxy=1./Y/exp(1)-X./(Y.^2)/exp(1);

surf(X,Y,Fxy);