%% SR : 12th of August 2015
% Try to define the variance matrix to generate a proper multivariate gaussian 
% distribution

addpath S:\svn\DSP\common\matlab


clear all;close all;clc;

% Build covariance matrix of linearly corelated variables
N = 1000;
% Generate the first rand var
x = rand(N,2);

noise = rand(N,2);

% Mean matrix, each col for a 2D multivariate distribution
Mu = [1/2 10; 1/2 12];

% Generate linearly corelated variables
% Generate x uniformly distributed
muX = 2; x = 2*muX*rand(N,1);  % *2 since mean(rand(N,1)) = 1/2

y = 2*Mu(1,2)*noise(:,1) + noise(:,2)+3.5;
z = 2*Mu(2,2)*noise(:,1) + noise(:,2);
SIG1 = cov(x,y);
SIG2 = cov(x,z);

figure('name','Linearly corelated variables');
cGrpNames = cell(length(x), 2);
cGrpNames{:,1} = ' 
C(:) = {'String'};
gscatter([x;x],[y;z],[ones(length(x),1); zeros(length(x),1)],'br', 'ox',...
  'DisplayName',{'x','y'});
title([]);
X = [[x;x] [y;z]];
% X = [x y];

Sigma(:,:,1) = SIG1;
Sigma(:,:,2) = SIG2;
PComponents = [1/2,1/2];
S = struct('mu',Mu,'Sigma',Sigma,'PComponents',PComponents);

options = statset('Display','final');
gm = fitgmdist(X,2,'Options',options,'Start', S);
% gm = fitgmdist(X,1,'Options',options);

idx = cluster(gm,X);
cluster1 = (idx == 1);
cluster2 = (idx == 2);

figure('name','fitted data');hold on;
gscatter(X(:,1),X(:,2),idx,'br', 'ox');
% mark individuals missed by clustering procedure
[i f] = find(idx(1:N/2) == 1);
scatter( X(i,1), X(i,2), 'gs');
axisRanges = get(gca,{'XLim','YLim'});
legend('Cluster 1','Cluster 2','Location','NW');

gaussMixPdf = @(x,y)pdf(gm,[x y]);
ezcontour(gaussMixPdf,axisRanges{1},axisRanges{2},500 );
hold off;

figure;
% [x,y] = meshgrid(linspace(0,1,100),linspace(0,25,100));
ezmesh(gaussMixPdf,axisRanges{1},axisRanges{2},500 );

return;


mu1 = [0 0];
mu2 = [0 2];
V1 = sortrows(mvnrnd(mu1,SIG1,400), 1);
V2 = sortrows(mvnrnd(mu2,SIG2,400), 1);

X = [ V1; V2 ];
figure('name','original mutlivariate distributions');
gscatter(X(:,1),X(:,2),[ones(length(V1),1); zeros(length(V1),1)],'br', 'ox');

options = statset('Display','final');
gm = fitgmdist(X,2,'Options',options);

idx = cluster(gm,X);
cluster1 = (idx == 1);
cluster2 = (idx == 2);

figure('name','fitted data');hold on;
gscatter(X(:,1),X(:,2),idx,'br', 'ox');
% mark individuals missed by clustering procedure
[i f] = find(idx(1:400) == 1);
scatter(X(i,1),X(i,2),'gs');
axisRanges = get(gca,{'XLim','YLim'});
legend('Cluster 1','Cluster 2','Location','NW');


ezcontour(@(x,y)pdf(gm,[x y]),axisRanges{1},axisRanges{2},500 );
hold off;