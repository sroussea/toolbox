%% SR : 10th of July 2014
% Try gaussian mixture model matlab facilities

addpath S:\svn\DSP\common\matlab


clear all;close all;clc;
N = 100;

mu1 = [0 0];
sigXX = 100;
sigYY = sigXX;
sigXY = sigXX*.94;
% sigXY = sigXX*.10;
sigma1 = [sigXX sigXY; sigXY sigYY];
% mu2 = [0 15];
% sigma2 = sigma1;
sigma2 = [sigXX -sigXY; -sigXY sigYY];;
mu2 = [-15 15];
V1 = mvnrnd(mu1,sigma1,N);
V2 = mvnrnd(mu2,sigma2,N);

% dTeta = pi/4;
% rotMat = [ cos(dTeta) -sin(dTeta); sin(dTeta) cos(dTeta) ];
% V1 = V1*rotMat;
% V2 = V2*rotMat;

X = [ V1; V2 ];

figure;
scatter(X(1:length(V1),1),X(1:length(V1),2),10,'bo');hold on;
scatter(X(length(V1)+1:end,1),X(length(V1)+1:end,2),10,'r+');hold off;

options = statset('Display','final');
gm = fitgmdist(X,2,'Options',options);
figure;

idx = cluster(gm,X);
cluster1 = (idx == 1);
cluster2 = (idx == 2);

scatter(X(cluster1,1),X(cluster1,2),10,'r+');
hold on
scatter(X(cluster2,1),X(cluster2,2),10,'bo');
ezcontour(@(x,y)pdf(gm,[x y]),[-40 40],[-40 40]);
hold off
legend('Cluster 1','Cluster 2','Location','NW')


%% Perform CPA
xPop1 = V1(:,1); yPop1 = V1(:,2);
xPop2 = V2(:,1); yPop2 = V2(:,2);


figTitle = ['Var X distributions for each population'];
figure('name',figTitle,'NumberTitle','off'); 
hist(xPop1,30);hold on;
hist(xPop2,30);hold off;
title(figTitle); xlabel('var X');
h = findobj(gca,'Type','patch');
set(h(1),'FaceColor','r','EdgeColor','w','FaceAlpha', 0.5);
set(h(2),'FaceColor','g','EdgeColor','w' );
legend('population 1','population 2');

figTitle = ['Var Y distributions for each population'];
figure('name',figTitle,'NumberTitle','off'); 
hist(yPop1,30);hold on;
hist(yPop2,30);hold off;
title(figTitle); xlabel('var Y');
h = findobj(gca,'Type','patch');
set(h(1),'FaceColor','r','EdgeColor','w','FaceAlpha', 0.5);
set(h(2),'FaceColor','g','EdgeColor','w');
legend('population 1','population 2');

x = vertcat( xPop1, xPop2 );
y = vertcat( yPop1, yPop2 );

%  We only take the first 2 vars into account
X = horzcat(x,y);
redData = bsxfun(@minus,X,mean(X));
redData = bsxfun(@rdivide,redData,sqrt(var(redData,0)));
[pc,score,latent,tsquare] = princomp(redData);
varLab = {'x','y'};
% figure;plotPCA(varLab, pc, score, latent);

Xpop1 = horzcat(xPop1,yPop1);
redXpop1 = bsxfun(@rdivide,Xpop1,sqrt(var(Xpop1,0)));
scoreRedPop1 = redXpop1*pc;
Xpop2 = horzcat(xPop2,yPop2);
redXpop2 = bsxfun(@rdivide,Xpop2,sqrt(var(Xpop2,0)));
scoreRedPop2 = redXpop2*pc;

figTitle = ['Composit Var1 distributions for each population'];
figure('name',figTitle,'NumberTitle','off'); 
hist(scoreRedPop1(:,1),30);hold on;
hist(scoreRedPop2(:,1),30);hold off;
title(figTitle); xlabel('Composit Var1 ');
h = findobj(gca,'Type','patch');
set(h(1),'FaceColor','r','EdgeColor','w','FaceAlpha', 0.5);
set(h(2),'FaceColor','g','EdgeColor','w');
legend('population 1','population 2');


figTitle = ['Composit Var2 distributions for each population'];
figure('name',figTitle,'NumberTitle','off'); 
hist(scoreRedPop1(:,2),30);hold on;
hist(scoreRedPop2(:,2),30);hold off;
title(figTitle); xlabel('Composit Var1 ');
h = findobj(gca,'Type','patch');
set(h(1),'FaceColor','r','EdgeColor','w','FaceAlpha', 0.5);
set(h(2),'FaceColor','g','EdgeColor','w' );
legend('population 1','population 2');

