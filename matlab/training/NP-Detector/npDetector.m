%% SR : 20th of October 2015
% Implementation of the Neyman Pearson detector for :
%  - a DC known level 
%  - a Gaussian noise with known power
% 
% 



clear all;close all;clc;

% Sample size
N = 30;
% Gaussian distribution parameters
mu = 0 ; sig = 1;
% DC level
dcLevel = 2;
% number of polls
pollsNumber = 100;
% Monte Carlo size
monteSize = 100;

% Neyman Pearson parameters 
% test level
alfa = 0.05;


%% From manufacturer point of view
% H0 : no signal ; H1 :signal present
%  test threshold
testThresh = sig*icdf('Normal', alfa , mu, sig)/sqrt(N);

% Set hypothesis 
bH0ActuallyTrue = 1;

errRate    = zeros(monteSize, 1);

% Store the actually true hypothesis
if ( bH0ActuallyTrue )
  errType = 'Type I';
else
  errType = 'Type II';
end
figTitErrTypeRate = [' Error type ' errType ' rate distribution for a test size of ' num2str(alfa)];

  parfor iMonte = 1:monteSize
    
    % Generate the signal
    noise = sig*randn( N, pollsNumber );
    if ( bH0ActuallyTrue )
      mySignal = noise;
    else
      mySignal = dcLevel + noise;
    end
    % statistics : EMV <=> empirical mean
    emvStat = mean(mySignal);

    %  Test computation 
    testResult = emvStat > testThresh;

    if ( bH0ActuallyTrue )
      errRate( iMonte )  = sum(~testResult )/pollsNumber; % err Type I
    else
      errRate( iMonte )  = sum(testResult )/pollsNumber;  % err Type II
    end

  end

% tit_ = 'Mean error rates';
% figure('name',tit_,'NumberTitle','off');
% p(:,1)= plot(lambdRange, manuAvErrRate, 'r'); hold on;
% p(:,2)= plot(lambdRange, dealAvErrRate, 'b');  
% title(tit_); xlabel('lambda');
% legend(p(1,:),{['Manufacturer mean error rates'],...
% ['Dealer mean error rates']},'Location','NorthEast');
% hold off;


figure('name',figTitErrTypeRate,'NumberTitle','off'); 
hist(errRate,10)
% ;hold on;
% hist(xPop2,30);hold off;
title(figTitErrTypeRate); xlabel(manuErrType);
h = findobj(gca,'Type','patch');
set(h(1),'FaceColor','r','EdgeColor','w','FaceAlpha', 0.5);
% set(h(2),'FaceColor','g','EdgeColor','w' );
% legend('population 1','population 2');


figure('name',figTitDealErrTypeRate,'NumberTitle','off'); 
hist(dealErrRate,10)
title(figTitDealErrTypeRate); xlabel(dealErrType);
h = findobj(gca,'Type','patch');
set(h(1),'FaceColor','r','EdgeColor','w','FaceAlpha', 0.5);

figTitle = ['Dealer  / manufacturer agreement rates for a test size of ' num2str(alfa)];
figure('name',figTitle,'NumberTitle','off'); 
hist(AgreementRates,10)
title(figTitle); xlabel('Dealer  / manufacturer agreement rates');
h = findobj(gca,'Type','patch');
set(h(1),'FaceColor','r','EdgeColor','w','FaceAlpha', 0.5);

