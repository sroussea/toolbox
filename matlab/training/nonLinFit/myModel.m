function [yTheo]=myModel( beta , x )

global iRef;
tst = interp1(x-beta,iRef,x);
divergentIds = ~isfinite(tst);
tst( divergentIds ) = iRef(divergentIds);

yTheo = tst;

% yTheo = beta./2;

% yTheo = (x-beta).^2;

% p1 =  4.275e-6;
% p2 =  -0.0001782;
% p3 =    0.001747; 
% p4 =      0.0213;
% yTheo = p1*(x-beta).^3 + p2*(x-beta).^2 + p3*(x-beta) + p4;

end