close all;clear all;clc;

%% little training
% xRange=[0:.1:5];
% expY = (xRange-5/2).^2 + 1.*randn(1,size(xRange,2));
% 
% plot(xRange,expY);
% 
% beta0 = 1;
% 
% [beta,R,J,CovB,MSE] = nlinfit(xRange,expY,@myModel,beta0)
% betaStdErr = sqrt(diag(CovB))


%% Concrete try on real data
addpath E:\tmp\GaspardData\matlabRdy\cuts
addpath S:\svn\DSP\Analysis\matlab\myLib
addpath S:\svn\DSP\Analysis\training\matlab\MIToolbox

headr=zeros(1,2);
% fid = fopen('run_0150.dat.10Oct12_11h05m10s_12562Tks_2560Pts_00_01_CutDiscri.bin');
fid = fopen('run_0151.dat.10Oct12_11h29m00s._96024Tks_2560Pts_00_01_pCut.bin');


headr = fread(fid, size(headr), '*ushort') ;
nbOfpts=headr(1); nbOfTks=headr(2);

tksI = double( fread(fid, [nbOfpts, nbOfTks], '*ushort') );
tksQ = double( fread(fid, [nbOfpts, nbOfTks], '*ushort') );

fclose(fid);

% Remove noise
tksIMA = movAver( tksI, 20); clear tksI;
tksQMA = movAver( tksQ, 20); clear tksQ;
figure(1); plot(tksIMA(300:450,:));title('tksI');

figure(2);
Qmax=max(tksQMA);
Imax=max(tksIMA);
plot(Qmax,Imax,'o','MarkerSize',3 );

%% Keep a very narrow (Q,Imax) ROI
myCut = find( Qmax > 270 & Qmax < 410 & Imax > 300 & Imax < 505 );

figure(3);hold on;
plot(Qmax,Imax,'bo','MarkerSize',1 );
plot(Qmax(myCut),Imax(myCut),'ro','MarkerSize',5);hold off;
 
% Keep only ROI
roiT=(300:500);
% Itk(:,:) = tksIMA(roiT,:);

Itk(:,:) = tksIMA(roiT,myCut);

clear tksIMA;

%% Curve synchro
% Do the linear fit to compute time shift
ft_ = fittype('poly1');
for i = 1:size(Itk,2)

  [maxI maxID] = max( Itk( :, i ) );
  dataSetId = find ( Itk(1:maxID,i) > .1 * maxI & Itk(1:maxID,i) < .8 * maxI );
 
%   plot(dataSetId,Itk(dataSetId),'bo');

  [fitRes gof] = fit(dataSetId,Itk(dataSetId,i),ft_);

  xRangeInter = (dataSetId(1)-5:dataSetId(end) );
  interpolI =fitRes.p1.*xRangeInter +fitRes.p2;
%   plot(xRangeInter ,interpolI,'r');
  
  [val iDs] = find(interpolI<0);
  startID(i) = xRangeInter(iDs(end));

end
hold off,iDs,val;

% plot result
tRangeMat = repmat( roiT' , 1, size(Itk,2) ) - repmat( startID, size(Itk,1), 1);
% figure(4); plot(tRangeMat,Itk);

%% create sync and normalized curves
for iFit = 1:size(Itk,2)
  
  INorm(:,iFit) = Itk(:,iFit)/sum(Itk(:,iFit));
  Isync1(:,iFit) = [Itk(startID(iFit):end,iFit); zeros(startID(iFit)-1,1 ) ];
  IsyncNorm(:,iFit) = Isync1(:,iFit)/sum(Isync1(:,iFit));
  
end
 figure(4);plot(INorm(40:110,:));title('Curves normalized before resync')
clear INorm,Itk;


%% Check for the time shift to make curves perfectly synchro
% [c d]=max(IsyncNormCut) => indexes returned into d give the same result
% as find(round(betaVec)~=0) with greater performances !

% Find ids of the max of each curve
[val iDsOfMax]=max(Isync1);
% choose the most frequent max ID and select it as the reference
[cumul values] = hist(iDsOfMax);
[val id] = max(cumul);
idMaxRef = round(values(id));


% just for monitoring purpose
averMaxIndex = round(mean(iDsOfMax));
IMaxRoi = (averMaxIndex-17:averMaxIndex+18);
% Isync1MaxRoi = Isync1(IMaxRoi,:);
Isync1MaxRoi = Isync1(:,:);

IsyncNormCut = IsyncNorm(IMaxRoi,:);
figure(5); plot(IsyncNormCut);title('Curves after Sync1 & norm');

% Select the reference signal
refIds = find( iDsOfMax == idMaxRef );
global iRef;
iRef = IsyncNormCut(:,refIds(1));

for iBeta=2:size(IsyncNormCut,2)
  
  iExp = IsyncNormCut(:,iBeta);

  beta0 = 0.1;
  xRange = (1:size(IsyncNormCut(:,iBeta),1));
  [beta,R,J,CovB,MSE] = nlinfit(xRange,iExp',@myModel,beta0);
  betaStdErr = sqrt(diag(CovB));
  betaVec(iBeta) = beta;
end;
clear Isync1MaxRoi;



curveIDToShift = find( iDsOfMax ~= idMaxRef );
for iID = 1:size(curveIDToShift,2)
  curID = curveIDToShift( iID );
  iShift = iDsOfMax( curID  ) - idMaxRef;
  startID(curID)= startID(curID) + iShift;
end

for iFit = 1:size(Itk,2)
  
  Isync1(:,iFit) = [Itk(startID(iFit):end,iFit); zeros(startID(iFit)-1,1 ) ];
  IsyncNorm(:,iFit) = Isync1(:,iFit)/sum(Isync1(:,iFit));
  
end


figure(6); plot(IsyncNorm(IMaxRoi,:));title('Curves after Sync 2 & norm');

figure(7)
plot ( Isync1(:,:) );  

% plot (xRange,iRef,'b',xRange, iExp,'r'); hold on;
% plot( xRange , myModel(beta,xRange),'ro');
