clc;close all;clear all;

N=1000;
Fs=1e3;
Ts=1/Fs;
timeSlot = N*Ts;
tVect = (0:N-1)*Ts;
F_axis = (0:N-1)*Fs/N;
F_axis_Single = (0:N/2-1)*Fs/N;

sig=2*sin(2*pi*Fs/500*tVect)';
noise=randn(size(sig),1);
sigNoised=sig+ noise

fftNoise_Db_V=fft(noise)/N;   % FFT double sided coeff in Vpeak / 2
fftSig_Db_V=fft(sig)/N;   % FFT double sided coeff in Vpeak / 2
fftSigNoised_Db_V=fft(sigNoised)/N;   % FFT double sided coeff in Vpeak / 2

difFFT=fftSigNoised_Db_V-fftNoise_Db_V;

ifftSig_Db_V=ifft(fftSig_Db_V*N);
ifftDiffFFT=ifft(difFFT*N);

fftNoise_Sg_V=fftNoise_Db_V(1:N/2,:);   % FFT single sided coeff in Vpeak 
fftNoise_Sg_V(2:end,:)=2*fftNoise_Sg_V(2:end,:);

fftSig_Sg_V=fftSig_Db_V(1:N/2,:);   % FFT single sided coeff in Vpeak 
fftSig_Sg_V(2:end,:)=2*fftSig_Sg_V(2:end,:);

absFftNoise_Sg_V=abs(fftNoise_Sg_V);  % FFT Module in QPk
absFftSig_Sg_V=abs(fftSig_Sg_V);  % FFT Module in QPk



f1=figure(1);
plot(F_axis_Single,absFftNoise_Sg_V,'b');
title('absFFT noise in Q peak');

figure(2);
plot(F_axis_Single,absFftSig_Sg_V,'b');
title('absFFT signal in Q peak');

figure(3);
plot(tVect,ifftSig_Db_V,'b');
title('iFftSig signal in Q peak');

figure(4);
plot(tVect,ifftDiffFFT,'b');
title('ifftDiffFFT signal in Q peak');


return;
