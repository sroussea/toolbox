load carbig
X = [Horsepower,Weight];
y = MPG;

function [y]=modelfun(b,x)
y=b(1) + b(2)*x(:,1).^b(3) + ...
    b(4)*x(:,2).^b(5);
end;

beta0 = [-50 500 -1 500 -1];
mdl = fitnlm(X,y,modelfun,beta0)