clc;close all;clear all;
% Change default axes fonts.
set(0,'DefaultAxesFontName', 'Times New Roman')
set(0,'DefaultAxesFontSize', 16)

% Change default text fonts.
set(0,'DefaultTextFontname', 'Times New Roman')
set(0,'DefaultTextFontSize', 16)

addpath D:\tmp\GaspardData\matlabRdy\cuts
addpath S:\svn\DSP\common\matlab
addpath S:\svn\DSP\Analysis\matlab\noiseStudy1\res

%  addpath /vol0/rousseau/locData/run_2012_10/cuts
%  addpath /vol0/rousseau/curAn/common/matlab
%  addpath /vol0/rousseau/curAn/matlab/noiseStudy1/res/


% ===========> TO FIT
fileName = 'run_0157.dat.11Oct12_02h51m06s._307905Tks_2560Pts_00_01_300V_MultiM1.bin';

tic;


% ===========> TO FIT
%% Perform  selection into (E-m) graph
OneMev300V = round(1/1.4999e-2);
% Read curves from file
[tksIRaw tksQRaw Imax, Qmax ] = readCurves( fileName );

% % SR 31st ofMay : cannot go down 500 since signal must softly fade away to 0
roiT=(290:550);
% roiT=(300:2500);

% remove null tracks
[xxx id] = find( max(abs(tksIRaw(roiT,:))) < 20);
validIds = (1:size(tksIRaw,2));
validIds=validIds(setdiff(1:length(validIds),id));
tksIRaw = tksIRaw(:,validIds);
tksQRaw = tksQRaw(:,validIds);
Imax = Imax(validIds);
Qmax = Qmax(validIds);

%sort tracks by ascendent energy
energSortedIds = sortrows([Qmax',(1:length(Qmax))']);
tksIRaw = tksIRaw(:,energSortedIds(:,2));
tksQRaw = tksQRaw(:,energSortedIds(:,2));
Imax = Imax(energSortedIds(:,2));
Qmax = Qmax(energSortedIds(:,2));


% Create a cut to select a particular energy range
minQmax =  min(Qmax); maxQmax = max(Qmax);
dE = (maxQmax - minQmax )/length(Qmax)*100;
binsQmax = (0 : round((maxQmax-minQmax)/dE));

nBin = 1;
myCut300V_2 = find( Qmax >= minQmax+dE*binsQmax(nBin) & Qmax < minQmax+dE*binsQmax(end));
%  myCut300V_2 = find( Qmax >= minQmax+dE*binsQmax(nBin) & Qmax < minQmax+dE*binsQmax(nBin+1));
% myCut300V_2 = 1;
myCut = myCut300V_2;

E300V = Qmax.*1.4999e-2 -4.96922e-1;

Ecur = E300V(myCut);


% OneMev275V = round(1/1.4e-2);
% QRange275V_1 = [150,150+OneMev275V];
% QRange275V_2 = [469-OneMev275V, 469];
% [ Imax, Qmax, myCut275V_1 ] = biDimCut( fileName, QRange275V_1 );
% [ Imax, Qmax, myCut275V_2 ] = biDimCut( fileName, QRange275V_2 );
% % myCut = myCut275V_2;
% myCut = myCut275V_1;
% E275V = Qmax.*1.4e-2 + 3.0e-2;
% Ecur = E275V;


% OneMev250V = round(1/1.4e-2);
% QRange250V_1 = [154,154+OneMev250V];
% QRange250V_2 = [471-OneMev250V, 471];
% [ Imax, Qmax, myCut250V_1 ] = biDimCut( fileName, QRange250V_1 );
% [ Imax, Qmax, myCut250V_2 ] = biDimCut( fileName, QRange250V_2 );
% E250V = Qmax.*1.4e-2 + 3.0e-2;
% myCut = myCut250V_1;
% myCut = myCut250V_2;
% E250V = Qmax.*1.4e-2 + 3.0e-2;
% Ecur = E250V;

figure; 
h=plot(E300V,Imax,'o','MarkerSize',1);
hold on;
plot(E300V(myCut),Imax(myCut),'ro','MarkerSize',3);
title(['Sélection de ',num2str(size(myCut,2)), ' signaux dans la bande d''énergie [',...
  num2str(min(Ecur),3),'-',num2str(max(Ecur),3),'] Mev']);
ylabel('Amplitude maximum du signal de courant en LSB');xlabel('Energie en Mev');
hold off;

%myCut=myCut([1:30]);

% keep only E selected tracks in memory
tksIRaw = tksIRaw(:,myCut);

% select only region of interest 
Itk(:,:) = tksIRaw(roiT,:);
figure;
 plot(Itk,'b'); 

% figure;hold on;
ft_ = fittype('poly1');
parfor i = 1:size(Itk,2)

  [maxI maxID] = max( Itk( :, i ) );
  
  alf = .5;
  dataSetId = find ( Itk(1:maxID,i) > .1 * maxI & Itk(1:maxID,i) < alf * maxI );
 % if rising edge skewness is very steep, we enlarge the data set
 while ( (length(dataSetId) < 3) & ( alf <= 1 ) )
  alf = alf+.1;
  dataSetId = find ( Itk(1:maxID,i) > .1 * maxI & Itk(1:maxID,i) < alf * maxI );
 end   
  
%   plot(dataSetId,Itk(dataSetId,i),'bo');

  [fitRes gof] = fit(dataSetId,Itk(dataSetId,i),ft_);

%  SR 7th of November : dataSetId(1)-10 : 10 to make linear interpolation
%  cross the abscisse axe
  xRangeInter = (dataSetId(1)-10:dataSetId(end) );
  interpolI =fitRes.p1.*xRangeInter +fitRes.p2;
  plot(xRangeInter ,interpolI,'r');
  
  [row iDs] = find(interpolI<0);
  
  startID(i) = xRangeInter(iDs(end));

%% 9th of January 2014: seems to deteriorate the fit quality !!!
%% Has to be understood
%   newStartID = startID(i);
%   if Itk( startID(i) , i ) > 5
%     pulseAtStart = Itk( newStartID ,i );
%     while ( pulseAtStart > 5 )
%       newStartID = newStartID - 1;
%       pulseAtStart = Itk( newStartID ,i );
%     end
%     startID(i) = newStartID;
%   end
  
end
% hold off;

figure;hold on;
roiStart = 0;
xc1 = (roiStart:length(roiT)-1+roiStart)';

% Good results with myModelFunc1Splitted  & myCut300V_1
% start :  [ tau   tau1   tau2  t1      t2     a       b ]
cellFitInput = {... 
 ['tau    ' 'tau1    ' 'tau2    ' '   t1  ' '   t2  ' '     a   ' '     b  '];
 [16          1        -15          5        18        150       5];...
 [10       .015      -150         5.01       5         1       .1];...
 [30          16        -10        40        40        600      600]
 };

parfor iFit = 1:size(Itk,2)
% for iFit = 1:size(Itk,2)
  
  
  tmp = tksIRaw( roiT,iFit);
  tksIExp(:,iFit)=[tmp(startID(iFit)+roiStart:end); zeros(startID(iFit)-1+roiStart,1 ) ];  
  
  plot(xc1,tksIExp(:,iFit),'r');

  % Fit this model using new data

  beta0 = [16          1        -15          5        18        150       5];
 mdl = fitnlm(xc1,tksIExp(:,iFit) , @myModelFunc1Splitted,beta0);
 
 theoC(:,iFit) = zeros( length(xc1), 1);
   
  rSq(iFit)  = mdl.Rsquared.Ordinary ;
  tau(iFit)  = mdl.Coefficients.Estimate(1);
  tau1(iFit) = mdl.Coefficients.Estimate(2);
  tau2(iFit) = mdl.Coefficients.Estimate(3);
  t1(iFit)   = mdl.Coefficients.Estimate(4);
  t2(iFit)   = mdl.Coefficients.Estimate(5);
  a(iFit)    = mdl.Coefficients.Estimate(6);
  b(iFit)    = mdl.Coefficients.Estimate(7);
  
                             
  theoC(:,iFit) = mdl.predict;

  
  plot(xc1,theoC(:,iFit),'b');  
 
  
end

%b = a./3;

% title('theoC vs exp curves');hold off;
figTheoVsExp = figure('name','curves theo vs Exp','NumberTitle','off'); 
 plot(xc1,tksIExp,'r');hold on;
 plot(xc1,theoC,'b');  hold off;
title('Mod�le Vs mesures bande basse d''�nergie � 300V');
ylabel('Amplitude en LSB');xlabel('temps en ns');

figTau2 = figure('name','tau2','NumberTitle','off');hist(tau2,100);title('tau2') 
figTau1 = figure('name','tau1','NumberTitle','off');hist(tau1,100);title('tau1') 
figTau = figure('name','tau','NumberTitle','off');hist(tau,100);title('tau') 
figA = figure('name','a','NumberTitle','off');hist(a,100);title('a') 
figB = figure('name','b','NumberTitle','off');hist(b,100);title('b') 
figT1 = figure('name','t1','NumberTitle','off');hist(t1,100);title('t1') 
figT2 = figure('name','t2','NumberTitle','off');hist(t2,100);title('t2') 

figMosaic1 = figs2subplots([figTau1 figT1 figA figTau2 figT2 figB ],[2 3] )
set(figMosaic1 ,'name','Mosaic 1 : Fit parameters','NumberTitle','off');

figRsq =figure('name','Determination Coeff','NumberTitle','off');
[rSqBins rSqPop] = hist(rSq,300);hist(rSq,300);xlim([0.98 1]);
title({['Coefficients de d�termination des r�gressions des ',...
  num2str(length(myCut))],' signaux de la bande haute d''�nergie'});
ylabel('Coefficients de d�termination');xlabel('nombre de signaux');


difTheoExp = (tksIExp(:,:) - theoC(:,:));

meandifTheoExp = mean(difTheoExp,1 );
stdDifTheoExp  = std(difTheoExp,1 );
% figure;
% plot(meandifTheoExp  );title('meandifTheoExp ');
% figure;
% plot(vardifTheoExp  );title('vardifTheoExp  ');

% 300V_2 .9993 =>  71 tks
% [vals ids ]=find(rSq >.9993 )
% 300V_1 .995 =>  99 tks
% [sortRSq iDsRSq]= sort(rSq,'descend')
% propOfBest = 1;
% ids = iDsRSq(1:round(propOfBest*length(iDsRSq)));
ids= (1:length(rSq));
% 275V_2 .999 => 99 tks
% [vals ids ]=find(rSq >.999 )
% 275V_1 .975 =>   99 tks
% [vals ids ]=find(rSq >.975 );
% 250V_1 .979 =>   61 tks
% [vals ids ]=find(rSq >=.979 );
% 250V_2 .97 =>  all tks => 266
% 250V_2 .999 =>  100 tks
% 250V_2 .9991 =>  66 tks
% [vals ids ]=find(rSq >.9991 );

N = size(difTheoExp,1);
Fs = 1e9;
Ts=1/Fs;
F_axis = (0:N)*Fs/N;
F_axis_Sg = (0:N/2)*Fs/N;


figDifTheoExp = figure('name','Diff Theo-Exp','NumberTitle','off');
roiT=(1:N-50);
plot(roiT,difTheoExp(roiT,ids) ); 
% title('Différence entre modèle et mesures bande haute d''énergie à 300V');
title('Diff�rence entre mod�le et mesures bande basse d''�nergie � 300V');
ylabel('Amplitude en LSB');xlabel('temps en ns');
xlim([1 roiT(end)]);

figMosaic2 = figs2subplots([figTau figTheoVsExp figRsq figDifTheoExp ],[2 3] )
set(figMosaic2 , 'name','Mosaic 2 : DSP, Détermination...','NumberTitle','off');

 
figT1vsE=figure('name','t1 vs E','NumberTitle','off'); plot(Ecur,t1,'bo');title('t1 vs E');
figT2vsE=figure('name','t2 vs E','NumberTitle','off'); plot(Ecur,t2,'bo');title('t2 vs E');
figTauVsE=figure('name','tau vs E','NumberTitle','off'); plot(Ecur,tau,'bo');title('tau vs E');
figTau1VsE=figure('name','tau1 vs E','NumberTitle','off'); plot(Ecur,tau1,'bo');title('tau1 vs E');
figTau2VsE=figure('name','tau2 vs E','NumberTitle','off'); plot(Ecur,tau2,'bo');title('tau2 vs E');
figAvsE=figure('name','a vs E','NumberTitle','off'); plot(Ecur,a,'bo');title('a vs E');
figBvsE=figure('name','b vs E','NumberTitle','off'); plot(Ecur,b,'bo');title('Amplitude du courant d''ions à t=0 vs E');
ylabel('Amplitude du courant d''ions à t=0');xlabel('Energie en MeV');

figR2vsE=figure('name','R² vs E','NumberTitle','off'); plot(Ecur,rSq,'bo');title('R² vs E');
ylabel('Coefficients de détermination R²');xlabel('Energie en MeV');

figMosaic3 = figs2subplots([figT1vsE figAvsE figT2vsE figBvsE],[2 2] );
set(figMosaic3 , 'name','Mosaic 3 : t1,t2,a,b vs E','NumberTitle','off');
figMosaic4 = figs2subplots([figTauVsE figTau1VsE figTau2VsE figR2vsE],[2 2] );
set(figMosaic4 , 'name','Mosaic 4 : tau,tau1,tau2,R² vs E','NumberTitle','off');

cellFig = {figMosaic1, figMosaic2, figMosaic3, figMosaic4};
writeResToFile( tau, tau1, tau2, t1, t2, a, b,...
    stdDifTheoExp, rSq, cellFitInput, cellFig);

ovrHead=toc

return;
  f3=figure;hold on;
for iTrack = 1:size( difTheoExp,2)
  
  h = spectrum.periodogram('Blackman-Harris');
%   h = spectrum.periodogram;
  hopts = psdopts(h)
  set(hopts,'Fs',Fs,'SpectrumType','onesided');
  myPsd = psd(h,difTheoExp(:,iTrack),hopts)
  avgPowPerio_f(iTrack) = avgpower(myPsd)
%   plot(myPsd.Frequencies(40:end),10.^(myPsd.data(40:end)/10))
%   plot(myPsd.Frequencies(40:end),10*log10(myPsd.data(40:end)))
  myPsdData(:,iTrack) = myPsd.data;

end
hold off;

QmaxTheo = 2*a./b.^3;
ImaxTheo = a.*((2./(b*exp(1)).^2));
%  figure(7);plot(2*a./b.^3,a.*((2./(b*exp(1)).^2)),'bo');
figure(8); plot(QmaxTheo/ mean(QmaxTheo./Qmax),ImaxTheo/mean(ImaxTheo./Imax),'ro',Qmax,Imax,'bo')

figure(9);
 m2=moment(Isync,2);
 plot(Qmax,m2,'ro');
 
 
