clc;close all;clear all;

N=700;
nTracks=1000;
% fid = fopen('run_0151.dat.10Oct12_11h29m00s_10k.bin');
%fid = fopen('run_0150.dat.10Oct12_11h05m10s_2k_t10.bin');
fid = fopen('tracks.bin');


% Read tracks : sort by column : track_i = tracks(:,i)
tracks_Q = double(fread(fid, [N, nTracks], '*uint16'));
fclose(fid);



% Q stands for magnitude in ADC quantum unit
% BL stands for Base Line suppresssed
tracks_QBL = tracks_Q; 

%remove base line
ped=mean(tracks_Q(1:300,:));
tracks_QBL = tracks_Q-repmat(ped,size(tracks_Q,1),1);

% plot(tracks_QBL )

[max,id]=max(tracks_QBL);
listMax=[max ; id]';
InterestCurvesIds = find(listMax(:,1)>950 & listMax(:,1)<1000);
InterestTks= tracks_QBL(:,InterestCurvesIds);
 figure(1);plot(InterestTks);


%  B=corrcoef([InterestTks(:,1)  tracks_QBL])
%  [R,P]=corrcoef(tracks_QBL(:,1:20))
%  [i j] = find(B(:,1)>.99);
%  figure(2); plot(  tracks_QBL(:,i(1:end-1)));
%  return
 

 
 return




Fs=1e9;
Ts=1/Fs;
timeSlot = N*Ts;
tVect = (0:N-1)*Ts;
F_axis = (0:N-1)*Fs/N;
F_axis_Single = (0:N/2-1)*Fs/N;

detX=2*sin(2*pi*Fs/10*3.5*tVect)';
% detMat=repmat(detX,1,10);
detMat=detX;
tracks_QBL(:,1) = tracks_QBL(:,1) + detMat;


% !!!!!!!!!!!!!!!!!!!! UNITS EXPLANATION
% Modulus of coeffs returned by the FFT function is in N/2*A where A is 
% peak magnitude of the signal 
% By default, FFT is doubled sided and it consists of negative Freqs + DC +
% positive freqs, thats why FFT double sided coeff magnitude of each 
% harmonic is divided by 2. 
% Concerning the N, it comes from the definition of the FFT which is a
% sum in DFT over the N signal samples (an integral in TF).
% If we want to get coeff in harmonic peak magnitude (VPk), then multiply 
% FFT mudulus coeff by 2/N.
% Since rms(Sin(t)) = 1/sqrt(2), if we divide FFT coeff in VPk by sqrt(2),
% we obtain Vrms

% Concerning PSD, the original formula tells :
% PSD = (abs(FFT)^2)/N where abs(z) perform modulus of the complex number z
% Then you obtain a PSD which depend linearly with N in unit of N/4*VPk^2

% To get PSD in VPk^2 , then take the modulus of the double sided FFT 
% coeffs given in VPk/2 and do the square : DSP(VPk^2/Hz) = abs(FFT).^2

% To get PSD in Vrms^2 , just take the single sided PSD by multiplying
% positive double sided coeffs by 2 (except the DC) 


% FFT is done column per column, so FFT of track (:,1) is in FFT(:,1)
% Be carefull : tracks unit is in DAC quantum, we call it Q (=125�V)
FFT_Db_V=fft(tracks_QBL)/N;   % FFT double sided coeff in Vpeak / 2
FFT_Db_Vrms=FFT_Db_V/sqrt(2); % FFT double sided coeff in Vrms


FFT_Sg_V=FFT_Db_V(1:N/2,:);   % FFT single sided coeff in Vpeak 
FFT_Sg_V(2:end,:)=2*FFT_Sg_V(2:end,:);

absFFT_VPk=abs(FFT_Sg_V);  % FFT Module in QPk
absFFT_Vrms=abs(absFFT_VPk)/sqrt(2);  % FFT Module in Qrms


psd_VPk2=abs(FFT_Db_V).^2;  % Power spectrum density in Q^2
psd_V2rms(:,:)=psd_VPk2(1:N/2,:);
psd_V2rms(2:end,:) = 2*psd_V2rms(2:end,:);

df=Fs/N;      % Frequency resolution
psd_V2rmsPerHz=1/df*psd_V2rms; % PSD in Q^2/Hz

f1=figure(1);
plot(F_axis_Single,absFFT_VPk,'b');
title('absFFT in Q peak');

f2=figure(2);
plot(F_axis_Single,absFFT_Vrms,'b');
title('absFFT in Q rms');

f3=figure(3);
plot(F_axis_Single,10*log10(psd_V2rms),'b');
title('psd Db');

f4=figure(4);
plot(F_axis_Single,psd_V2rmsPerHz,'b');
title('psd in Qrms^2 Per Hz');


avgPowFft_f = sum(psd_VPk2)
averPower_t = mean( tracks_QBL.^2 )
figure5=figure(5)
hist(averPower_t);title('Average Power in Qpeak^2');
