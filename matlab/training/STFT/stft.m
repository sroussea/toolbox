% Copyleft (C) Forrest Sheng Bao 1984-2009 http://fsbao.net
% with Dept. of Electrical and Computer Engineering 
%and Dept. of Computer Science, 
%Texas Tech University, Lubbock, Texas, USA
%
% This program shows the STFT result using Hamming and Chebychev window under different window length.
%
% This program licensed in GNU GENERAL PUBLIC LICENSE (GPL) v3 or later.
% If you do not know what GNU GPL is, please visit http://www.gnu.org/copyleft/gpl.html

% Example to generate a sine wave with a linear change in frequency
%   Evaluate the time-frequency charactoristic using the STFT
% 	Sine wave should vary between 10 and 200 Hz over a  sec period
% Assume a sample rate of 500 Hz
%%
clear all; close all;
%  Constants
N = 500;				% Number of points
fs = 500;			    % Sample freq;
f1 = 10;				% Minimum frequency
f2 = 200;			    % Maximum frequency

colormap('gray');
t = (1:N)/fs;
%Generate chirp signal (i.e., linear change in freq)
fc = ((1:N)*((f2-f1)/N)) + f1;
x = sin(pi*t.*fc);
plot(t,x,'k');
xlabel('Time (sec)','FontSize',14);
ylabel('\it{x(t)}','FontSize',14);
axis([0 1 -1.25 1.25]);

for nfft = [128 64 32] % Window size

    % Compute spectrogram in Hamming window, 50% overlap
    [B,f,t] = spectrogram(x,nfft,[],nfft,fs);

    figure;
        mesh(t,f,abs(B));
        xlabel('Time (sec)','FontSize',14);
        ylabel('Frequency (Hz)','FontSize',14);
        axis([0 1 0 250 0 12]);
        colormap('gray'); caxis([0 20]);
        title(['Window size = ' num2str(nfft) ' Window type= Hamming' ]);
    figure;
        contour(t,f,abs(B));
        xlabel('Time (sec)','FontSize',14);
        ylabel('Frequency (Hz)','FontSize',14);
        axis([0 1 0 250]);
        colormap('gray'); caxis([0 40]);
        title(['Window size = ' num2str(nfft) ' Window type= Hamming']);


    % Compute spectrogram in Chebychev window, 50% overlap
    w = window(@chebwin,nfft);
    [B,f,t] = spectrogram(x,w,[],nfft,fs);

    figure;
        mesh(t,f,abs(B));
        xlabel('Time (sec)','FontSize',14);
        ylabel('Frequency (Hz)','FontSize',14);
        axis([0 1 0 250 0 12]);
        colormap('gray'); caxis([0 20]);
        title(['Window size = ' num2str(nfft) ' Window type= Chebyshev' ]);
    figure;
        contour(t,f,abs(B));
        xlabel('Time (sec)','FontSize',14);
        ylabel('Frequency (Hz)','FontSize',14);
        axis([0 1 0 250]);
        colormap('gray'); caxis([0 40]);
        title(['Window size = ' num2str(nfft) ' Window type= Chebyshev']);

end