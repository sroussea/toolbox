function [powSpectra F_axis_Sg] = powDensitySpectra( dlTrack, Ts, bPlot )
   
    N = size(dlTrack,1);
    Fs=1/Ts;
    F_axis = (0:N)*Fs/N;
    F_axis_Sg = (0:N/2)*Fs/N;



    fft_dlTrack = fft(dlTrack);
    absFFTdlTrack = abs(fft_dlTrack)/N;    % FFT Module in �m 
    absFFTdlTrack_Sg = absFFTdlTrack(1:N/2+1,:);    
    absFFTdlTrack_Sg(2:end,:) = 2*absFFTdlTrack_Sg(2:end,:);
    powSpectra = mean(0.5*absFFTdlTrack_Sg.^2,2);
    powSpectra(1) = 2*powSpectra(1); 
    
    % SR 14th of August 2018 : why this 0.5 multiplication to get mono
    % sided DSP ?
    % Regular DSP is equal to [abs(fft_dlTrack)/N].^2 ,if we want to
    % compute a single sided powSpectra from the single sided FFT, we
    % should compensate for the factor 2 :
    % Let be FFT_dbl = [-V1/2, V0, V1/2], then FFT_sg = [V0, V1]
    % IF powSpectra_dbl = abs(FFT_dbl).^2 THEN
    % powSpectra_sg = [powSpectra_dbl(1), 0.5*powSpectra_dbl(2:end)]
    if ( logical(bPlot) )
        figure;
        roiF = (1:size(F_axis_Sg,2));
        plot(F_axis_Sg(roiF),powSpectra(roiF),'b')
        xlabel('Hz');ylabel('�m^2/Hz');
        title('Spectral power of the cart oscillations');
    end
    
    
    
end

