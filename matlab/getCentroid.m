function [centroid] = getCentroid( grayLevelMat)

grayLevelMat=grayLevelMat/sum(grayLevelMat(:));
[m,n]=size(grayLevelMat);
[I,J]=ndgrid(0:m-1,0:n-1);
centroid = [dot(I(:),grayLevelMat(:)),  dot(J(:),grayLevelMat(:))];  