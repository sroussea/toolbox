%% ----------------------------------------------------------------------

%   Sylvain Rousseau 7th of June 2013

% function [ myCut ] = biDimCut( fileName, QRange )
%
% Perform a track selection over bidim from some energy range
% ----------------------------------------------------------------------

function [tksIRaw tksQRaw Imax, Qmax ] = readCurves( fileName )

[tksIRaw tksQRaw] =  readSignedRawData(fileName);

%% Perform  selection into (E-m) graph
Imax=max(tksIRaw);
Qmax=max(tksQRaw);

end