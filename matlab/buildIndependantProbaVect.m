% 
% Build the independant probability vector of nucleotide pair appearance from the
% 2 discrete probability distributions of single nucleotide appearance of each 
% sequence.
% The discrete probability distributions of single nucleotide appearance of 
% the sequence Bi is stored in the matrix row nucleo_proba(indexBi,:)

function [pBipBj] = buildIndependantProbaVect( nucleo_proba, indexBi, indexBj)
pBipBj=[];
% nucIProUsed=[];
% nucJProUsed=nucleo_proba(indexBj,:)
if indexBi~=indexBj
  for i=1:length(nucleo_proba)
%     nucIProUsed=[nucIProUsed nucleo_proba(indexBi,i)]
    
    pBipBj=[pBipBj nucleo_proba(indexBi,i)*nucleo_proba(indexBj,:)];
  end
else  
  pBipBj=nucleo_proba(indexBi,:).*nucleo_proba(indexBi,:);
end
