function [ fftCoefs absFftSg spectPowDensity avPow F_axis_Sg ] = fft1D_Tools( sigInput, Fs )

N = length(sigInput);
Ts=1/Fs;
F_axis = (0:N)*Fs/N;
F_axis_Sg = (0:N/2)*Fs/N;

fftCoefs = fft(sigInput);
absFft = abs(fftCoefs)/N;    % FFT Module in V 
absFftSg(1:N/2+1,:) = absFft(1:N/2+1,:);
absFftSg(2:end,:) = 2*absFftSg(2:end,:);
spectPowDensity = .5*absFftSg.^2;
avPow = sum(spectPowDensity,1)

