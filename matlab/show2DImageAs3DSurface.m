%% --------------------------------------------------------------------
% Show 2D image as a smooth 3D surface  
% 
% input :
% myImage  : 2d matrix containing image in grey levels 
% figTitle : string containing the figure title
% xRange   : X range of the definition domain
% yRange   : Y range of the definition domain

% Call example :
% imOrg : an image previously read out from myHit.tiff file

% [imOrg,mu]=imread('myHit.tiff');
% hitWideFrameSize = 100;    % sub image length
% spotCoords = [386 1339] ;  % coordinates of the spot which I want to plot
% hitFrame = [ spotCoords - hitWideFrameSize/2  hitWideFrameSize hitWideFrameSize];
% imOrgCrop    = imcrop(imOrg,hitFrame);
% xRange = (hitFrame(1): hitFrame(1) + hitWideFrameSize);
% yRange = (hitFrame(2): hitFrame(2) + hitWideFrameSize);
% show2DImageAs3DSurface( imOrgCrop,listOfIms(iFile,:), xRange, yRange );

% ---------------------------------------------------------------------


function [ ] = show2DImageAs3DSurface( myImage,figTitle, xRange, yRange )

  [X,Y] = meshgrid(xRange,yRange);
  figure('name',figTitle,'NumberTitle','off'); 

  colormap Jet
  surf(X,Y,double(myImage),'FaceColor','interp',...
   'EdgeColor','none',...
   'FaceLighting','gouraud');
 title(figTitle);
  axis tight;
  view(-25, 16);
