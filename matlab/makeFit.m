function [ polyFitCoefs] = makeFit( polyDegs, f1Restrictions)
 
rangeNb = size(f1Restrictions, 2);
coefValues = cell(1,rangeNb );

parfor resId = 1:rangeNb 
  restrictionF1 = f1Restrictions{1,resId};
  rang1 = f1Restrictions{2,resId};

  [xData, yData] = prepareCurveData( rang1, restrictionF1 );

  % Set up fittype and options.
  fitType = ['poly' num2str(polyDegs(resId))];
  ft = fittype( fitType );

  % Fit model to data.
  [fitresult, gof] = fit( xData, yData, ft );
  coefValues{resId} = coeffvalues(fitresult);
    
end

    polyFitCoefs = horzcat(coefValues{:});