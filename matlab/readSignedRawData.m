function [tksI tksQ] =  readSignedRawData( fileName )

headr=zeros(1,2);
fid = fopen(fileName);


headr = fread(fid, size(headr), '*ushort') ;
nbOfpts=headr(1); nbOfTks=headr(2);

tksI = double( fread(fid, [nbOfpts, nbOfTks], '*short') );
tksQ = double( fread(fid, [nbOfpts, nbOfTks], '*short') );

fclose(fid);



end