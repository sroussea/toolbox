
% BE CAREFUL : biplot normalizes the score data before plotting :
% https://nl.mathworks.com/matlabcentral/answers/224271-how-to-decipher-a-biplots-observation
%
% biplot scales the scores so that they fit on the plot: It divides each score by the maximum absolute value of all scores, 
% and multiplies by the maximum coefficient length of coefs.
% Then biplot changes the sign of score coordinates according to the sign convention for the coefs.
% 
% In R2015a, the exact transformation steps can be seen in lines 198 and 199 of "biplot.m":
%       maxCoefLen = sqrt(max(sum(coefs.^2,2)));  % coefs == pc
%       scores = bsxfun(@times, maxCoefLen.*(scores ./ max(abs(scores(:)))), colsign);


function plotPCA(varNames, pc, score,latent, iVarMax)

xx=char(varNames);
xx=horzcat(xx,blanks(size(xx,1))');
xx = char(reshape(xx',1,size(xx,1)*size(xx,2)));
iVarMax = length(varNames);

for i = 1:iVarMax
  for j = i+1:iVarMax
    tit = ['pc' num2str(i) ' vs pc' num2str(j)];
    figure('name',  strcat(tit, ' for VAR = ( ', xx, ' )' )) ;
    biplot(pc(:,[i j]),'Scores',score(:,[i j]),'VarLabels',varNames');title(tit);    
  end   
end
% tit = 'pc2 vs pc1'; figure('name',  strcat(tit, ' for VAR = ( ', xx, ' )' )) ;
% biplot(pc(:,1:2),'Scores',score(:,1:2),'VarLabels',varNames');title(tit);
% tit = 'pc3 vs pc2'; figure('name', strcat(tit, ' for VAR = ( ', xx, ' )' )  );
% biplot(pc(:,2:3),'Scores',score(:,2:3),'VarLabels',varNames');title(tit);
% tit = 'pc3 vs pc1'; figure('name', strcat(tit, ' for VAR = ( ', xx, ' )' )  );
% biplot(pc(:,[1 3]),'Scores',score(:,[1 3]),'VarLabels',varNames');title(tit);

figure('name', strcat('inertia for var = ', xx));plot(latent/sum(latent),'b-o')
return;