% function [entropy] = EntropyInShannon( probaDistri )
%Function which computes and returns the entropy (in Shannon) of any discrete probability
%distribution given as input parameter
function [entropy] = EntropyInShannon( probaDistri )

%make 0*log2( 0 ) null
nullEltId = find(probaDistri == 0);
probaDistri( nullEltId )  = 1;
entropy = -sum( probaDistri.*log2(probaDistri));
