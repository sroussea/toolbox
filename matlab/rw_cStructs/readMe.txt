https://www.mathworks.com/matlabcentral/fileexchange/24540-struct_read-struct_write



struct_read simplifies the reading of a C structure from a binary file by providing a functionality similar to the Python 'struct' module. The elements of the structure are specified as a series of characters (and optional multipliers) in a format string. struct_read then reads these binary values returning a cell array of the results. Results are returned either as character arrays or doubles since these are the default MATLAB types.

struct_write similarly takes a file handle, a format string and a cell array of data. Each item in the cell array is converted to the specified format in the string and written to the file handle.

The following list of format specifiers is generally supported.
% FORMAT SPECIFIERS:
%
% Specifier Bytes: Notes
% c 2 char returned as a string character
% C 2 char returned as a number (double)
% b 2 signed char
% B 2 unsigned char
% h 2 short
% H 4 unsigned short
% i 4 int
% I 4 unsigned int
% l 4 long
% L 4 unsigned long
% f 4 float
% d 8 double
% s char[] char[] returns character string

% EXAMPLE:
%
% Given the following C structure definition for a depth measurement from an
% echo-sounder:
%
% typedef struct
% {
% float depth1; //Depth1
% char units; // m for meters, f for feet
% double time_stamp; //Time stamp, number of seconds elapsed since midnight (00:00:00), January 1, 1970
% char[256] navstring; // GPS Position String
% } ECHO;
%
% fid = fopen('mybinaryfile','r');
% D = struct_read(fid,'fcd256c')
%
% D:
% { [23.456],'m',[1242238192.870432012], ...
% '$GPGGA,182503.00,7121.11801,N,15651.53926,W,1,12,0.8,20.49,M,-048,M,,*57' }

NOTE:
Reading and properly interpreting C-pointers is not generally supported.
