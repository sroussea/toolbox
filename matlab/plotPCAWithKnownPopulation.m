% -------------------------------------------------------------------------
%  Sylvain Rousseau. IPNO.
%  3rd of June 2014
% -------------------------------------------------------------------------
%  SR : will draw the individuals in PC axis
%  varNames  : contain the names of the factors
%  pc        : eigen vectors of the correlation matrix
%  score     : individuals coordinates in PC axis
%  latent    : eigen values of the correlation matrix
%  stPopDesc : array of structures to describe each population
%              struct(
%              'bPlot' : 0 or 1 depending on if we ask to plot this
%              population
%              'size'  : (int) nb of individuals in the population
%              'name'  : (string) name of the population
% axisIds    : vector containig PC axis ids to graph
% saveGraphs : bool, save graphs on disk if true
% savPath    : string containing location path to save results
% -------------------------------------------------------------------------



function plotPCAWithKnownPopulation(varNames, pc, score,latent, stPopDesc,...
         axisIds, saveGraphs, savPath )

% Build a string made of variable names 
xx = strjoin(varNames,',')

bPlotAsked = [stPopDesc.bPlot];
popSizes   = [stPopDesc.size];
popNames   = strvcat(stPopDesc.name);


[ flags ids ] = find( bPlotAsked ~=0 );
nAxis = length(axisIds);
colorList = ['r' 'g' 'c' 'm'];

if ( saveGraphs == 1 ) && ~(exist(savPath) == 7)
    mkdir(savPath);
end;

for xPC = 1:(nAxis - 1)
  for yPC = (xPC + 1):nAxis;
    tit = ['pc' num2str(yPC) ' vs pc' num2str(xPC)];      
    filNam = [ savPath '\' tit '_' num2str( length(latent) ) 'vars.fig' ] ;
    h = figure('name',  strcat(tit, ' for VAR = ( ', xx, ' )' )) ;hold on;
    for iPop = ids
      idStartPop1 = sum(popSizes(1:iPop)) - popSizes(iPop) + 1;
      idEndPop1   = sum(popSizes(1:iPop)) ;
      if ( iPop == ids(end) )
        hPlot{iPop} = biplot(pc(:,[xPC yPC]),'Scores',score(idStartPop1:idEndPop1,[xPC yPC]),'VarLabels',varNames','Color', colorList(iPop));
      else
        hPlot{iPop} = biplot(pc(:,[xPC yPC]),'Scores',score(idStartPop1:idEndPop1,[xPC yPC]),'Color', colorList(iPop));
      end;
      title(tit);xlabel(['pc' num2str(xPC)]);ylabel(['pc' num2str(yPC)]);
    end;
    hold off;
    legend(cellfun(@(v) v(1), hPlot), {stPopDesc.name})
    
    % save current plot if requested 
    if ( saveGraphs == 1 )
      savefig( h, filNam);
    end;
  end;
end;

tit = strcat('inertia for var = ', xx);
h = figure('name', tit);plot(latent/sum(latent),'b-o')
if ( saveGraphs == 1 )
  savefig( h, [savPath '\'  tit '.fig']);
end;

return;