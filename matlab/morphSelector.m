% function [goodCurvesID] = EntropyInShannon( probaDistri )
%Function which computes and returns the entropy (in Shannon) of any discrete probability
%distribution given as input parameter
function [goodCurvesID] = morphSelector( cellTracks )


% detect good (I,Q) pairs on Min-Max criterion
nTracks = size(cellTracks{1,1}, 2);
goodICurv=zeros(1,nTracks);
goodQCurv=goodICurv;

ItracksMA = movAver( cellTracks{1,1}, 10 );
QtracksMA = movAver( cellTracks{1,2}, 10 );

goodICurv = find ( max(ItracksMA) > 50 & min(ItracksMA) > -20 )   
goodQCurv = find ( max(QtracksMA) < 10 & min(QtracksMA) > -2500 )   
goodCurvesID = intersect( goodICurv ,goodQCurv );

